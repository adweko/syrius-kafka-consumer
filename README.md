# Syrius-Kafka-Consumer

# Kontext & Beschreibung
Der Syrius-Kafka-Consumer ist die Schnittstelle zwischen dem MDM-System und Syrius und dient der Synchronisation des
Partnerdatenbestands dieser beiden Systeme.
MDM sendet Informationen über darin abgespeicherte Partner nach Kafka. 
Syrius-Kafka-Consumer konsumiert diese Daten, transformiert sie in das Zielformat für Syrius und sendet sie per
Webservice-Call nach Syrius.

Das Kafka-Topic, das konsumiert wird, ist *ch-mdm.change.avro*. Es beinhaltet Änderungen am Partner, keine vollständigen
Entitäten.

Es wird davon ausgegangen, dass eine Nachricht in Kafka immer einer vollständigen Transaktion entspricht.

## Authentifizierung der Webservice-Calls nach Syrius
Zu Syrius wird eine SSL-Verbindung hergestellt, die Details sind in der Klasse SSLConfig.java implementiert.

Weiterhin müssen die Webservice-Calls via SAML authentifiziert werden.
Dazu muss eine valide SAML-Response im HTTP-Header mitgeschickt werden, und zwar im Format einer Basic user:password Authentifizierung.
Im HTTP-Header muss der key "Authorization" mit dem value "Basic <base64(userid:samlresponse)>" gesetzt sein.
Dabei besteht "<base64(userid:samlresponse)>" aus der Konkatenierung von username, explizitem Doppelpunkt und dem 
XML der SAML-Response. Dieser String wird base64-enkodiert.
Die Implementierung findet sich in der Klasse SyriusConsumer.java in der Funktion prepareWs().
Zu beachten ist, dass die WS-Calls auf Syrius zunächst mit einem 307 Redirect antworten. Dabei wir ein
Authentifizierungscookie gesetzt, der beibehalten werden muss für alle Folgerequests.
Beibehalten von Cookies und Folgen von Redirections (insbesondere auf gleiche URL) sind standardmäßig deaktiviert,
 wenn die APIs via Apache CXF generiert werden. Daher werden in prepareWs() die entsprechenden Configs gesetzt.

Der gesetzte Wert für "userId" wird laut Angabe von Syrius bei der SAML-Authentifizierung nicht überprüft.
Er scheint allerdings in den Calls an irgendeiner Stelle aufzutreten, ein invalider User führt zu "Fehler in API Bridge".
Klärung offen (2020/08/11).

# Open Points

## Korrekte Befüllung des Feldes "ExterneVersion"
Für Kontext zum Feld siehe [Syrius Dokumentation](http://syrius-doku.tst.oc.hermes.scsyr.hev.cloud/doku/aktuell/de-CH/7305815563.html).

Es handelt sich dabei um das von einem externen System zu befüllende Feld zur "Versionsnummer dieser Entität". Dieses
sollte entsprechend mit jedem Update über einen Webservice-Call mit einem höheren Wert befüllt werden.
Momentan wird zu Befüllung der Zeitstempel aus "BITEMPMDMTimestamp" benutzt, was dem technischen Zeitstempel der
 Transaktion im MDM entspricht, der als UTC-Timestamp im Format yyyyMMddHHmmssSSS geliefert wird 
 (z.B. 20200618144950121).
 Dieser wird zur Befüllung des Feldes in "nanosecond since epoch" gemappt, mit folgendem Code: 
 ```java
// JurPersonMapper.java l. 165
 long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4,6)),
                Integer.parseInt(mdmTimeStamp.substring(6,8)), Integer.parseInt(mdmTimeStamp.substring(8,10)),
                Integer.parseInt(mdmTimeStamp.substring(10,12)), Integer.parseInt(mdmTimeStamp.substring(12,14)),
                Integer.parseInt(mdmTimeStamp.substring(14,17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;
```
- Es wurde beobachtet, dass die aktuelle Implementierung auch nicht zu immergleichen Werten führt, vermutlich wegen
 Rundungsfehlern beim Aufruf von toEpochSeconds().
 
Derzeit ist folgende weitere Modifikation eingebaut: 
- Da eine Transaktion aus dem MDM zu mehreren Updates (gleich Webservice-Aufrufen) auf einer Entität Geschäftspartner
im Syrius führen kann, wird momentan ein Fixwert pro Call hierauf addiert (Suche nach "mdmModifiedEpoch" im Code).



Die einzige aus der Syrius-Dokumentation ersichtliche Anforderung ist, dass für neuere Versionen die externeVersion steigen muss, 
also hier mit jedem Call, da Calls mit kleinerer externeVersion abgelehnt werden.
Die derzeitige Logik kann hier brechen, wenn zwei MDM-Transaktionen auf derselben Entität
zeitlich sehr nah beieinander liegen oder, etwas offensichtlicher, wenn ein Reprocessing stattfindet. 
Beispiel Reprocessing: Nachricht führt zu drei Calls, mit (t1-t2-t3). Diese Nachricht wird re-processed. Insgesamt kommen
dann Calls nach Syrius mit (t1-t2-t3-t1-t2-t3), statt dem zu erwartenden (t1-t2-t3-t4-t5-t6).

Mögliche Verbesserungen wären,
 1. Stattdessen den current-UTC-time-in-nanoseconds für jeden Call zu nehmen. Einfach umzusetzen, kann aber gefährlich sein.
 2. Pro MDM-PartnerID einen Counter einführen, der inkrementiert wird mit jedem Call. Etwas komplexere Umsetzung, 
   aber sehr sicher. Interne State Stores in Kafka Streams bieten sich für derartige Use Cases an und haben den Vorteil,
   dass sie automatisch Teil von Transaktionen sind (Teil der "processsing guarantees" von Kafka), und man sich daher
   im Fehlerfall nicht um ein Rollback kümmern muss. Jeder andere lokale, ausfallsichere Speicher#
   kann auch benutzt werden.
 
 
In Absprache mit Daniela Martinez und Marco Wagner (Call 28.7.2020) soll Variante 1 umgesetzt werden.
