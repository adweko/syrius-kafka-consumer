//package com.helvetia.app.quarkus.kafka;
//
//import com.adcubum.syrius.api.partnermgmt.common.codes.v1.*;
//import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.*;
//import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.data.partnerrolle.v1.PartnerrolleServicePortType;
//import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.ApiFaultMessage;
//import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.PartnerReplikationExpServicePortType;
//import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.*;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.helvetia.app.quarkus.kafka.common.AppHelper;
//import com.helvetia.app.quarkus.kafka.common.WSSecurityHeaderSOAPHandler;
//import com.helvetia.app.quarkus.kafka.config.SAMLAssertionCreator;
//import com.helvetia.app.quarkus.kafka.config.SAMLResponseCreator;
//import com.helvetia.app.quarkus.kafka.config.SyriusConfig;
//import com.helvetia.app.quarkus.kafka.config.WsConfiguration;
//import com.helvetia.security.saml.SAMLFactory;
//import io.quarkus.test.junit.QuarkusTest;
//import org.apache.cxf.configuration.security.AuthorizationPolicy;
//import org.apache.cxf.endpoint.Client;
//import org.apache.cxf.frontend.ClientProxy;
//import org.apache.cxf.headers.Header;
//import org.apache.cxf.jaxb.JAXBDataBinding;
//import org.apache.cxf.message.Message;
//import org.apache.cxf.transport.http.HTTPConduit;
//import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
//import org.jetbrains.annotations.NotNull;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.w3c.dom.Element;
//
//import javax.inject.Inject;
//import javax.xml.bind.JAXB;
//import javax.xml.datatype.DatatypeConfigurationException;
//import javax.xml.datatype.DatatypeFactory;
//import javax.xml.namespace.QName;
//import javax.xml.transform.TransformerException;
//import javax.xml.ws.BindingProvider;
//import javax.xml.ws.handler.Handler;
//import javax.xml.ws.handler.MessageContext;
//import java.io.IOException;
//import java.io.StringWriter;
//import java.security.KeyManagementException;
//import java.security.KeyStoreException;
//import java.security.NoSuchAlgorithmException;
//import java.security.cert.CertificateException;
//import java.util.*;
//
//
//@QuarkusTest
//public class SyriusDummyTest {
//
//    @Inject
//    SyriusConfig syriusConfig;
//
//    @Test
//    public void testrepliziereDummyNatPerson() throws Exception {
//        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
//        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
//        RepliziereNatPersonRequestType natPersonRequestType = getRepliziereNatPersonRequestType();
//
//        // Generate SAML assertion & response and add it to the port
//        WsConfiguration wsConfiguration = new WsConfiguration();
//        SyriusConsumer.prepareWs(partnerReplikationExpServicePortType, wsConfiguration.getConfiguration(), syriusConfig);
//
//        RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereNatPerson(natPersonRequestType);
//        Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
//        System.out.println("Response:  " + responseType.getReplikationErfolgreich());
//    }
//
//    @Test
//    public void testErstelleSAML() throws Exception {
//
//        System.out.println("testErstelleSAML Start");
//
//        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
//        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
//        RepliziereNatPersonRequestType natPersonRequestType = getRepliziereNatPersonRequestType();
//        final RepliziereJurPersonRequestType jurPersonRequestType = getDummyRepliziereJurPersonRequestType();
//
//        // Generate SAML assertion
//        WsConfiguration wsConfiguration = new WsConfiguration();
//        SAMLResponseCreator responseCreator = new SAMLResponseCreator();
//        // SAMLAssertionCreator assertionCreator = new SAMLAssertionCreator();
//
//        Element samlResponse = responseCreator.getSAMLResponse(wsConfiguration.getConfiguration(), false);
//        // Element samlResponse = assertionCreator.getSAMLAssertion(wsConfiguration.getConfiguration(), true);
//
//        String samlResponseString = responseCreator.getSamlStringResponse(samlResponse);
//        System.out.println("testErstelleSAML SAML: " + samlResponseString);
//
//        String samlResponseStringWithUserBase64 = new String(Base64.getEncoder().encode(
//                (wsConfiguration.getConfiguration().getLoggedInUser() + ":" + samlResponseString).getBytes()));
//        System.out.println("testErstelleSAML SAML (base64): " + samlResponseStringWithUserBase64);
//
//
//        // System.out.println("testErstelleSAML Orig:  " + AppHelper.classToJson(partnerReplikationExpServicePortType));
//
////        syriusConfig.getSslConfig().setupSSL(partnerReplikationExpServicePortType);
//////        System.out.println("testErstelleSAML with SSL:  " + AppHelper.objectToXml(partnerReplikationExpServicePortType));
////
////        // SAML-Assertion dem Request hinzufügen
////        // SyriusConsumer.prepareWs(partnerReplikationExpServicePortType,samlResponse);
////        // System.out.println("testErstelleSAML with SAML:  " + AppHelper.classToJson(partnerReplikationExpServicePortType));
////
////        BindingProvider bindingProvider = (BindingProvider) partnerReplikationExpServicePortType;
////
////        // START simple setting of HTTP Header
////        Map<String, List<String>> headers = new HashMap<>();
////        headers.put("Authorization", Arrays.asList("Basic " + samlResponseStringWithUserBase64));
////        bindingProvider.getRequestContext().put(Message.PROTOCOL_HEADERS, headers);
////        // END simple setting of HTTP Header
////
////        // START Not working so far
//////        // info: https://www.it-swarm.dev/de/java/wie-fuege-ich-den-header-hinzu-soap-anfordern/1071794235/
//////        List<Handler> handlerChain = new ArrayList<>();
//////        handlerChain.add(new WSSecurityHeaderSOAPHandler(wsConfiguration.getConfiguration().getLoggedInUser(), samlResponseString));
//////        bindingProvider.getBinding().setHandlerChain(handlerChain);
////        // END not working
////
////        // START setting basic auth instead of low-level header
////        // see https://stackoverflow.com/questions/27252195/how-to-dynamically-add-http-headers-in-cxf-client
//////        AuthorizationPolicy authzPolicy = new AuthorizationPolicy();
//////        authzPolicy.setUserName(wsConfiguration.getConfiguration().getLoggedInUser());
//////        authzPolicy.setPassword(samlResponseStringWithUserBase64);
//////        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(AuthorizationPolicy.class.getName(), authzPolicy);
////        // END setting basic auth instead of low-level header
////
////        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().entrySet().forEach(System.out::println);
////
////
////        // Up to here, we get org.apache.cxf.binding.soap.SoapFault: Error reading XMLStreamReader: Unexpected EOF in prolog
////        // caused by com.ctc.wstx.exc.WstxEOFException: Unexpected EOF in prolog
////        // which seems to be because the response is not parseable
////        // and is likely related to a redirectino (http code 307)
////
////        // this here sets "follow redirects"
////        Client client = ClientProxy.getClient(partnerReplikationExpServicePortType);
////        HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
////        final HTTPClientPolicy policy = httpConduit.getClient();
////        policy.setAutoRedirect(true);
////
////        client.getRequestContext().put(
////                org.apache.cxf.message.Message.MAINTAIN_SESSION, Boolean.TRUE);
////
////        // ... but leads to java.io.IOException: Relative Redirect detected on Conduit
////
//////
////        // allow relative redirects  then
////        client.getRequestContext().put("http.redirect.relative.uri", "true");
////        // ... leads to java.io.IOException: Redirect loop detected on Conduit
////
////        // allowing for some looping on same URI
////        client.getRequestContext().put("http.redirect.max.same.uri.count", 2);
////        // ... now leads to 401
////
//
//        SyriusConsumer.prepareWs(partnerReplikationExpServicePortType, wsConfiguration.getConfiguration(), syriusConfig);
//
//        RepliziereResponseType responseType;
//
////        responseType = partnerReplikationExpServicePortType.repliziereJurPerson(jurPersonRequestType);
//
//        responseType = partnerReplikationExpServicePortType.repliziereNatPerson(natPersonRequestType);
//        Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
//        System.out.println("testErstelleSAML Response:  " + responseType.getReplikationErfolgreich());
//
//        int i = 1;
//        Assertions.assertEquals(1, i);
//    }
//
//    @NotNull
//    private RepliziereNatPersonRequestType getRepliziereNatPersonRequestType() throws DatatypeConfigurationException {
//        RepliziereNatPersonRequestType natPersonRequestType = new RepliziereNatPersonRequestType();
//        natPersonRequestType.setMessageId(UUID.randomUUID().toString());
//        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
//        // che0bmc, 20200722, entferne externeReferenz
//        // WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
//        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
//        wsPartnerIdType.setId(UUID.randomUUID().toString());
//        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
//        // wsPartnerReferenzType.setExterneReferenz(wsExterneReferenzType);
//        WsNatPersonType wsNatPersonType = new WsNatPersonType();
//        //wsNatPersonType.setGeburtsdatum(DatatypeFactory.newInstance().newXMLGregorianCalendar("1977-01-01"));
//        WsGeschlechtCodeType wsGeschlechtCodeType = new WsGeschlechtCodeType();
//        wsGeschlechtCodeType.setCodeId("-10000");
//        wsNatPersonType.setGeschlecht(wsGeschlechtCodeType);
//        //wsNatPersonType.setNationalitaet("CH");
//        wsNatPersonType.setVorname("LetsHope");
//        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
//        wsMutationsgrundIdType.setId("-1");
//        wsNatPersonType.setMutationsgrundId(wsMutationsgrundIdType);
//        wsNatPersonType.setName("ItWorksAgain");
//        wsNatPersonType.setKorrespondenzsprache("de-CH");
//        WsPartnerschutzDefIdType wsPartnerschutzDefIdType = new WsPartnerschutzDefIdType();
//        //wsPartnerschutzDefIdType.setId("-1");
//        wsNatPersonType.setPartnerschutzDefId(wsPartnerschutzDefIdType);
//        wsNatPersonType.setStateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-05-05"));
//        wsNatPersonType.setStateUpto(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        WsNatPersonStrukturType wsNatPersonStrukturType = new WsNatPersonStrukturType();
//        wsNatPersonStrukturType.setGueltAb(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-05-05"));
//        wsNatPersonStrukturType.setGueltBis(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        //wsNatPersonStrukturType.setExterneVersion(6223372036854775804L);
//        wsNatPersonStrukturType.getNatPersonStates().add(wsNatPersonType);
//        natPersonRequestType.setNatPersonStruktur(wsNatPersonStrukturType);
//        natPersonRequestType.setPartnerReferenz(wsPartnerReferenzType);
//        return natPersonRequestType;
//    }
//
//    @Test
//    public void testRepliziereDummyDomizilAdresse() throws Exception {
//        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
//        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
//        RepliziereDomiziladresseRequestType repliziereDomiziladresseRequestType = new RepliziereDomiziladresseRequestType();
//        WsPartnerReferenzType partnerReferenzType = new WsPartnerReferenzType();
//        WsPartnerIdType partnerIdType = new WsPartnerIdType();
//        //partnerIdType.setId(UUID.randomUUID().toString());
//        partnerIdType.setId("c4265409-3886-4d11-ac9f-8394f00848cb");
//        partnerReferenzType.setPartnerId(partnerIdType);
//        repliziereDomiziladresseRequestType.setMessageId(UUID.randomUUID().toString());
//        repliziereDomiziladresseRequestType.setPartnerReferenz(partnerReferenzType);
//        WsDomiziladresseStrukturType wsDomiziladresseStrukturType = new WsDomiziladresseStrukturType();
//        wsDomiziladresseStrukturType.setGueltAb(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-01-01"));
//        wsDomiziladresseStrukturType.setGueltBis(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        //WsAdresseVerwendungType wsAdresseVerwendungType = new WsAdresseVerwendungType();
////        wsAdresseVerwendungType.setErlaubnis();
////        wsAdresseVerwendungType.setErlaubnisart();
//        //wsDomiziladresseStrukturType.setAdressverwendung(wsAdresseVerwendungType);
//        WsAdresseType wsAdresseType = new WsAdresseType();
//        wsAdresseType.setStateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-01-01"));
//        wsAdresseType.setStateUpto(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        wsAdresseType.setPlz("9000");
//        wsAdresseType.setOrt("St. Gallen");
//        wsAdresseType.setLand("CH");
//        wsAdresseType.setHausnummer("3");
//        wsAdresseType.setStrasse("Bahnhofstrasse");
////        wsAdresseType.setKanton();
////        wsAdresseType.setGemeindeKanton();
////        wsAdresseType.setAbhaengigVon();
////        wsAdresseType.setBemerkungen();
////        wsAdresseType.setGemeindename();
////        wsAdresseType.setGemeindenummer();
////        wsAdresseType.setHausId();
////        wsAdresseType.setHausNrZusatz();
//        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
//        wsMutationsgrundIdType.setId("-1");
//        wsAdresseType.setMutationsgrundId(wsMutationsgrundIdType);
////        wsAdresseType.setPlzId();
////        wsAdresseType.setPlzZusatz();
////        wsAdresseType.setPostfachOhneNr();
////        wsAdresseType.setStrasseId();
////        wsAdresseType.setStrasseNrReihenfolge();
////        wsAdresseType.setZusatz1NachStr();
////        wsAdresseType.setZusatz1VorStr();
////        wsAdresseType.setZusatz2NachStr();
////        wsAdresseType.setZusatz2VorStr();
////        wsAdresseType.setZusatz3VorStr();
////        wsAdresseType.setZusatz4VorStr();
//        wsDomiziladresseStrukturType.getAdresseStates().add(wsAdresseType);
//        repliziereDomiziladresseRequestType.setDomiziladresseStruktur(wsDomiziladresseStrukturType);
//        WsAdresseReferenzType wsAdresseReferenzType = new WsAdresseReferenzType();
//        WsAdresseIdType wsAdresseIdType = new WsAdresseIdType();
//        wsAdresseIdType.setId(UUID.randomUUID().toString());
//        //wsAdresseIdType.setId("14275ce7-cc2e-4588-9e4b-45ff27cbe1f3");
//        wsAdresseReferenzType.setAdresseId(wsAdresseIdType);
//        repliziereDomiziladresseRequestType.setAdresseReferenz(wsAdresseReferenzType);
//
//        // Generate SAML assertion & response and add it to the port
//        WsConfiguration wsConfiguration = new WsConfiguration();
//        SyriusConsumer.prepareWs(partnerReplikationExpServicePortType, wsConfiguration.getConfiguration(), syriusConfig);
//
//        RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereDomiziladresse(repliziereDomiziladresseRequestType);
//        Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
//        System.out.println("Response:  " + responseType.getReplikationErfolgreich());
//    }
//
//    @Test
//    public void testRepliziereDummyZusatzAdresse() throws Exception {
//        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
//        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
//        RepliziereZusatzadresseRequestType repliziereZusatzadresseRequestType = new RepliziereZusatzadresseRequestType();
//        repliziereZusatzadresseRequestType.setMessageId(UUID.randomUUID().toString());
//        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
//        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
//        wsPartnerIdType.setId("5CA07564-5EEB-4910-9B5A-372486F07C97");
//        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
//        repliziereZusatzadresseRequestType.setPartnerReferenz(wsPartnerReferenzType);
//        WsAdresseReferenzType wsAdresseReferenzType = new WsAdresseReferenzType();
//        WsAdresseIdType wsAdresseIdType = new WsAdresseIdType();
//        wsAdresseIdType.setId(UUID.randomUUID().toString());
//        wsAdresseReferenzType.setAdresseId(wsAdresseIdType);
//        repliziereZusatzadresseRequestType.setAdresseReferenz(wsAdresseReferenzType);
//        WsZusatzadresseStrukturType wsZusatzadresseStrukturType = new WsZusatzadresseStrukturType();
//        //wsZusatzadresseStrukturType.setAdressverwendung();
//        wsZusatzadresseStrukturType.setGueltAb(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-01-01"));
//        wsZusatzadresseStrukturType.setGueltBis(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        WsAdresseType wsAdresseType = new WsAdresseType();
//        wsAdresseType.setStateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-01-01"));
//        wsAdresseType.setStateUpto(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        wsAdresseType.setPlz("8050");
//        wsAdresseType.setOrt("Zürich");
//        wsAdresseType.setLand("CH");
//        wsAdresseType.setHausnummer("2");
//        wsAdresseType.setStrasse("Hofwiesenstrasse");
////        wsAdresseType.setKanton();
////        wsAdresseType.setGemeindeKanton();
////        wsAdresseType.setAbhaengigVon();
////        wsAdresseType.setBemerkungen();
////        wsAdresseType.setGemeindename();
////        wsAdresseType.setGemeindenummer();
////        wsAdresseType.setHausId();
////        wsAdresseType.setHausNrZusatz();
//        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
//        wsMutationsgrundIdType.setId("e2212371-1235-4a37-a7ef-41ffd865f4d1");
//        wsAdresseType.setMutationsgrundId(wsMutationsgrundIdType);
////        wsAdresseType.setPlzId();
////        wsAdresseType.setPlzZusatz();
////        wsAdresseType.setPostfachOhneNr();
////        wsAdresseType.setStrasseId();
////        wsAdresseType.setStrasseNrReihenfolge();
////        wsAdresseType.setZusatz1NachStr();
////        wsAdresseType.setZusatz1VorStr();
////        wsAdresseType.setZusatz2NachStr();
////        wsAdresseType.setZusatz2VorStr();
////        wsAdresseType.setZusatz3VorStr();
////        wsAdresseType.setZusatz4VorStr();
//        wsZusatzadresseStrukturType.getAdresseStates().add(wsAdresseType);
//        repliziereZusatzadresseRequestType.setZusatzadresseStruktur(wsZusatzadresseStrukturType);
//
//        // Generate SAML assertion & response and add it to the port
//        WsConfiguration wsConfiguration = new WsConfiguration();
//        SyriusConsumer.prepareWs(partnerReplikationExpServicePortType, wsConfiguration.getConfiguration(), syriusConfig);
//
//        RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereZusatzadresse(repliziereZusatzadresseRequestType);
//        Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
//        System.out.println("Response:  " + responseType.getReplikationErfolgreich());
//    }
//
//    @Test
//    public void testDummyJurPerson() throws Exception {
//        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
//        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
//        RepliziereJurPersonRequestType repliziereJurPersonRequestType = getDummyRepliziereJurPersonRequestType();
//
//        // Generate SAML assertion & response and add it to the port
//        WsConfiguration wsConfiguration = new WsConfiguration();
//        SyriusConsumer.prepareWs(partnerReplikationExpServicePortType, wsConfiguration.getConfiguration(), syriusConfig);
//
//        RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereJurPerson(repliziereJurPersonRequestType);
//        Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
//        System.out.println("Response:  " + responseType.getReplikationErfolgreich());
//    }
//
//    @NotNull
//    private RepliziereJurPersonRequestType getDummyRepliziereJurPersonRequestType() throws DatatypeConfigurationException {
//        RepliziereJurPersonRequestType repliziereJurPersonRequestType = new RepliziereJurPersonRequestType();
//        WsPartnerReferenzType partnerReferenzType = new WsPartnerReferenzType();
//        WsPartnerIdType partnerIdType = new WsPartnerIdType();
//        partnerIdType.setId(UUID.randomUUID().toString());
//        partnerReferenzType.setPartnerId(partnerIdType);
//        repliziereJurPersonRequestType.setMessageId(UUID.randomUUID().toString());
//        repliziereJurPersonRequestType.setPartnerReferenz(partnerReferenzType);
//        WsJurPersonStrukturType wsJurPersonStrukturType = new WsJurPersonStrukturType();
//        wsJurPersonStrukturType.setGueltAb(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-01-01"));
//        wsJurPersonStrukturType.setGueltBis(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        repliziereJurPersonRequestType.setJurPersonStruktur(wsJurPersonStrukturType);
//        WsJurPersonType wsJurPersonType = new WsJurPersonType();
//        //wsJurPersonType.setBranche();
//        WsRechtsformCodeType wsRechtsformCodeType = new WsRechtsformCodeType();
//        wsRechtsformCodeType.setCodeId("-43804");
//        wsJurPersonType.setRechtsform(wsRechtsformCodeType);
////        wsJurPersonType.setZusatz1();
////        wsJurPersonType.setZusatz2();
////        wsJurPersonType.setIntBetreuerId();
//        wsJurPersonType.setKorrespondenzsprache("de-CH");
////        wsJurPersonType.setMitarbeiterBenutzerId();
//        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
//        wsMutationsgrundIdType.setId("-1");
//        wsJurPersonType.setMutationsgrundId(wsMutationsgrundIdType);
//        wsJurPersonType.setName("TestTestAG");
//        //wsJurPersonType.setPartnerschutzDefId();
//        wsJurPersonType.setStateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-01-01"));
//        wsJurPersonType.setStateUpto(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        //wsJurPersonType.setVip();
//        wsJurPersonStrukturType.getJurPersonStates().add(wsJurPersonType);
//        return repliziereJurPersonRequestType;
//    }
//
//    @Test
//    public void testDummyMail() throws Exception {
//        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
//        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
//        RepliziereEmailRequestType repliziereEmailRequestType = new RepliziereEmailRequestType();
//        repliziereEmailRequestType.setMessageId(UUID.randomUUID().toString());
//        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
//        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
//        wsPartnerIdType.setId("964c7a15-32d8-4ae2-a96c-f1663e9bf752");
//        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
//        repliziereEmailRequestType.setPartnerReferenz(wsPartnerReferenzType);
//        WsEmailReferenzType wsEmailReferenzType = new WsEmailReferenzType();
//        WsEmailIdType wsEmailIdType = new WsEmailIdType();
//        wsEmailIdType.setId(UUID.randomUUID().toString());
//        wsEmailReferenzType.setEmailId(wsEmailIdType);
//        repliziereEmailRequestType.setEmailReferenz(wsEmailReferenzType);
//        WsEmailStrukturType wsEmailStrukturType = new WsEmailStrukturType();
//        WsEmailType wsEmailType = new WsEmailType();
//        wsEmailType.setEmailAdresse("test3_emailprivat@test.ch");
//        //wsEmailType.setIsDefStandard();
//        //wsEmailType.setIsStandard();
//        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
//        wsMutationsgrundIdType.setId("-1");
//        wsEmailType.setMutationsgrundId(wsMutationsgrundIdType);
//        wsEmailType.setStateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-04-16"));
//        wsEmailType.setStateUpto(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        wsEmailStrukturType.getEmailStates().add(wsEmailType);
//        WsEmailDefIdType wsEmailDefIdType = new WsEmailDefIdType();
//        wsEmailDefIdType.setId("-40");
//        wsEmailStrukturType.setEmailDefId(wsEmailDefIdType);
//        wsEmailStrukturType.setGueltAb(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-04-16"));
//        wsEmailStrukturType.setGueltBis(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        WsKommVerbVerwendungType wsKommVerbVerwendungType = new WsKommVerbVerwendungType();
//        WsErlaubnisCodeType wsErlaubnisCodeType = new WsErlaubnisCodeType();
//        wsErlaubnisCodeType.setCodeId("-64831");
//        wsKommVerbVerwendungType.setErlaubnis(wsErlaubnisCodeType);
//        WsErlaubnisartCodeType wsErlaubnisartCodeType = new WsErlaubnisartCodeType();
//        wsErlaubnisartCodeType.setCodeId("-64868");
//        wsKommVerbVerwendungType.setErlaubnisart(wsErlaubnisartCodeType);
//        WsKommVerbTypCodeType wsKommVerbTypCodeType = new WsKommVerbTypCodeType();
//        wsKommVerbTypCodeType.setCodeId("-64826");
//        wsKommVerbVerwendungType.setVerbindungstyp(wsKommVerbTypCodeType);
//        wsEmailStrukturType.setKommVerbVerwendung(wsKommVerbVerwendungType);
//        repliziereEmailRequestType.setEmailStruktur(wsEmailStrukturType);
//
//        // Generate SAML assertion & response and add it to the port
//        WsConfiguration wsConfiguration = new WsConfiguration();
//        SyriusConsumer.prepareWs(partnerReplikationExpServicePortType, wsConfiguration.getConfiguration(), syriusConfig);
//
//        RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereEmail(repliziereEmailRequestType);
//        Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
//        System.out.println("Response:  " + responseType.getReplikationErfolgreich());
//    }
//
//    @Test
//    public void testDummyTelefon() throws Exception {
//        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
//        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
//        RepliziereTelefonRequestType repliziereTelefonRequestType = new RepliziereTelefonRequestType();
//        repliziereTelefonRequestType.setMessageId(UUID.randomUUID().toString());
//        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
//        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
//        wsPartnerIdType.setId("4dc2cc2e-b802-4064-9ef2-75daccee0f03");
//        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
//        // che0wea, 20200722, entferne externeReferenz
////        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
////        wsPartnerReferenzType.setExterneReferenz(wsExterneReferenzType);
//        repliziereTelefonRequestType.setPartnerReferenz(wsPartnerReferenzType);
//        WsTelefonReferenzType wsTelefonReferenzType = new WsTelefonReferenzType();
//        WsTelefonIdType wsTelefonIdType = new WsTelefonIdType();
//        wsTelefonIdType.setId(UUID.randomUUID().toString());
//        wsTelefonReferenzType.setTelefonId(wsTelefonIdType);
//        repliziereTelefonRequestType.setTelefonReferenz(wsTelefonReferenzType);
//        WsTelefonStrukturType wsTelefonStrukturType = new WsTelefonStrukturType();
//        WsTelefonType wsTelefonType = new WsTelefonType();
//        wsTelefonType.setTelefonNummer("1234");
//        //wsEmailType.setIsDefStandard();
//        //wsEmailType.setIsStandard();
//        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
//        wsMutationsgrundIdType.setId("e2212371-1235-4a37-a7ef-41ffd865f4d1");
//        wsTelefonType.setMutationsgrundId(wsMutationsgrundIdType);
//        wsTelefonType.setStateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-03-04"));
//        wsTelefonType.setStateUpto(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        wsTelefonStrukturType.getTelefonStates().add(wsTelefonType);
//        WsTelefonDefIdType wsTelefonDefIdType = new WsTelefonDefIdType();
//        wsTelefonDefIdType.setId("-32");
//        wsTelefonStrukturType.setTelefonDefId(wsTelefonDefIdType);
//        wsTelefonStrukturType.setGueltAb(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-03-04"));
//        wsTelefonStrukturType.setGueltBis(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        WsKommVerbVerwendungType wsKommVerbVerwendungType = new WsKommVerbVerwendungType();
//        WsErlaubnisCodeType wsErlaubnisCodeType = new WsErlaubnisCodeType();
//        wsErlaubnisCodeType.setCodeId("-64830");
//        wsKommVerbVerwendungType.setErlaubnis(wsErlaubnisCodeType);
//        WsErlaubnisartCodeType wsErlaubnisartCodeType = new WsErlaubnisartCodeType();
//        wsErlaubnisartCodeType.setCodeId("-64868");
//        wsKommVerbVerwendungType.setErlaubnisart(wsErlaubnisartCodeType);
//        WsKommVerbTypCodeType wsKommVerbTypCodeType = new WsKommVerbTypCodeType();
//        wsKommVerbTypCodeType.setCodeId("-64827");
//        wsKommVerbVerwendungType.setVerbindungstyp(wsKommVerbTypCodeType);
//        wsTelefonStrukturType.setKommVerbVerwendung(wsKommVerbVerwendungType);
//        repliziereTelefonRequestType.setTelefonStruktur(wsTelefonStrukturType);
//
//        // Generate SAML assertion & response and add it to the port
//        WsConfiguration wsConfiguration = new WsConfiguration();
//        SyriusConsumer.prepareWs(partnerReplikationExpServicePortType, wsConfiguration.getConfiguration(), syriusConfig);
//
//        RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereTelefon(repliziereTelefonRequestType);
//        Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
//        System.out.println("Response:  " + responseType.getReplikationErfolgreich());
//    }
//
//    public void testWeb() throws Exception {
//        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
//        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
//        RepliziereWebRequestType repliziereWebRequestType = new RepliziereWebRequestType();
//        repliziereWebRequestType.setMessageId(UUID.randomUUID().toString());
//        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
//        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
//        wsPartnerIdType.setId("");
//        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
//        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
//        wsPartnerReferenzType.setExterneReferenz(wsExterneReferenzType);
//        repliziereWebRequestType.setPartnerReferenz(wsPartnerReferenzType);
//        WsWebReferenzType wsWebReferenzType = new WsWebReferenzType();
//        WsWebIdType wsWebIdType = new WsWebIdType();
//        wsWebIdType.setId(UUID.randomUUID().toString());
//        wsWebReferenzType.setWebId(wsWebIdType);
//        repliziereWebRequestType.setWebReferenz(wsWebReferenzType);
//        WsWebStrukturType wsWebStrukturType = new WsWebStrukturType();
//        WsWebType wsWebType = new WsWebType();
//        wsWebType.setBenutzerName("fischer.fischi@gmail.com");
//        wsWebType.setUrl("");
//        //wsEmailType.setIsDefStandard();
//        //wsEmailType.setIsStandard();
//        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
//        wsMutationsgrundIdType.setId("e2212371-1235-4a37-a7ef-41ffd865f4d1");
//        wsWebType.setMutationsgrundId(wsMutationsgrundIdType);
//        wsWebType.setStateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-03-04"));
//        wsWebType.setStateUpto(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        wsWebStrukturType.getWebStates().add(wsWebType);
//        WsWebDefIdType wsWebDefIdType = new WsWebDefIdType();
//        wsWebDefIdType.setId("0b34ad50-89af-4c63-824d-e960c1411524");
//        wsWebStrukturType.setWebDefId(wsWebDefIdType);
//        wsWebStrukturType.setGueltAb(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-03-04"));
//        wsWebStrukturType.setGueltBis(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        WsKommVerbVerwendungType wsKommVerbVerwendungType = new WsKommVerbVerwendungType();
//        WsErlaubnisCodeType wsErlaubnisCodeType = new WsErlaubnisCodeType();
//        wsErlaubnisCodeType.setCodeId("-64830");
//        wsKommVerbVerwendungType.setErlaubnis(wsErlaubnisCodeType);
//        WsErlaubnisartCodeType wsErlaubnisartCodeType = new WsErlaubnisartCodeType();
//        wsErlaubnisartCodeType.setCodeId("-64868");
//        wsKommVerbVerwendungType.setErlaubnisart(wsErlaubnisartCodeType);
//        WsKommVerbTypCodeType wsKommVerbTypCodeType = new WsKommVerbTypCodeType();
//        wsKommVerbTypCodeType.setCodeId("");
//        wsKommVerbVerwendungType.setVerbindungstyp(wsKommVerbTypCodeType);
//        wsWebStrukturType.setKommVerbVerwendung(wsKommVerbVerwendungType);
//        repliziereWebRequestType.setWebStruktur(wsWebStrukturType);
//
//        // Generate SAML assertion & response and add it to the port
//        WsConfiguration wsConfiguration = new WsConfiguration();
//        SyriusConsumer.prepareWs(partnerReplikationExpServicePortType, wsConfiguration.getConfiguration(), syriusConfig);
//
//        RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereWeb(repliziereWebRequestType);
//        Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
//        System.out.println("Response:  " + responseType.getReplikationErfolgreich());
//    }
//
//    @Test
//    public void testRolle() throws Exception {
//        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
//        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
//        ReplizierePartnerrolleRequestType partnerrolleRequestType = new ReplizierePartnerrolleRequestType();
//        partnerrolleRequestType.setMessageId(UUID.randomUUID().toString());
//        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
//        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
//        //wsPartnerIdType.setId(UUID.randomUUID().toString());
//        wsPartnerIdType.setId("007e1d79-f1f4-45f7-b9d2-e088b8b311d1");
//        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
//        partnerrolleRequestType.setPartnerReferenz(wsPartnerReferenzType);
//        WsPartnerrolleReferenzType wsPartnerrolleReferenzType = new WsPartnerrolleReferenzType();
//        WsPartnerrolleIdType wsPartnerrolleIdType = new WsPartnerrolleIdType();
//        WsPartnerrolleDefIdType wsPartnerrolleDefIdType = new WsPartnerrolleDefIdType();
//        wsPartnerrolleDefIdType.setId("74423f70-7a7e-4c3b-9e45-fb12b64c97cf");
////        String rolleID = getRolle(wsPartnerIdType.getId(), wsPartnerrolleDefIdType.getId());
////        if (rolleID != null){
////            wsPartnerrolleIdType.setId(rolleID);
////        }else{
//        wsPartnerrolleIdType.setId(UUID.randomUUID().toString());
//        // }
//
//        wsPartnerrolleReferenzType.setPartnerrolleId(wsPartnerrolleIdType);
//        partnerrolleRequestType.setPartnerrolleReferenz(wsPartnerrolleReferenzType);
//        WsPartnerrolleStrukturType wsPartnerrolleStrukturType = new WsPartnerrolleStrukturType();
//        wsPartnerrolleStrukturType.setGueltAb(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-05-05"));
//        wsPartnerrolleStrukturType.setGueltBis(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        WsPartnerrolleType wsPartnerrolleType = new WsPartnerrolleType();
//        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
//        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");
//        wsPartnerrolleType.setMutationsgrundId(wsMutationsgrundIdType);
//        wsPartnerrolleType.setStateFrom(DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-05-05"));
//        wsPartnerrolleType.setStateUpto(DatatypeFactory.newInstance().newXMLGregorianCalendar("3000-01-01"));
//        wsPartnerrolleStrukturType.getPartnerrolleStates().add(wsPartnerrolleType);
//
//        wsPartnerrolleStrukturType.setPartnerrolleDefId(wsPartnerrolleDefIdType);
////        wsPartnerrolleStrukturType.setExterneVersion(202004061400L);
//        partnerrolleRequestType.setPartnerrolleStruktur(wsPartnerrolleStrukturType);
//
//        // Generate SAML assertion & response and add it to the port
//        WsConfiguration wsConfiguration = new WsConfiguration();
//        SyriusConsumer.prepareWs(partnerReplikationExpServicePortType, wsConfiguration.getConfiguration(), syriusConfig);
//
//        RepliziereResponseType responseType = partnerReplikationExpServicePortType.replizierePartnerrolle(partnerrolleRequestType);
//        Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
//        System.out.println("Response:  " + responseType.getReplikationErfolgreich());
//    }
//
//    @Test
//    public void test() {
//        PartnerrolleServicePortType partnerrolleServicePortType = syriusConfig.getPartnerrolleService().getPartnerrolleServiceV1();
//        ((BindingProvider) partnerrolleServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://apibridge-partnermgmt-hmf-dss2-02.tst.oc.hermes.scsyr.hev.cloud//apibridge-partnermgmt/PartnerrolleService_v1");
//
//    }
//
//}
