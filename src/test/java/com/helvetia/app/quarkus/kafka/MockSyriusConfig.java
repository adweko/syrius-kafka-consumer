package com.helvetia.app.quarkus.kafka;

import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.PartnerReplikationExpServiceV0;
import com.helvetia.app.quarkus.kafka.config.SSLConfig;
import com.helvetia.app.quarkus.kafka.config.SyriusConfig;
import io.quarkus.test.Mock;

import javax.enterprise.context.ApplicationScoped;

@Mock
@ApplicationScoped
public class MockSyriusConfig extends SyriusConfig {
    @Override
    public PartnerReplikationExpServiceV0 getReplikationExpService() {
        return super.getReplikationExpService();
    }

    @Override
    public SSLConfig getSslConfig() {
        return super.getSslConfig();
    }
}
