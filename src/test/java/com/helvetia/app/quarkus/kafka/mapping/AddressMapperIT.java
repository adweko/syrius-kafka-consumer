package com.helvetia.app.quarkus.kafka.mapping;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.PartnerReplikationExpServicePortType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereDomiziladresseRequestType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereResponseType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereZusatzadresseRequestType;
import com.helvetia.app.quarkus.kafka.SyriusConsumer;
import com.helvetia.app.quarkus.kafka.config.SyriusConfig;
import com.helvetia.app.quarkus.kafka.config.WsConfiguration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDEntry;
import io.quarkus.test.junit.QuarkusTest;
import org.apache.avro.Schema;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import javax.inject.Inject;
import javax.xml.ws.BindingProvider;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@QuarkusTest
public class AddressMapperIT {

    @Inject
    SyriusConfig syriusConfig;

    Logger logger = LoggerFactory.getLogger(AddressMapperIT.class);
    JsonAvroConverter converter = new JsonAvroConverter();


    @Test
    public void testRepliziereDomizilAdresse() throws Exception {
        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
        Path avsc = Paths.get("src/main/resources/avro/changes.avsc");
        File example = new File(AddressMapperIT.class.getClassLoader().getResource("testFiles/address/domicileaddress.json").getFile());
        Schema schema = new Schema.Parser().parse(avsc.toFile());
        DocumentContext context = JsonPath.parse(example);
       changeMdmResponse changeMessage = converter.convertToSpecificRecord(context.jsonString().getBytes(), changeMdmResponse.class, schema);
        List<hasChannelIDEntry> channelList = changeMessage.getBusinessPartnerID().get(0).getHasChannelID();
        for (int i = 0; i < channelList.size(); i++) {
           hasChannelIDEntry channel = channelList.get(i);
            RepliziereDomiziladresseRequestType domiziladresseRequestType = syriusConfig.getAddressMapper().mapDomizilAdresse(channel);

            // Generate SAML assertion & response and add it to the port
            WsConfiguration wsConfiguration = new WsConfiguration();
            SyriusConsumer.prepareWs(partnerReplikationExpServicePortType, wsConfiguration.getConfiguration(), syriusConfig);

            RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereDomiziladresse(domiziladresseRequestType);
            Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
            System.out.println("Response:  " + responseType.getReplikationErfolgreich());
        }
    }


    @Test
    public void testReplizierePostAdresse() throws Exception {
        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
        Path avsc = Paths.get("src/main/resources/avro/changes.avsc");
        File example = new File(AddressMapperIT.class.getClassLoader().getResource("testFiles/address/postaddress.json").getFile());
        Schema schema = new Schema.Parser().parse(avsc.toFile());
        DocumentContext context = JsonPath.parse(example);
       changeMdmResponse changeMessage = converter.convertToSpecificRecord(context.jsonString().getBytes(), changeMdmResponse.class, schema);
        List<hasChannelIDEntry> channelList = changeMessage.getBusinessPartnerID().get(0).getHasChannelID();
        for (int i = 0; i < channelList.size(); i++) {
            hasChannelIDEntry channel = channelList.get(i);
            RepliziereZusatzadresseRequestType zusatzadresseRequestType = syriusConfig.getAddressMapper().mapZusatzAdresse(channel);
            RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereZusatzadresse(zusatzadresseRequestType);
            Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
            System.out.println("Response:  " + responseType.getReplikationErfolgreich());
        }
    }

    @Test
    public void testRepliziereZusatzAdresse() throws Exception {
        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
        Path avsc = Paths.get("src/main/resources/avro/changes.avsc");
        File example = new File(AddressMapperIT.class.getClassLoader().getResource("testFiles/address/additionaladdress.json").getFile());
        Schema schema = new Schema.Parser().parse(avsc.toFile());
        DocumentContext context = JsonPath.parse(example);
        changeMdmResponse changeMessage = converter.convertToSpecificRecord(context.jsonString().getBytes(), changeMdmResponse.class, schema);
        List<hasChannelIDEntry> channelList = changeMessage.getBusinessPartnerID().get(0).getHasChannelID();
        for (int i = 0; i < channelList.size(); i++) {
            hasChannelIDEntry channel = channelList.get(i);
            RepliziereZusatzadresseRequestType zusatzadresseRequestType = syriusConfig.getAddressMapper().mapZusatzAdresse(channel);
            RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereZusatzadresse(zusatzadresseRequestType);
            Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
            System.out.println("Response:  " + responseType.getReplikationErfolgreich());
        }
    }
}
