package com.helvetia.app.quarkus.kafka.mapping;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.ApiFaultMessage;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.PartnerReplikationExpServicePortType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereNatPersonRequestType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.ReplizierePartnerrolleRequestType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereResponseType;
import com.helvetia.app.quarkus.kafka.SyriusConsumer;
import com.helvetia.app.quarkus.kafka.config.SyriusConfig;
import com.helvetia.app.quarkus.kafka.config.WsConfiguration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import inboundTrafoZepasUnderwriting.json.Response.RowSchema.hasUnderwritingCodeID.hasUnderwritingCodeIDEntry;
import io.quarkus.test.junit.QuarkusTest;
import lombok.Data;
import org.apache.avro.Schema;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import javax.inject.Inject;
import javax.xml.ws.BindingProvider;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@QuarkusTest
@Data
public class NatPersonMapperIT {

    Logger logger = LoggerFactory.getLogger(NatPersonMapperIT.class);
    JsonAvroConverter converter = new JsonAvroConverter();

    @Inject
    SyriusConfig syriusConfig;


    @Test
    public void natPersonTest() throws Exception {
        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
        Path avsc = Paths.get("src/main/resources/avro/changes.avsc");
        File example = new File(NatPersonMapperIT.class.getClassLoader().getResource("testFiles/natPerson.json").getFile());
        Schema schema = new Schema.Parser().parse(avsc.toFile());
        DocumentContext context = JsonPath.parse(example);
        changeMdmResponse changeMessage = converter.convertToSpecificRecord(context.jsonString().getBytes(), changeMdmResponse.class, schema);

        // che0bmc, 20200721, use a global epoch to modify the timestamp
        String mdmTimeStamp = changeMessage.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0)
                .getBITEMPMDMTimestamp().toString();
        long mdmModifiedEpoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4, 6)),
                Integer.parseInt(mdmTimeStamp.substring(6, 8)), Integer.parseInt(mdmTimeStamp.substring(8, 10)),
                Integer.parseInt(mdmTimeStamp.substring(10, 12)), Integer.parseInt(mdmTimeStamp.substring(12, 14)),
                Integer.parseInt(mdmTimeStamp.substring(14, 17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;

        RepliziereNatPersonRequestType natPersonRequestType = syriusConfig.getNatPersonMapper().mapNatPerson(changeMessage, mdmModifiedEpoch);

        // Generate SAML assertion & response and add it to the port
        WsConfiguration wsConfiguration = new WsConfiguration();
        SyriusConsumer.prepareWs(partnerReplikationExpServicePortType, wsConfiguration.getConfiguration(), syriusConfig);

        RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereNatPerson(natPersonRequestType);
        Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
        System.out.println("Response:  " + responseType.getReplikationErfolgreich());
    }

    @Test
    public void testUWCode() throws Exception {
        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
        Path avsc = Paths.get("src/main/resources/avro/changes.avsc");
        File example = new File(AddressMapperIT.class.getClassLoader().getResource("testFiles/roles/uwcode.json").getFile());
        Schema schema = new Schema.Parser().parse(avsc.toFile());
        DocumentContext context = JsonPath.parse(example);
        changeMdmResponse changeMessage = converter.convertToSpecificRecord(context.jsonString().getBytes(), changeMdmResponse.class, schema);
        List<hasUnderwritingCodeIDEntry> uwList = changeMessage.getBusinessPartnerID().get(0).getHasUnderwritingCodeID();
        for (int i = 0; i < uwList.size(); i++) {
            hasUnderwritingCodeIDEntry uwCode = uwList.get(i);
            List<ReplizierePartnerrolleRequestType> uwRequestTypes = syriusConfig.getNatPersonMapper().mapUwCodeRolle(uwCode);
            uwRequestTypes.forEach(uwRequestType -> {
                RepliziereResponseType responseType = null;
                try {
                    responseType = partnerReplikationExpServicePortType.replizierePartnerrolle(uwRequestType);
                } catch (ApiFaultMessage apiFaultMessage) {
                    throw new RuntimeException(apiFaultMessage);
                }
                Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
                System.out.println("Response:  " + responseType.getReplikationErfolgreich());
            });
        }
    }



//    @Test
//    public void testDiplomat() throws Exception {
//        rolleTest("testFiles/roles/diplomat.json");
//    }
//
//    @Test
//    public void testEmployee() throws Exception {
//        rolleTest("testFiles/roles/employee.json");
//    }
//
//    public void rolleTest(String file) throws Exception {
//        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
//        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
//        Path avsc = Paths.get("src/main/resources/avro/changes.avsc");
//        File example = new File(NatPersonMapperTest.class.getClassLoader().getResource(file).getFile());
//        Schema schema = new Schema.Parser().parse(avsc.toFile());
//        DocumentContext context = JsonPath.parse(example);
//        changeMdmResponse changeMessage = converter.convertToSpecificRecord(context.jsonString().getBytes(), changeMdmResponse.class, schema);
//        ReplizierePartnerrolleRequestType partnerrolleRequestType = syriusConfig.getNatPersonMapper().mapPersonenRolle(changeMessage);
//        RepliziereResponseType responseType = partnerReplikationExpServicePortType.replizierePartnerrolle(partnerrolleRequestType);
//        Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
//        System.out.println("Response:  " + responseType.getReplikationErfolgreich());
//    }
}
