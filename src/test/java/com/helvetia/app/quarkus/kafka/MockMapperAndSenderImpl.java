package com.helvetia.app.quarkus.kafka;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.PartnerReplikationExpServicePortType;
import com.helvetia.app.quarkus.kafka.config.SyriusConfig;
import com.helvetia.app.quarkus.kafka.config.WsConfiguration;
import com.helvetia.app.quarkus.kafka.logging.LogHelper;
import com.helvetia.app.quarkus.kafka.streams.MapperAndSender;
import com.helvetia.app.quarkus.kafka.streams.exception.dlq.MappingException;
import com.helvetia.app.quarkus.kafka.streams.model.ResultOrExceptionWithInput;
import io.quarkus.test.Mock;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.ws.BindingProvider;

@Mock
@ApplicationScoped
public class MockMapperAndSenderImpl implements MapperAndSender<changeMdmResponse, Boolean> {
    @Inject
    SyriusConfig syriusConfig;

    @Inject
    WsConfiguration wsConfiguration;

    PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = null;

    @Inject
    LogHelper logHelper;

    @Override
    public ResultOrExceptionWithInput<changeMdmResponse, Boolean> mapAndSend(changeMdmResponse v) {

        try {
            if (partnerReplikationExpServicePortType == null) {
                partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
                ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
            }

            String logTransactionId = "";
            logTransactionId = LogHelper.getTechMdmPartnerID(v.getBusinessPartnerID().get(0))
                    + "|" + LogHelper.getPartnerId(v.getBusinessPartnerID().get(0), LogHelper.ZEPASPARTNERKEY)
                    + "|" + LogHelper.getPartnerId(v.getBusinessPartnerID().get(0), LogHelper.SYRIUSPARTNERKEY);

            logHelper.logInfo(logTransactionId, "value: " + v);
            PartnerSyrius partnerSyrius;
            try {
                partnerSyrius = PartnerSyrius.fromChangeMdmResponse(v, syriusConfig);
            } catch (Exception e) {
                throw new MappingException(e);
            }
            final boolean success = true; //SyriusConsumer.sendPartnerToSyrius(partnerSyrius, syriusConfig, partnerReplikationExpServicePortType, logTransactionId, wsConfiguration.getConfiguration(), logHelper);
            return new ResultOrExceptionWithInput<>(success);
        } catch (Exception e) {
            return new ResultOrExceptionWithInput<>(v, e);
        }
    }
}
