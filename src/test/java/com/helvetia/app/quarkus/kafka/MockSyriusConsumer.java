package com.helvetia.app.quarkus.kafka;

import io.quarkus.test.Mock;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import java.util.Properties;

@Mock
@ApplicationScoped
@Data
@Slf4j
public class MockSyriusConsumer extends SyriusConsumer {

    @Override
    public void startConsuming() {
        log.info("MockSyriusConsumer");
    }


    @Override
    public Properties configure() {

        return new Properties();
    }



}
