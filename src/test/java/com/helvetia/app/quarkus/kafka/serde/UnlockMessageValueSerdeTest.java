package com.helvetia.app.quarkus.kafka.serde;

import com.helvetia.app.quarkus.kafka.streams.model.UnlockMessageValue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UnlockMessageValueSerdeTest {

    @Test
    public void roundTrip() {
        String raw = "{\"TECHMDMPartnerID\":\"E107B98D-5083-436D-9513-D626B9C475BC\",\"RepublishedAt\":\"20200819152152623\"}";
        final UnlockMessageValue unlockMessageValue = UnlockMessageValue.fromJsonString(raw);
        Assertions.assertEquals("E107B98D-5083-436D-9513-D626B9C475BC", unlockMessageValue.getPartnerId().getValue());
    }

   

}
