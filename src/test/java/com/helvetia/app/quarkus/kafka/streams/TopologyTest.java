package com.helvetia.app.quarkus.kafka.streams;

import ch.mdm.syrius.dlq.DLQMessages;
import changePartner.com.helvetia.custom.changeMdmResponse;
import changePartner.json.Response.Row;
import com.helvetia.app.quarkus.kafka.MockMapperAndSenderImpl;
import com.helvetia.app.quarkus.kafka.streams.exception.dlq.MappingException;
import com.helvetia.app.quarkus.kafka.streams.exception.stop.SendingFailedException;
import com.helvetia.app.quarkus.kafka.streams.model.*;
import com.helvetia.app.quarkus.kafka.streams.processor.UnlockAndMaybeReprocessDlqTransformer;
import inboundTrafoZepasLegalPerson.json.Response.RowSchema.isBusinessPartner.isBusinessPartnerEntry;
import inboundTrafoZepasNaturalPerson.json.Response.RowSchema.isNaturalPerson.isNaturalPersonEntry;
import io.confluent.kafka.schemaregistry.testutil.MockSchemaRegistry;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectSpy;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.processor.StateStore;
import org.apache.kafka.streams.state.KeyValueStore;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import javax.inject.Inject;
import java.time.Instant;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertThrows;

@QuarkusTest
public class TopologyTest {

    @ConfigProperty(name = "ch.helvetia.kafka.input.topic")
    String inputTopicName;

    @ConfigProperty(name = "ch.helvetia.kafka.unlock.topic")
    String unlockTopicName;

    @Inject
    Topology topology;

    @InjectSpy
    MockMapperAndSenderImpl mapperAndSender;

    TopologyTestDriver testDriver;

    private static final String SCHEMA_REGISTRY_SCOPE = "schemaregmock";
    private static final String MOCK_SCHEMA_REGISTRY_URL = "mock://" + SCHEMA_REGISTRY_SCOPE;

    TestInputTopic<String, changeMdmResponse> chMdmChangeTopic;
    TestInputTopic<String, String> chMDmUnlockTopic;

    KeyValueStore<TechMDMPartnerID, DLQMessages> dlqStore;
    KeyValueStore<TechMDMPartnerID, UnlockMessageValue> republishedStore;

    @AfterEach
    public void tearDown() {
        testDriver.getAllStateStores().values().forEach(StateStore::flush);
        testDriver.close();
        MockSchemaRegistry.dropScope(SCHEMA_REGISTRY_SCOPE);
    }


    @BeforeEach
    public void setUp() {
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "testApplicationId");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");
        config.put("schema.registry.url", MOCK_SCHEMA_REGISTRY_URL);
        testDriver = new TopologyTestDriver(topology, config);

        Serde<changeMdmResponse> avroChangeMdmResponseSerde = new SpecificAvroSerde<>();

        // Configure Serdes to use the same mock schema registry URL
        Map<String, String> serdeConfig = Map.of(
                AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, MOCK_SCHEMA_REGISTRY_URL);
        avroChangeMdmResponseSerde.configure(serdeConfig, false);

        chMdmChangeTopic = testDriver.createInputTopic(inputTopicName, new StringSerializer(), avroChangeMdmResponseSerde.serializer());
        chMDmUnlockTopic = testDriver.createInputTopic(unlockTopicName, new StringSerializer(), new StringSerializer());

        dlqStore = testDriver.getKeyValueStore(TopologyProducer.DLQ_STORE_NAME);
        republishedStore = testDriver.getKeyValueStore(TopologyProducer.REPUBLISHED_STORE_NAME);

    }


    @Test
    public void regularProcessing() {
        final changeMdmResponse response = buildValidDublette();
        chMdmChangeTopic.pipeInput(response);
        Assertions.assertEquals(0, dlqStore.approximateNumEntries());
    }

    @Test
    public void messageWithMissingFieldsGoesToDLQ() {
        String partnerId = "123";
        changeMdmResponse inputMsg = buildRelevantMinimalNatPerson(partnerId);
        chMdmChangeTopic.pipeInput(inputMsg);
        final DLQMessages dlqMessage = dlqStore.get(TechMDMPartnerID.fromRawValueString(partnerId));
        Assertions.assertNotNull(dlqMessage);
        System.out.println(dlqMessage);
        Assertions.assertEquals(inputMsg, dlqMessage.getMessages().get(0).getOriginalMessage());
    }

    @Test
    public void unlockMessageClearsDlqsOldRecords() {
        String partnerId = "123";
        final TechMDMPartnerID techMDMPartnerID = TechMDMPartnerID.fromRawValueString(partnerId);
        changeMdmResponse inputMsg = buildRelevantMinimalNatPerson(partnerId);
        Instant t1 = Instant.now();
        chMdmChangeTopic.pipeInput(inputMsg, t1);
        final DLQMessages dlqMessage = dlqStore.get(TechMDMPartnerID.fromRawValueString(partnerId));
        Assertions.assertEquals(inputMsg, dlqMessage.getMessages().get(0).getOriginalMessage());

        Instant t2 = t1.plusSeconds(10);
        final UnlockMessageValue unlockMessageValue = UnlockMessageValue.create(techMDMPartnerID, new MDMTimestamp(t2));
        chMDmUnlockTopic.pipeInput(createDummyUnlockKey(techMDMPartnerID), unlockMessageValue.toJsonString());
        testDriver.advanceWallClockTime(UnlockAndMaybeReprocessDlqTransformer.REPROCESS_FREQUENCY);

        Assertions.assertEquals(0, dlqStore.approximateNumEntries());
        Mockito.verify(mapperAndSender, Mockito.times(1)).mapAndSend(Mockito.any());
    }

    @Test
    public void mappingErrorsGoToDLQ() {
        changeMdmResponse response = buildValidDublette();
        final TechMDMPartnerID techMDMPartnerID = TechMDMPartnerID.fromRawValueString(response.getBusinessPartnerID().get(0).getTECHMDMPartnerID().toString());
        // make invalid
        response.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).setBITEMPMDMTimestamp("INVALID_TIMESTAMP");
        chMdmChangeTopic.pipeInput(response);
        Assertions.assertEquals(1, dlqStore.approximateNumEntries());
        DLQMessages dlqMessage = dlqStore.get(techMDMPartnerID);
        Assertions.assertEquals(response, dlqMessage.getMessages().get(0).getOriginalMessage());
    }

    @Test
    public void validMessagesReceivedTooEarlyInDlqAreReprocessed() {
        Instant t1 = Instant.now();
        Instant t2 = t1.plusSeconds(10);
        Instant t3 = t2.plusSeconds(10);
        changeMdmResponse invalidMessage = buildValidDublette();
        final TechMDMPartnerID techMDMPartnerID = TechMDMPartnerID.fromRawValueString(invalidMessage.getBusinessPartnerID().get(0).getTECHMDMPartnerID().toString());
        // make invalid
        invalidMessage.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).setBITEMPMDMTimestamp("INVALID_TIMESTAMP");
        chMdmChangeTopic.pipeInput(invalidMessage, t1);
        // called once, but threw MappingException
        Mockito.verify(mapperAndSender, Mockito.times(1)).mapAndSend(Mockito.any());
        // went to DLQ
        Assertions.assertEquals(1, dlqStore.approximateNumEntries());
        DLQMessages dlqMessage = dlqStore.get(techMDMPartnerID);
        Assertions.assertEquals(invalidMessage, dlqMessage.getMessages().get(0).getOriginalMessage());

        // now pipe a valid msg for same Partner at t3, simulate arrival prior to unlock at t2. Should got to DLQ as well
        changeMdmResponse validMessage = buildValidDublette();
        chMdmChangeTopic.pipeInput(validMessage, t3);

        dlqMessage = dlqStore.get(techMDMPartnerID);
        Assertions.assertEquals(validMessage, dlqMessage.getMessages().get(1).getOriginalMessage());

        final UnlockMessageValue unlockMessageValue = UnlockMessageValue.create(techMDMPartnerID, new MDMTimestamp(t2));
        chMDmUnlockTopic.pipeInput(createDummyUnlockKey(techMDMPartnerID), unlockMessageValue.toJsonString());

        Assertions.assertTrue(republishedStore.get(techMDMPartnerID) != null);
        // map and send is not called directly, need to wait for punctuator
        Mockito.verify(mapperAndSender, Mockito.times(1)).mapAndSend(Mockito.any());
        // advancing time to call punctuator
        testDriver.advanceWallClockTime(UnlockAndMaybeReprocessDlqTransformer.REPROCESS_FREQUENCY.plusSeconds(1));

        // now map and send was called once more for valid partner
        Mockito.verify(mapperAndSender, Mockito.times(2)).mapAndSend(Mockito.any());
        // and dlq is empty
        Assertions.assertNull(dlqStore.get(techMDMPartnerID));

    }

    @Test
    public void errorDuringReprocessingDlqCanBeHandledByRepublishingPartnerAgain() {
        Instant t1 = Instant.now();
        Instant t2 = t1.plusSeconds(10);
        Instant t3 = t2.plusSeconds(10);
        changeMdmResponse invalidMessage = buildValidDublette();
        final TechMDMPartnerID techMDMPartnerID = TechMDMPartnerID.fromRawValueString(invalidMessage.getBusinessPartnerID().get(0).getTECHMDMPartnerID().toString());
        // make invalid
        invalidMessage.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).setBITEMPMDMTimestamp("INVALID_TIMESTAMP");
        chMdmChangeTopic.pipeInput(invalidMessage, t1);
        // called once, but threw MappingException
        Mockito.verify(mapperAndSender, Mockito.times(1)).mapAndSend(Mockito.any());
        // went to DLQ
        Assertions.assertEquals(1, dlqStore.approximateNumEntries());
        DLQMessages dlqMessage = dlqStore.get(techMDMPartnerID);
        Assertions.assertEquals(invalidMessage, dlqMessage.getMessages().get(0).getOriginalMessage());

        // now pipe another invalid msg for same Partner at t3, simulate arrival prior to unlock at t2. Should go to DLQ as well
        changeMdmResponse secondInvalidMessage = buildValidDublette();
        secondInvalidMessage.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).setBITEMPMDMTimestamp("SECOND_sINVALID_TIMESTAMP");
        chMdmChangeTopic.pipeInput(secondInvalidMessage, t3);

        dlqMessage = dlqStore.get(techMDMPartnerID);
        Assertions.assertEquals(secondInvalidMessage, dlqMessage.getMessages().get(1).getOriginalMessage());

        // make mock return failure

        Mockito.when(mapperAndSender.mapAndSend(Mockito.any())).thenThrow(new SendingFailedException("mock sending failure"));

        final UnlockMessageValue unlockMessageValue = UnlockMessageValue.create(techMDMPartnerID, new MDMTimestamp(t2));

        chMDmUnlockTopic.pipeInput(createDummyUnlockKey(techMDMPartnerID), unlockMessageValue.toJsonString());
        testDriver.advanceWallClockTime(UnlockAndMaybeReprocessDlqTransformer.REPROCESS_FREQUENCY.plusSeconds(1));
        // now map and send was called once more for the second invalid partner
        Mockito.verify(mapperAndSender, Mockito.times(2)).mapAndSend(Mockito.any());
        // the dlq is still full
        Assertions.assertTrue(dlqStore.get(techMDMPartnerID).getMessages().size() == 2);

        //sending another unlock message at t4 will clear the dlq
        Instant t4 = t3.plusSeconds(10);
        final UnlockMessageValue unlockMessageValueT4 = UnlockMessageValue.create(techMDMPartnerID, new MDMTimestamp(t4));

        chMDmUnlockTopic.pipeInput(createDummyUnlockKey(techMDMPartnerID), unlockMessageValueT4.toJsonString());
        testDriver.advanceWallClockTime(UnlockAndMaybeReprocessDlqTransformer.REPROCESS_FREQUENCY.plusSeconds(1));
        // now dlq is clear
        Assertions.assertNull(dlqStore.get(techMDMPartnerID));

    }

    @Test
    public void unknownErrorsLeadToCrash() {
        Mockito.when(mapperAndSender.mapAndSend(Mockito.any())).thenReturn(
                new ResultOrExceptionWithInput<>(null, new Exception("something"))
        );
        Exception exception = assertThrows(Exception.class, () -> {
            changeMdmResponse inputMessage = buildValidDublette();
            chMdmChangeTopic.pipeInput(inputMessage);
        });
    }

    @Test
    public void SendingFailedLeadsToCrash() {
        Mockito.when(mapperAndSender.mapAndSend(Mockito.any())).thenReturn(
                new ResultOrExceptionWithInput<>(null, new SendingFailedException("send failed"))
        );
        Exception exception = assertThrows(Exception.class, () -> {
            changeMdmResponse inputMessage = buildValidDublette();
            chMdmChangeTopic.pipeInput(inputMessage);
        });
        Assert.assertTrue(exception instanceof SendingFailedException);
    }

    @Test
    public void MappingFailedGoesToDLQ() {
        changeMdmResponse inputMessage = buildValidDublette();
        Mockito.when(mapperAndSender.mapAndSend(Mockito.any())).thenReturn(
                new ResultOrExceptionWithInput<>(inputMessage, new MappingException("mapping failed"))
        );
        chMdmChangeTopic.pipeInput(inputMessage);
        Assertions.assertEquals(1, dlqStore.approximateNumEntries());
    }


    private static changeMdmResponse buildRelevantMinimalNatPerson(String partnerId) {
        return changeMdmResponse.newBuilder().setBusinessPartnerID(Collections.singletonList(
                Row.newBuilder()
                        .setTECHMDMPartnerID(partnerId)
                        .setIsBusinessPartner(Collections.singletonList(
                                isBusinessPartnerEntry.newBuilder()
                                        .setTECHSourceSystem("NOT_SYRIUS")
                                        .build()
                        ))
                        .setIsNaturalPerson(Collections.singletonList(
                                isNaturalPersonEntry.newBuilder()
                                        .setTECHMDMPartnerID(partnerId)
                                        .build()
                        ))
                        .build()
        )).build();
    }


    private static changeMdmResponse buildValidDublette() {
        final String msg = "{\"BusinessPartnerID\": [{\"TECHInternalPID_Location\": null, \"TECHInternalPID_Channel\": null, \"TECHInternalPID_NaturalPerson\": null, \"TECHInternalPID_LegalPerson\": null, \"TECHInternalPID_Underwriting\": null, \"TECHInternalPID_Duplicates\": null, \"TECHInternalPID\": null, \"hasChannelID\": [], \"isLegalPerson\": [], \"isNaturalPerson\": [{\"NaturalPerson\": [{\"academicDegree\": null, \"alienStatus\": null, \"creditRating\": null, \"dateOfBirth\": {\"day\": 1, \"month\": 1, \"year\": 2000}, \"dateOfDeath\": null, \"discountCategory\": null, \"DNA\": null, \"driverLicenseID\": null, \"dutyToContribute\": null, \"employmentGrade\": null, \"employmentStatus\": null, \"function\": null, \"goalAndNeed\": null, \"healthData\": null, \"healthInformation\": null, \"hobby\": null, \"homeTown\": null, \"inEducation\": null, \"isBiometric\": null, \"language\": \"D\", \"maritalStatus\": null, \"membership\": null, \"name\": \"Dublette\", \"nameAtBirth\": null, \"nationalIDCard\": null, \"nationality\": null, \"noAds\": null, \"note\": null, \"passport\": null, \"pets\": null, \"placeOfBirth\": null, \"religion\": null, \"residencePermit\": null, \"residentPermitValidTo\": null, \"salutation\": \"01\", \"secondName\": null, \"securityClassification\": null, \"sex\": \"1\", \"signature\": null, \"skills\": null, \"socialAssistanceRecipients\": null, \"socialSecurityNumber\": null, \"surname\": \"Die\", \"TECHExternalPID\": \"5692275f-60a7-41cc-9227-5f60a7f1ccb4\", \"TECHInternalPID\": \"522F851C-B1F1-4B23-B929-779CA2004995\", \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"thirdName\": null, \"domicileCity\": null, \"domicileCountry\": null, \"domicileHouseNumber\": null, \"domicileRegion\": null, \"domicileStreet\": null, \"domicileZIP\": null, \"_stp_id\": \"NaturalPerson:651289D8924F56F61B1E7AB20C81B111\", \"_stp_label\": \"651289D8924F56F61B1E7AB20C81B111\", \"_stp_type\": \"NaturalPerson\", \"employee\": null, \"otherTitle\": null, \"profession\": null, \"TECHUser\": \"CHTI-MDM\", \"employeeUser\": null}], \"BITEMPMDMTimestamp\": \"20200619130552742\", \"BITEMPSourceTimestamp\": \"20200619150548781\", \"BITEMPValidFrom\": {\"day\": 19, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 31, \"month\": 12, \"year\": 9999}, \"TECHExternalPID\": \"5692275f-60a7-41cc-9227-5f60a7f1ccb4\", \"TECHInternalPID\": \"522F851C-B1F1-4B23-B929-779CA2004995\", \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"_stp_id\": \"isNaturalPerson:20200619130552742\", \"_stp_key\": \"20200619130552742\", \"_stp_label\": \"isNaturalPerson\", \"TECHUser\": \"CHTI-MDM\"}], \"hasUnderwritingCodeID\": [], \"isDuplicate\": [{\"BusinessPartnerID\": [{\"TECHMDMPartnerID\": \"8105C5BB-858F-48B6-8E70-1FE88A9272D1\", \"hasIdentificationNumber\": [{\"IdentificationNumber\": [{\"IDNumber\": \"60377811\", \"IDType\": \"1\", \"TECHExternalPID\": \"8383723c-347a-4c71-8372-3c347a2c71e0\", \"TECHInternalPID\": \"CFA765F8-AC45-4AA2-8249-A157F986874E\", \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null, \"TECHUser\": null}], \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200619130137665\", \"BITEMPSourceTimestamp\": \"20200619150132540\", \"BITEMPValidFrom\": {\"day\": 19, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": null, \"TECHExternalPID\": \"8383723c-347a-4c71-8372-3c347a2c71e0\", \"TECHMDMPartnerID\": \"8105C5BB-858F-48B6-8E70-1FE88A9272D1\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"TECHUser\": null}]}], \"BITEMPMDMTimestamp\": \"20200619130552742\", \"BITEMPSourceTimestamp\": \"20200619150548781\", \"BITEMPValidFrom\": {\"day\": 19, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 31, \"month\": 12, \"year\": 9999}, \"TECHExternalPID\": \"5692275f-60a7-41cc-9227-5f60a7f1ccb4\", \"TECHInternalPID\": \"68A5BB70-0B8A-4955-A257-06F51E74FE2A\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"BITEMPDeleted\": \"\"}], \"Status\": null, \"CRUD\": null, \"hasUpdate\": true, \"user_fields\": [], \"Status_Description\": null, \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"TECHUser\": \"CHTI-MDM\", \"isBusinessPartner\": [{\"BusinessPartner\": [{\"encryptionCode_VIP\": null, \"newBusinessPartnerID\": null, \"TECHExternalPID\": \"5692275f-60a7-41cc-9227-5f60a7f1ccb4\", \"TECHInternalPID\": \"522F851C-B1F1-4B23-B929-779CA2004995\", \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"_stp_id\": \"BusinessPartner:A72A8059F91CBD645CD8CC979B4749DE\", \"_stp_label\": \"A72A8059F91CBD645CD8CC979B4749DE\", \"_stp_type\": \"BusinessPartner\", \"TECHUser\": null}], \"BITEMPMDMTimestamp\": \"20200619130552742\", \"BITEMPSourceTimestamp\": \"20200619150548781\", \"TECHExternalPID\": \"5692275f-60a7-41cc-9227-5f60a7f1ccb4\", \"TECHInternalPID\": \"522F851C-B1F1-4B23-B929-779CA2004995\", \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"BITEMPValidFrom\": {\"day\": 19, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 31, \"month\": 12, \"year\": 9999}, \"_stp_id\": \"isBusinessPartner:20200619130552742\", \"_stp_label\": \"A72A8059F91CBD645CD8CC979B4749DE\", \"_stp_key\": \"20200619130552742\", \"TECHUser\": null}], \"TECHExternalPID\": null, \"hasIdentificationNumber\": [{\"IdentificationNumber\": [{\"IDNumber\": \"60377812\", \"IDType\": \"1\", \"_stp_label\": null, \"TECHExternalPID\": \"118ef16d-24b6-4ff5-8ef1-6d24b60ff5f0\", \"TECHInternalPID\": \"7E81A04F-4A6B-4896-900B-722F4A861040\", \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null, \"_stp_id\": null, \"_stp_type\": null}], \"BITEMPMDMTimestamp\": \"20200619130230634\", \"BITEMPSourceTimestamp\": \"20200619150224582\", \"BITEMPValidFrom\": {\"day\": 19, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": null, \"TECHExternalPID\": \"118ef16d-24b6-4ff5-8ef1-6d24b60ff5f0\", \"TECHInternalPID\": null, \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"TECHSourceSystem\": null, \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null}], \"TECHSourceSystem\": \"CH_ZEPAS\", \"Status_Code\": null}]}";
        JsonAvroConverter converter = new JsonAvroConverter();
        return converter.convertToSpecificRecord(msg.getBytes(), changeMdmResponse.class, changeMdmResponse.getClassSchema());
    }

    private String createDummyUnlockKey(TechMDMPartnerID partnerId) {
        return UnlockMessageKey.create(partnerId, null, null, null).toJsonString();
    }
}
