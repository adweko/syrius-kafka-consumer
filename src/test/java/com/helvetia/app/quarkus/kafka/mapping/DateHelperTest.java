package com.helvetia.app.quarkus.kafka.mapping;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

public class DateHelperTest {

    @Test
    public void testConversion() throws DatatypeConfigurationException {
        String sourceTimestamp = "20200618144950121";
        XMLGregorianCalendar cal = DateHelper.gregorianDateFromMDMTimestamp(sourceTimestamp);
        Assertions.assertEquals(2020, cal.getYear());
        Assertions.assertEquals(6, cal.getMonth());
        Assertions.assertEquals(18, cal.getDay());
        Assertions.assertEquals(14, cal.getHour());
        Assertions.assertEquals(49, cal.getMinute());
        Assertions.assertEquals(50, cal.getSecond());
        Assertions.assertEquals(121, cal.getMillisecond());
    }

}
