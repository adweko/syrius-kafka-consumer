package com.helvetia.app.quarkus.kafka.mapping;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.PartnerReplikationExpServicePortType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereEmailRequestType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereResponseType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereTelefonRequestType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereWebRequestType;
import com.helvetia.app.quarkus.kafka.config.SyriusConfig;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDEntry;
import io.quarkus.test.junit.QuarkusTest;
import org.apache.avro.Schema;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import javax.inject.Inject;
import javax.xml.ws.BindingProvider;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@QuarkusTest
public class KommVerMapperIT {

    @Inject
    SyriusConfig syriusConfig;

    Logger logger = LoggerFactory.getLogger(KommVerMapper.class);
    JsonAvroConverter converter = new JsonAvroConverter();

    @Test
    public void testRepliziereEmail() throws Exception {
        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
        Path avsc = Paths.get("src/main/resources/avro/changes.avsc");
        File example = new File(AddressMapperIT.class.getClassLoader().getResource("testFiles/comm/email.json").getFile());
        Schema schema = new Schema.Parser().parse(avsc.toFile());
        DocumentContext context = JsonPath.parse(example);
        changeMdmResponse changeMessage = converter.convertToSpecificRecord(context.jsonString().getBytes(), changeMdmResponse.class, schema);
        List<hasChannelIDEntry> channelList = changeMessage.getBusinessPartnerID().get(0).getHasChannelID();
        for (int i = 0; i < channelList.size(); i++) {
            hasChannelIDEntry channel = channelList.get(i);
            RepliziereEmailRequestType emailRequestType = syriusConfig.getKommVerMapper().mapMail(channel);
            RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereEmail(emailRequestType);
            Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
            System.out.println("Response:  " + responseType.getReplikationErfolgreich());
        }
    }

    @Test
    public void testRepliziereTelephone() throws Exception {
        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
        Path avsc = Paths.get("src/main/resources/avro/changes.avsc");
        File example = new File(AddressMapperIT.class.getClassLoader().getResource("testFiles/comm/telephone.json").getFile());
        Schema schema = new Schema.Parser().parse(avsc.toFile());
        DocumentContext context = JsonPath.parse(example);
        changeMdmResponse changeMessage = converter.convertToSpecificRecord(context.jsonString().getBytes(), changeMdmResponse.class, schema);
        List<hasChannelIDEntry> channelList = changeMessage.getBusinessPartnerID().get(0).getHasChannelID();
        for (int i = 0; i < channelList.size(); i++) {
            hasChannelIDEntry channel = channelList.get(i);
            RepliziereTelefonRequestType telefonRequestType = syriusConfig.getKommVerMapper().mapTelefon(channel);
            RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereTelefon(telefonRequestType);
            Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
            System.out.println("Response:  " + responseType.getReplikationErfolgreich());
        }
    }

    @Test
    public void testRepliziereFax() throws Exception {
        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
        Path avsc = Paths.get("src/main/resources/avro/changes.avsc");
        File example = new File(AddressMapperIT.class.getClassLoader().getResource("testFiles/comm/fax.json").getFile());
        Schema schema = new Schema.Parser().parse(avsc.toFile());
        DocumentContext context = JsonPath.parse(example);
        changeMdmResponse changeMessage = converter.convertToSpecificRecord(context.jsonString().getBytes(), changeMdmResponse.class, schema);
        List<hasChannelIDEntry> channelList = changeMessage.getBusinessPartnerID().get(0).getHasChannelID();
        for (int i = 0; i < channelList.size(); i++) {
            hasChannelIDEntry channel = channelList.get(i);
            RepliziereTelefonRequestType faxRequestType = syriusConfig.getKommVerMapper().mapFax(channel);
            RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereTelefon(faxRequestType);
            Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
            System.out.println("Response:  " + responseType.getReplikationErfolgreich());
        }
    }

    @Test
    public void testRepliziereMobile() throws Exception {
        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
        Path avsc = Paths.get("src/main/resources/avro/changes.avsc");
        File example = new File(AddressMapperIT.class.getClassLoader().getResource("testFiles/comm/mobile.json").getFile());
        Schema schema = new Schema.Parser().parse(avsc.toFile());
        DocumentContext context = JsonPath.parse(example);
        changeMdmResponse changeMessage = converter.convertToSpecificRecord(context.jsonString().getBytes(),changeMdmResponse.class, schema);
        List<hasChannelIDEntry> channelList = changeMessage.getBusinessPartnerID().get(0).getHasChannelID();
        for (int i = 0; i < channelList.size(); i++) {
            hasChannelIDEntry channel = channelList.get(i);
            RepliziereTelefonRequestType mobileRequestType = syriusConfig.getKommVerMapper().mapMobile(channel);
            RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereTelefon(mobileRequestType);
            Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
            System.out.println("Response:  " + responseType.getReplikationErfolgreich());
        }
    }

    @Test
    public void testRepliziereSkype() throws Exception {
        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
        Path avsc = Paths.get("src/main/resources/avro/changes.avsc");
        File example = new File(AddressMapperIT.class.getClassLoader().getResource("testFiles/comm/skype.json").getFile());
        Schema schema = new Schema.Parser().parse(avsc.toFile());
        DocumentContext context = JsonPath.parse(example);
        changeMdmResponse changeMessage = converter.convertToSpecificRecord(context.jsonString().getBytes(), changeMdmResponse.class, schema);
        List<hasChannelIDEntry> channelList = changeMessage.getBusinessPartnerID().get(0).getHasChannelID();
        for (int i = 0; i < channelList.size(); i++) {
            hasChannelIDEntry channel = channelList.get(i);
            RepliziereWebRequestType webRequestType = syriusConfig.getKommVerMapper().mapSkype(channel);
            RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereWeb(webRequestType);
            Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
            System.out.println("Response:  " + responseType.getReplikationErfolgreich());
        }
    }
}
