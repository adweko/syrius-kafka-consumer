package com.helvetia.app.quarkus.kafka.mapping;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.PartnerReplikationExpServicePortType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereJurPersonRequestType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereResponseType;
import com.helvetia.app.quarkus.kafka.SyriusConsumer;
import com.helvetia.app.quarkus.kafka.config.SyriusConfig;
import com.helvetia.app.quarkus.kafka.config.WsConfiguration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import io.quarkus.test.junit.QuarkusTest;
import org.apache.avro.Schema;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import javax.inject.Inject;
import javax.xml.ws.BindingProvider;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@QuarkusTest
public class JurPersonMapperIT {

    Logger logger = LoggerFactory.getLogger(NatPersonMapperIT.class);
    JsonAvroConverter converter = new JsonAvroConverter();

    @Inject
    SyriusConfig syriusConfig;

    @Test
    public void testRepliziereJurPerson() throws Exception {
        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
        Path avsc = Paths.get("src/main/resources/avro/changes.avsc");
        File example = new File(NatPersonMapperIT.class.getClassLoader().getResource("testFiles/jurPerson.json").getFile());
        Schema schema = new Schema.Parser().parse(avsc.toFile());
        DocumentContext context = JsonPath.parse(example);
        changeMdmResponse changeMessage = converter.convertToSpecificRecord(context.jsonString().getBytes(), changeMdmResponse.class, schema);
        RepliziereJurPersonRequestType jurPersonRequestType = syriusConfig.getJurPersonMapper().mapJurPerson(changeMessage);

        // Generate SAML assertion & response and add it to the port
        WsConfiguration wsConfiguration = new WsConfiguration();
        SyriusConsumer.prepareWs(partnerReplikationExpServicePortType, wsConfiguration.getConfiguration(), syriusConfig);

        RepliziereResponseType responseType = partnerReplikationExpServicePortType.repliziereJurPerson(jurPersonRequestType);
        Assertions.assertEquals(1, responseType.getReplikationErfolgreich());
        System.out.println("Response:  " + responseType.getReplikationErfolgreich());
    }
}
