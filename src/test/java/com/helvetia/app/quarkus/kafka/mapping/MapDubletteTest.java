package com.helvetia.app.quarkus.kafka.mapping;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.helvetia.app.quarkus.kafka.PartnerSyrius;
import com.helvetia.app.quarkus.kafka.config.SyriusConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import static com.helvetia.app.quarkus.kafka.SyriusConsumer.isMessageRelevant;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MapDubletteTest {

    SyriusConfig config = mock(SyriusConfig.class);


    @Test
    public void test() throws Exception {

        when(config.getKommVerMapper()).thenReturn(new KommVerMapper());
        when(config.getAddressMapper()).thenReturn(new AddressMapper());
        when(config.getNatPersonMapper()).thenReturn(new NatPersonMapper());
        when(config.getJurPersonMapper()).thenReturn(new JurPersonMapper());
        when(config.getDublettenMapper()).thenReturn(new DublettenMapper());

        JsonAvroConverter converter = new JsonAvroConverter();
        changeMdmResponse response = converter.convertToSpecificRecord(msg.getBytes(), changeMdmResponse.class, changeMdmResponse.getClassSchema());
        PartnerSyrius partnerSyrius = PartnerSyrius.fromChangeMdmResponse(response, config);

        Assertions.assertEquals("8105c5bb-858f-48b6-8e70-1fe88a9272d1", // survivor
                partnerSyrius.getRepliziereDublettenRequestType().getPartnerReferenz().getPartnerId().getId());
        Assertions.assertEquals("76796503-C4D6-46A3-91C9-913F1829A4E1".toLowerCase(), // non-survivor
                partnerSyrius.getRepliziereDublettenRequestType().getDubletten().get(0).getPartnerReferenz().getPartnerId().getId());


    }


    @Test
    public void test2() throws Exception {

        when(config.getKommVerMapper()).thenReturn(new KommVerMapper());
        when(config.getAddressMapper()).thenReturn(new AddressMapper());
        when(config.getNatPersonMapper()).thenReturn(new NatPersonMapper());
        when(config.getJurPersonMapper()).thenReturn(new JurPersonMapper());
        when(config.getDublettenMapper()).thenReturn(new DublettenMapper());

        JsonAvroConverter converter = new JsonAvroConverter();
        changeMdmResponse response = converter.convertToSpecificRecord(msg2.getBytes(), changeMdmResponse.class, changeMdmResponse.getClassSchema());

        Assertions.assertTrue(isMessageRelevant(response, config.getSYRIUS()));

        Assertions.assertThrows(NullPointerException.class, () -> PartnerSyrius.fromChangeMdmResponse(response, config));



    }

    private static final String msg = "{\"BusinessPartnerID\": [{\"TECHInternalPID_Location\": null, \"TECHInternalPID_Channel\": null, \"TECHInternalPID_NaturalPerson\": null, \"TECHInternalPID_LegalPerson\": null, \"TECHInternalPID_Underwriting\": null, \"TECHInternalPID_Duplicates\": null, \"TECHInternalPID\": null, \"hasChannelID\": [], \"isLegalPerson\": [], \"isNaturalPerson\": [{\"NaturalPerson\": [{\"academicDegree\": null, \"alienStatus\": null, \"creditRating\": null, \"dateOfBirth\": {\"day\": 1, \"month\": 1, \"year\": 2000}, \"dateOfDeath\": null, \"discountCategory\": null, \"DNA\": null, \"driverLicenseID\": null, \"dutyToContribute\": null, \"employmentGrade\": null, \"employmentStatus\": null, \"function\": null, \"goalAndNeed\": null, \"healthData\": null, \"healthInformation\": null, \"hobby\": null, \"homeTown\": null, \"inEducation\": null, \"isBiometric\": null, \"language\": \"D\", \"maritalStatus\": null, \"membership\": null, \"name\": \"Dublette\", \"nameAtBirth\": null, \"nationalIDCard\": null, \"nationality\": null, \"noAds\": null, \"note\": null, \"passport\": null, \"pets\": null, \"placeOfBirth\": null, \"religion\": null, \"residencePermit\": null, \"residentPermitValidTo\": null, \"salutation\": \"01\", \"secondName\": null, \"securityClassification\": null, \"sex\": \"1\", \"signature\": null, \"skills\": null, \"socialAssistanceRecipients\": null, \"socialSecurityNumber\": null, \"surname\": \"Die\", \"TECHExternalPID\": \"5692275f-60a7-41cc-9227-5f60a7f1ccb4\", \"TECHInternalPID\": \"522F851C-B1F1-4B23-B929-779CA2004995\", \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"thirdName\": null, \"domicileCity\": null, \"domicileCountry\": null, \"domicileHouseNumber\": null, \"domicileRegion\": null, \"domicileStreet\": null, \"domicileZIP\": null, \"_stp_id\": \"NaturalPerson:651289D8924F56F61B1E7AB20C81B111\", \"_stp_label\": \"651289D8924F56F61B1E7AB20C81B111\", \"_stp_type\": \"NaturalPerson\", \"employee\": null, \"otherTitle\": null, \"profession\": null, \"TECHUser\": \"CHTI-MDM\", \"employeeUser\": null}], \"BITEMPMDMTimestamp\": \"20200619130552742\", \"BITEMPSourceTimestamp\": \"20200619150548781\", \"BITEMPValidFrom\": {\"day\": 19, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 31, \"month\": 12, \"year\": 9999}, \"TECHExternalPID\": \"5692275f-60a7-41cc-9227-5f60a7f1ccb4\", \"TECHInternalPID\": \"522F851C-B1F1-4B23-B929-779CA2004995\", \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"_stp_id\": \"isNaturalPerson:20200619130552742\", \"_stp_key\": \"20200619130552742\", \"_stp_label\": \"isNaturalPerson\", \"TECHUser\": \"CHTI-MDM\"}], \"hasUnderwritingCodeID\": [], \"isDuplicate\": [{\"BusinessPartnerID\": [{\"TECHMDMPartnerID\": \"8105C5BB-858F-48B6-8E70-1FE88A9272D1\", \"hasIdentificationNumber\": [{\"IdentificationNumber\": [{\"IDNumber\": \"60377811\", \"IDType\": \"1\", \"TECHExternalPID\": \"8383723c-347a-4c71-8372-3c347a2c71e0\", \"TECHInternalPID\": \"CFA765F8-AC45-4AA2-8249-A157F986874E\", \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null, \"TECHUser\": null}], \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200619130137665\", \"BITEMPSourceTimestamp\": \"20200619150132540\", \"BITEMPValidFrom\": {\"day\": 19, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": null, \"TECHExternalPID\": \"8383723c-347a-4c71-8372-3c347a2c71e0\", \"TECHMDMPartnerID\": \"8105C5BB-858F-48B6-8E70-1FE88A9272D1\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"TECHUser\": null}]}], \"BITEMPMDMTimestamp\": \"20200619130552742\", \"BITEMPSourceTimestamp\": \"20200619150548781\", \"BITEMPValidFrom\": {\"day\": 19, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 31, \"month\": 12, \"year\": 9999}, \"TECHExternalPID\": \"5692275f-60a7-41cc-9227-5f60a7f1ccb4\", \"TECHInternalPID\": \"68A5BB70-0B8A-4955-A257-06F51E74FE2A\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"BITEMPDeleted\": \"\"}], \"Status\": null, \"CRUD\": null, \"hasUpdate\": true, \"user_fields\": [], \"Status_Description\": null, \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"TECHUser\": \"CHTI-MDM\", \"isBusinessPartner\": [{\"BusinessPartner\": [{\"encryptionCode_VIP\": null, \"newBusinessPartnerID\": null, \"TECHExternalPID\": \"5692275f-60a7-41cc-9227-5f60a7f1ccb4\", \"TECHInternalPID\": \"522F851C-B1F1-4B23-B929-779CA2004995\", \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"_stp_id\": \"BusinessPartner:A72A8059F91CBD645CD8CC979B4749DE\", \"_stp_label\": \"A72A8059F91CBD645CD8CC979B4749DE\", \"_stp_type\": \"BusinessPartner\", \"TECHUser\": null}], \"BITEMPMDMTimestamp\": \"20200619130552742\", \"BITEMPSourceTimestamp\": \"20200619150548781\", \"TECHExternalPID\": \"5692275f-60a7-41cc-9227-5f60a7f1ccb4\", \"TECHInternalPID\": \"522F851C-B1F1-4B23-B929-779CA2004995\", \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"BITEMPValidFrom\": {\"day\": 19, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 31, \"month\": 12, \"year\": 9999}, \"_stp_id\": \"isBusinessPartner:20200619130552742\", \"_stp_label\": \"A72A8059F91CBD645CD8CC979B4749DE\", \"_stp_key\": \"20200619130552742\", \"TECHUser\": null}], \"TECHExternalPID\": null, \"hasIdentificationNumber\": [{\"IdentificationNumber\": [{\"IDNumber\": \"60377812\", \"IDType\": \"1\", \"_stp_label\": null, \"TECHExternalPID\": \"118ef16d-24b6-4ff5-8ef1-6d24b60ff5f0\", \"TECHInternalPID\": \"7E81A04F-4A6B-4896-900B-722F4A861040\", \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null, \"_stp_id\": null, \"_stp_type\": null}], \"BITEMPMDMTimestamp\": \"20200619130230634\", \"BITEMPSourceTimestamp\": \"20200619150224582\", \"BITEMPValidFrom\": {\"day\": 19, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": null, \"TECHExternalPID\": \"118ef16d-24b6-4ff5-8ef1-6d24b60ff5f0\", \"TECHInternalPID\": null, \"TECHMDMPartnerID\": \"76796503-C4D6-46A3-91C9-913F1829A4E1\", \"TECHSourceSystem\": null, \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null}], \"TECHSourceSystem\": \"CH_ZEPAS\", \"Status_Code\": null}]}";

    private static final String msg2 = "{\"BusinessPartnerID\": [{\"TECHInternalPID_Location\": null, \"TECHInternalPID_Channel\": null, \"TECHInternalPID_NaturalPerson\": null, \"TECHInternalPID_LegalPerson\": null, \"TECHInternalPID_Underwriting\": null, \"TECHInternalPID_Duplicates\": \"702356B9-5D8C-413F-B551-37D5CA78BEF9\", \"TECHInternalPID\": null, \"hasChannelID\": [], \"isLegalPerson\": [], \"isNaturalPerson\": [], \"hasUnderwritingCodeID\": [], \"isDuplicate\": [{\"BusinessPartnerID\": [{\"TECHMDMPartnerID\": \"7071A408-8272-4B5F-A8E2-B74283F4510F\", \"hasIdentificationNumber\": [{\"IdentificationNumber\": [{\"IDNumber\": null, \"IDType\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": null, \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null, \"TECHUser\": null}], \"BITEMPDeleted\": null, \"BITEMPMDMTimestamp\": \"20200629070254402\", \"BITEMPSourceTimestamp\": \"20200629090247920\", \"BITEMPValidFrom\": {\"day\": 29, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 31, \"month\": 12, \"year\": 9999}, \"TECHExternalPID\": \"96a61ac3-dcb7-4e01-a61a-c3dcb7fe0157\", \"TECHMDMPartnerID\": \"7071A408-8272-4B5F-A8E2-B74283F4510F\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"TECHUser\": null}]}], \"BITEMPMDMTimestamp\": \"20200629072209441\", \"BITEMPSourceTimestamp\": \"20200629092000000\", \"BITEMPValidFrom\": {\"day\": 29, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 31, \"month\": 12, \"year\": 9999}, \"TECHExternalPID\": \"21f4b830-cd78-41bb-b4b8-30cd7881bb8d\", \"TECHInternalPID\": \"702356B9-5D8C-413F-B551-37D5CA78BEF9\", \"TECHSourceSystem\": \"CH_ZEPAS\", \"TECHMDMPartnerID\": \"8D2E7212-00F7-4B38-BB13-C101EB5860C2\", \"BITEMPDeleted\": \"X\"}], \"Status\": null, \"CRUD\": null, \"hasUpdate\": true, \"user_fields\": [], \"Status_Description\": null, \"TECHMDMPartnerID\": \"8D2E7212-00F7-4B38-BB13-C101EB5860C2\", \"TECHUser\": \"DEFAULT-MDM\", \"isBusinessPartner\": [{\"BusinessPartner\": [{\"encryptionCode_VIP\": null, \"newBusinessPartnerID\": null, \"TECHExternalPID\": \"21f4b830-cd78-41bb-b4b8-30cd7881bb8d\", \"TECHInternalPID\": \"702356B9-5D8C-413F-B551-37D5CA78BEF9\", \"TECHMDMPartnerID\": \"8D2E7212-00F7-4B38-BB13-C101EB5860C2\", \"TECHSourceSystem\": null, \"_stp_id\": \"BusinessPartner:72548CDD747514FB5D7630A8B4BB9778\", \"_stp_label\": \"72548CDD747514FB5D7630A8B4BB9778\", \"_stp_type\": \"BusinessPartner\", \"TECHUser\": \"DEFAULT-MDM\"}], \"BITEMPMDMTimestamp\": \"20200629072209441\", \"BITEMPSourceTimestamp\": null, \"TECHExternalPID\": \"21f4b830-cd78-41bb-b4b8-30cd7881bb8d\", \"TECHInternalPID\": \"702356B9-5D8C-413F-B551-37D5CA78BEF9\", \"TECHMDMPartnerID\": \"8D2E7212-00F7-4B38-BB13-C101EB5860C2\", \"TECHSourceSystem\": null, \"BITEMPValidFrom\": {\"day\": 29, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 31, \"month\": 12, \"year\": 9999}, \"_stp_id\": \"isBusinessPartner:20200629072209441\", \"_stp_label\": \"72548CDD747514FB5D7630A8B4BB9778\", \"_stp_key\": \"20200629072209441\", \"TECHUser\": \"DEFAULT-MDM\"}], \"TECHExternalPID\": \"21f4b830-cd78-41bb-b4b8-30cd7881bb8d\", \"hasIdentificationNumber\": [{\"IdentificationNumber\": [{\"IDNumber\": \"60378017\", \"IDType\": \"1\", \"_stp_label\": null, \"TECHExternalPID\": \"0c00e0e7-f85d-4871-80e0-e7f85d38712c\", \"TECHInternalPID\": \"3C630161-39D9-401B-BE1A-7B858A58BFD6\", \"TECHMDMPartnerID\": null, \"TECHSourceSystem\": null, \"_stp_id\": null, \"_stp_type\": null}], \"BITEMPMDMTimestamp\": \"20200629070328806\", \"BITEMPSourceTimestamp\": \"20200629090324185\", \"BITEMPValidFrom\": {\"day\": 29, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": null, \"TECHExternalPID\": \"0c00e0e7-f85d-4871-80e0-e7f85d38712c\", \"TECHInternalPID\": null, \"TECHMDMPartnerID\": \"8D2E7212-00F7-4B38-BB13-C101EB5860C2\", \"TECHSourceSystem\": null, \"_stp_id\": null, \"_stp_key\": null, \"_stp_label\": null}], \"TECHSourceSystem\": \"CH_ZEPAS\", \"Status_Code\": null}]}";

}
