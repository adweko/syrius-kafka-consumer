package com.helvetia.app.quarkus.kafka;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.helvetia.app.quarkus.kafka.config.SyriusConfig;
import com.helvetia.app.quarkus.kafka.mapping.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SyriusSourceSystemTest {

    SyriusConfig config = mock(SyriusConfig.class);


    @Test
    public void test() throws Exception {

        when(config.getKommVerMapper()).thenReturn(new KommVerMapper());
        when(config.getAddressMapper()).thenReturn(new AddressMapper());
        when(config.getNatPersonMapper()).thenReturn(new NatPersonMapper());
        when(config.getJurPersonMapper()).thenReturn(new JurPersonMapper());
        when(config.getDublettenMapper()).thenReturn(new DublettenMapper());
        when(config.getSYRIUS()).thenReturn(new SyriusConfig().getSYRIUS());

        JsonAvroConverter converter = new JsonAvroConverter();
        changeMdmResponse response = converter.convertToSpecificRecord(msg.getBytes(), changeMdmResponse.class, changeMdmResponse.getClassSchema());

        Assertions.assertFalse(SyriusConsumer.isMessageRelevant(response, config.getSYRIUS()));


    }

    private static final String msg = "{\"BusinessPartnerID\": [{\"TECHInternalPID_Location\": null, \"TECHInternalPID_Channel\": null, \"TECHInternalPID_NaturalPerson\": null, \"TECHInternalPID_LegalPerson\": null, \"TECHInternalPID_Underwriting\": null, \"TECHInternalPID_Duplicates\": null, \"TECHInternalPID\": \"E999F41F-6D0F-4159-92A3-AB0B32ADD32B\", \"hasChannelID\": null, \"isLegalPerson\": null, \"isNaturalPerson\": [{\"NaturalPerson\": [{\"academicDegree\": null, \"alienStatus\": null, \"creditRating\": null, \"dateOfBirth\": {\"day\": 1, \"month\": 1, \"year\": 1980}, \"dateOfDeath\": null, \"discountCategory\": null, \"DNA\": null, \"driverLicenseID\": null, \"dutyToContribute\": null, \"employmentGrade\": null, \"employmentStatus\": null, \"function\": null, \"goalAndNeed\": null, \"healthData\": null, \"healthInformation\": null, \"hobby\": null, \"homeTown\": \"\", \"inEducation\": null, \"isBiometric\": null, \"language\": \"D\", \"maritalStatus\": null, \"membership\": null, \"name\": \"Test\", \"nameAtBirth\": \"\", \"nationalIDCard\": null, \"nationality\": \"CH\", \"noAds\": null, \"note\": null, \"passport\": null, \"pets\": null, \"placeOfBirth\": null, \"religion\": null, \"residencePermit\": null, \"residentPermitValidTo\": null, \"salutation\": null, \"secondName\": \"\", \"securityClassification\": null, \"sex\": \"1\", \"signature\": null, \"skills\": null, \"socialAssistanceRecipients\": null, \"socialSecurityNumber\": null, \"surname\": \"Loca Number\", \"TECHExternalPID\": null, \"TECHInternalPID\": \"E999F41F-6D0F-4159-92A3-AB0B32ADD32B\", \"TECHMDMPartnerID\": \"28B56053-2C5E-47A8-9E9E-D8487C65761D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"thirdName\": null, \"domicileCity\": null, \"domicileCountry\": null, \"domicileHouseNumber\": null, \"domicileRegion\": null, \"domicileStreet\": null, \"domicileZIP\": null, \"_stp_id\": \"NaturalPerson:2F6270A518E7518AC676751E62EC64AB\", \"_stp_label\": \"2F6270A518E7518AC676751E62EC64AB\", \"_stp_type\": \"NaturalPerson\", \"employee\": null, \"otherTitle\": null, \"profession\": null, \"TECHUser\": null, \"employeeUser\": null}], \"BITEMPMDMTimestamp\": \"20200622151459509\", \"BITEMPSourceTimestamp\": \"20200622151231438\", \"BITEMPValidFrom\": {\"day\": 22, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": null, \"TECHInternalPID\": \"E999F41F-6D0F-4159-92A3-AB0B32ADD32B\", \"TECHMDMPartnerID\": \"28B56053-2C5E-47A8-9E9E-D8487C65761D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"_stp_id\": \"isNaturalPerson:20200622151459509\", \"_stp_key\": \"20200622151459509\", \"_stp_label\": \"isNaturalPerson\", \"TECHUser\": null}], \"hasUnderwritingCodeID\": null, \"isDuplicate\": null, \"Status\": null, \"CRUD\": \"CREATE\", \"hasUpdate\": true, \"user_fields\": [], \"Status_Description\": null, \"TECHMDMPartnerID\": \"28B56053-2C5E-47A8-9E9E-D8487C65761D\", \"TECHUser\": null, \"isBusinessPartner\": [{\"BusinessPartner\": [{\"encryptionCode_VIP\": \"\", \"newBusinessPartnerID\": null, \"TECHExternalPID\": null, \"TECHInternalPID\": \"E999F41F-6D0F-4159-92A3-AB0B32ADD32B\", \"TECHMDMPartnerID\": \"28B56053-2C5E-47A8-9E9E-D8487C65761D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"_stp_id\": \"BusinessPartner:A112AC74393CE92B422425193CB2F18A\", \"_stp_label\": \"A112AC74393CE92B422425193CB2F18A\", \"_stp_type\": \"BusinessPartner\", \"TECHUser\": \"CHE0ZKM\"}], \"BITEMPMDMTimestamp\": \"20200622151459509\", \"BITEMPSourceTimestamp\": \"20200622151231438\", \"TECHExternalPID\": null, \"TECHInternalPID\": \"E999F41F-6D0F-4159-92A3-AB0B32ADD32B\", \"TECHMDMPartnerID\": \"28B56053-2C5E-47A8-9E9E-D8487C65761D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"BITEMPValidFrom\": {\"day\": 22, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"_stp_id\": \"isBusinessPartner:20200622151459509\", \"_stp_label\": \"A112AC74393CE92B422425193CB2F18A\", \"_stp_key\": \"20200622151459509\", \"TECHUser\": \"CHE0ZKM\"}], \"TECHExternalPID\": null, \"hasIdentificationNumber\": [{\"IdentificationNumber\": [{\"IDNumber\": \"85001016\", \"IDType\": \"1\", \"_stp_label\": \"185001016\", \"TECHExternalPID\": null, \"TECHInternalPID\": \"E999F41F-6D0F-4159-92A3-AB0B32ADD32B\", \"TECHMDMPartnerID\": \"28B56053-2C5E-47A8-9E9E-D8487C65761D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"_stp_id\": \"IdentificationNumber:185001016\", \"_stp_type\": \"IdentificationNumber\"}, {\"IDNumber\": \"28b56053-2c5e-47a8-9e9e-d8487c65761d\", \"IDType\": \"2\", \"_stp_label\": \"228b56053-2c5e-47a8-9e9e-d8487c65761d\", \"TECHExternalPID\": null, \"TECHInternalPID\": \"E999F41F-6D0F-4159-92A3-AB0B32ADD32B\", \"TECHMDMPartnerID\": \"28B56053-2C5E-47A8-9E9E-D8487C65761D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"_stp_id\": \"IdentificationNumber:228b56053-2c5e-47a8-9e9e-d8487c65761d\", \"_stp_type\": \"IdentificationNumber\"}], \"BITEMPMDMTimestamp\": \"20200622151459509\", \"BITEMPSourceTimestamp\": \"20200622151231438\", \"BITEMPValidFrom\": {\"day\": 22, \"month\": 6, \"year\": 2020}, \"BITEMPValidTo\": {\"day\": 1, \"month\": 1, \"year\": 3000}, \"TECHExternalPID\": null, \"TECHInternalPID\": \"E999F41F-6D0F-4159-92A3-AB0B32ADD32B\", \"TECHMDMPartnerID\": \"28B56053-2C5E-47A8-9E9E-D8487C65761D\", \"TECHSourceSystem\": \"SYRIUS_MF\", \"_stp_id\": \"hasIdentificationNumber:20200622151459509\", \"_stp_key\": \"20200622151459509\", \"_stp_label\": \"hasIdentificationNumber\"}], \"TECHSourceSystem\": \"SYRIUS_MF\", \"Status_Code\": null}]}";
}
