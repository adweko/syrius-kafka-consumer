package com.helvetia.app.quarkus.kafka.config;

import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.PartnerReplikationExpServicePortType;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;

import javax.inject.Singleton;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

@Singleton
public class SSLConfig {

    public void setupSSL(PartnerReplikationExpServicePortType partnerReplikationExpServicePortType) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {

        Client client = ClientProxy.getClient(partnerReplikationExpServicePortType);
        HTTPConduit httpConduit = (HTTPConduit) client.getConduit();

        TLSClientParameters parameters = new TLSClientParameters();
        parameters.setDisableCNCheck(true);
        parameters.setSSLSocketFactory(createSSLContext().getSocketFactory());
        httpConduit.setTlsClientParameters(parameters);
    }


    public SSLContext createSSLContext() throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException, KeyManagementException {
        KeyStore truststore = KeyStore.getInstance("JKS");
        truststore.load(new FileInputStream("/var/run/secrets/java.io/truststores/user-truststore.jks"), "changeit".toCharArray());
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        tmf.init(truststore);
        SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, tmf.getTrustManagers(), new SecureRandom());
        return sslContext;
    }
}
