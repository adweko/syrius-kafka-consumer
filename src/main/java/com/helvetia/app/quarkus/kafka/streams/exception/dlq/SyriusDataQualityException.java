package com.helvetia.app.quarkus.kafka.streams.exception.dlq;

public class SyriusDataQualityException extends DLQException {
    public SyriusDataQualityException() {
        super();
    }

    public SyriusDataQualityException(String message) {
        super(message);
    }

    public SyriusDataQualityException(String message, Throwable cause) {
        super(message, cause);
    }

    public SyriusDataQualityException(Throwable cause) {
        super(cause);
    }
}
