package com.helvetia.app.quarkus.kafka.streams;

import ch.mdm.syrius.dlq.DLQMessages;
import changePartner.com.helvetia.custom.changeMdmResponse;
import com.helvetia.app.quarkus.kafka.logging.LogHelper;
import com.helvetia.app.quarkus.kafka.streams.exception.dlq.DLQException;
import com.helvetia.app.quarkus.kafka.streams.model.ResponseOrDlqmessages;
import com.helvetia.app.quarkus.kafka.streams.model.ResultOrExceptionWithInput;
import com.helvetia.app.quarkus.kafka.streams.model.TechMDMPartnerID;
import com.helvetia.app.quarkus.kafka.streams.model.UnlockMessageValue;
import com.helvetia.app.quarkus.kafka.streams.processor.*;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.util.Collections;
import java.util.Map;

// found it a bit unclear which properties are passed to StreamsConfig. See here for the relevant prefixes
// https://github.com/quarkusio/quarkus/blob/224fd01898801e9c15d2e0db73c32af4332a1d24/extensions/kafka-streams/runtime/src/main/java/io/quarkus/kafka/streams/runtime/KafkaStreamsPropertiesUtil.java#L18
// everything with prefix kafka-streams is passed to StreamsConfiguration

@ApplicationScoped
@Slf4j
public class TopologyProducer {

    @Inject
    MapperAndSender<changeMdmResponse, Boolean> mapperAndSender;

    @Inject
    RelevantMessageFilter<changeMdmResponse> relevantMessageFilter;

    @Inject
    RelationToEntity<changeMdmResponse, TechMDMPartnerID> relationToEntity;

    @Inject
    LogHelper logHelper;

    @ConfigProperty(name = "ch.helvetia.kafka.input.topic")
    String inputTopicName;

    @ConfigProperty(name = "ch.helvetia.kafka.unlock.topic")
    String unlockTopicName;

    @ConfigProperty(name = "ch.helvetia.kafka.dlq.topic")
    String dlqTopicName;

    @ConfigProperty(name = "quarkus.kafka-streams.schema-registry-url")
    String schemaRegistryUrl;

    @Inject
    LogAsUnrelatableProcessor logAsUnrelatableProcessor;
    @Inject
    MapToDeadLetterQueueTransformer mapToDeadLetterQueueTransformer;
    @Inject
    IsPartnerLockedAndFilterRepublishedTransformer isPartnerLockedTransformer;
    @Inject
    UnlockAndMaybeReprocessDlqTransformer unlockAndMaybeReprocessDlqTransformer;

    public static final String DLQ_STORE_NAME = "dlqStore";
    public static final String REPUBLISHED_STORE_NAME = "republishedStore";

    @Produces
    public Topology buildTopology() {

        // see https://github.com/confluentinc/kafka-streams-examples/blob/5.5.1-post/src/test/java/io/confluent/examples/streams/SpecificAvroIntegrationTest.java
        // When you want to override serdes explicitly/selectively
        final Map<String, String> serdeConfig = Collections.singletonMap("schema.registry.url",
                schemaRegistryUrl);
        final Serde<changeMdmResponse> responseAvroSerde = new SpecificAvroSerde<>();
        responseAvroSerde.configure(serdeConfig, false); // `false` for record values

        final Serde<DLQMessages> dlqMessagesAvroSerde = new SpecificAvroSerde<>();
        dlqMessagesAvroSerde.configure(serdeConfig, false); // `false` for record values

        StreamsBuilder builder = new StreamsBuilder();


        // We read the input topic as GlobalKTable. Global tables are always read in first, before any other messages are processed.
        // We need that for "new conusmers", i.e. ones that consume the entire topic from-beginning and are not interested in already
        // republished data.
        // They will ignore such messages based on the unlock/republish messages.
        // Note that we read in the key via jsonSerde of TECHMDMPartnerID, effectively ignoring all entries but the partnerID.
        // The idea is to enable look-up via partnerId, assuming that we are only ever interested in the latest value
        final GlobalKTable<TechMDMPartnerID, UnlockMessageValue> republishedGlobalKTable = builder.globalTable(unlockTopicName,
                Consumed.with(TechMDMPartnerID.jsonSerde, UnlockMessageValue.serde),
                Materialized.as(REPUBLISHED_STORE_NAME));

        final GlobalKTable<TechMDMPartnerID, DLQMessages> dlqGlobalKTable = builder.globalTable(dlqTopicName,
                Consumed.with(TechMDMPartnerID.serde, dlqMessagesAvroSerde),
                Materialized.as(DLQ_STORE_NAME));


        /** Regular messages arrive ... **/

        final KStream<String, changeMdmResponse> mdmInputStream = builder.stream(inputTopicName, Consumed.with(Serdes.String(), responseAvroSerde));

        // dummy transform just for wall clock punctuator
        mdmInputStream
                .transform(() -> unlockAndMaybeReprocessDlqTransformer) // returns deletes after unlock, or nothing if nothing there
                .to(dlqTopicName, Produced.with(TechMDMPartnerID.serde, dlqMessagesAvroSerde));

        @SuppressWarnings("unchecked") final KStream<String, changeMdmResponse>[] branch = mdmInputStream.branch(
                (key, response) -> relationToEntity.canBeRelatedToEntity(response),
                (key, response) -> !relationToEntity.canBeRelatedToEntity(response)
        );
        final KStream<String, changeMdmResponse> mdmValidUncheckedInputStream = branch[0];
        final KStream<String, changeMdmResponse> mdmUnrelatableInputStream = branch[1];

        mdmUnrelatableInputStream.process(() -> logAsUnrelatableProcessor);

        @SuppressWarnings("unchecked")
        KStream<TechMDMPartnerID, ResponseOrDlqmessages>[] maybeLockedInputStream = mdmValidUncheckedInputStream
                .selectKey((k, v) -> relationToEntity.extractEntity(v))
                .transform(() -> isPartnerLockedTransformer)
                .branch((k, v) -> v.isResponse(),
                        (k, v) -> v.isDlqMessages());

        maybeLockedInputStream[1]
                .mapValues(ResponseOrDlqmessages::getDlqMessages)
                .to(dlqTopicName, Produced.with(TechMDMPartnerID.serde, dlqMessagesAvroSerde));

        final KStream<TechMDMPartnerID, changeMdmResponse> syriusRelevantMessages = maybeLockedInputStream[0]
                .mapValues(ResponseOrDlqmessages::getResponse)
                .filter((k, v) -> relevantMessageFilter.isMessageRelevant(v));


        @SuppressWarnings("unchecked") final KStream<TechMDMPartnerID, ResultOrExceptionWithInput<changeMdmResponse, Boolean>>[] successesOrExceptions =
                syriusRelevantMessages
                        .mapValues(mapperAndSender::mapAndSend)
                        .branch((k, v) -> v.isResult(), (k, v) -> !v.isResult());

        final KStream<TechMDMPartnerID, ResultOrExceptionWithInput<changeMdmResponse, Boolean>> failures = successesOrExceptions[1];

        failures
                .peek((k, v) -> throwJobStoppingExceptions(v))
                .transform(() -> mapToDeadLetterQueueTransformer)
                .to(dlqTopicName, Produced.with(TechMDMPartnerID.serde, dlqMessagesAvroSerde));

        return builder.build();
    }


    private void throwJobStoppingExceptions(ResultOrExceptionWithInput errorResult) {
        final Exception exception = errorResult.getException();
        if (shouldNotThrow(exception)) {
            return;
        }
        if (exception instanceof RuntimeException) {
            throw (RuntimeException) exception;
        } else {
            throw new RuntimeException(exception);
        }
    }

    private boolean shouldNotThrow(Exception exception) {
        return exception instanceof DLQException;
    }


}
