package com.helvetia.app.quarkus.kafka.streams;

import com.helvetia.app.quarkus.kafka.streams.model.ResultOrExceptionWithInput;

public interface MapperAndSender<I,O>  {
    ResultOrExceptionWithInput<I, O> mapAndSend(I input);

}
