package com.helvetia.app.quarkus.kafka.mapping;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateHelper {


    // timestamps come as 20200618144950121
    // corresponding to   yyyyMMddHHmmssSSS
    public static javax.xml.datatype.XMLGregorianCalendar gregorianDateFromMDMTimestamp(String sourceTimeStamp) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS").withZone(ZoneId.of("UTC"));
            Instant time = Instant.from(formatter.parse(sourceTimeStamp));
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(time.toString());
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Obtain a Syrius-specific date from a com.helvetia.custom.dateFromXsDate.
     * Dates larger than 3000/01/01 are set to this date as the max. validTo date of Syrius
     *
     * @return
     */
    public static javax.xml.datatype.XMLGregorianCalendar syriusDateFromXsDateWithLimit(inboundTrafoZepasUnderwriting.com.helvetia.custom.dateFromXsDate date) {
        int year = date.getYear();
        int month = date.getMonth();
        int day = date.getDay();
        return threeIntDateToXmlCalendarWithValidate(year, month, day);
    }

    public static javax.xml.datatype.XMLGregorianCalendar syriusDateFromXsDateWithLimit(inboundTrafoZepasChannel.com.helvetia.custom.dateFromXsDate date) {
        int year = date.getYear();
        int month = date.getMonth();
        int day = date.getDay();
        return threeIntDateToXmlCalendarWithValidate(year, month, day);
    }

    public static javax.xml.datatype.XMLGregorianCalendar syriusDateFromXsDateWithLimit(inboundTrafoZepasLegalPerson.com.helvetia.custom.dateFromXsDate date) {
        int year = date.getYear();
        int month = date.getMonth();
        int day = date.getDay();
        return threeIntDateToXmlCalendarWithValidate(year, month, day);
    }

    public static javax.xml.datatype.XMLGregorianCalendar syriusDateFromXsDateWithLimit(inboundTrafoZepasNaturalPerson.com.helvetia.custom.dateFromXsDate date) {
        int year = date.getYear();
        int month = date.getMonth();
        int day = date.getDay();
        return threeIntDateToXmlCalendarWithValidate(year, month, day);
    }

    public static javax.xml.datatype.XMLGregorianCalendar syriusDateFromXsDateNoLimit(inboundTrafoZepasNaturalPerson.com.helvetia.custom.dateFromXsDate date) {
        int year = date.getYear();
        int month = date.getMonth();
        int day = date.getDay();
        return threeIntDateToXmlCalendarWithValidate(year, month, day);
    }


    private static XMLGregorianCalendar threeIntDateToXmlCalendar(int year, int month, int day) {
        XMLGregorianCalendar xmlGregorianCalendar = null;
        try {
            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }
        xmlGregorianCalendar.setYear(year);
        xmlGregorianCalendar.setMonth(month);
        xmlGregorianCalendar.setDay(day);
        return xmlGregorianCalendar;
    }

    private static XMLGregorianCalendar threeIntDateToXmlCalendarWithValidate(int year, int month, int day) {
        if (year > 3000 || (year == 3000 && (month > 1 || (month == 1 && day > 1)))) {
            year = 3000;
            month = 1;
            day = 1;
        }
        return threeIntDateToXmlCalendar(year, month, day);
    }

}
