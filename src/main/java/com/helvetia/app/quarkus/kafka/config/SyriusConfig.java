package com.helvetia.app.quarkus.kafka.config;

import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.data.partnerrolle.v1.PartnerrolleServiceV1;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.PartnerReplikationExpServiceV0;
import com.helvetia.app.quarkus.kafka.mapping.*;
import lombok.Data;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
@Data
public class SyriusConfig {

    @ConfigProperty(name = "ch.helvetia.kafka.bootstrap.servers")
    String bootstrapServers;

    @ConfigProperty(name = "ch.helvetia.kafka.schema.registry.url")
    String schemaRegistryURL;

    @ConfigProperty(name = "ch.helvetia.kafka.group.id")
    String groupID;

    @ConfigProperty(name = "ch.helvetia.kafka.input.topic")
    String inputTopic;

    @ConfigProperty(name = "ch.helvetia.syrius.endpoint")
    String endpoint;

    @ConfigProperty(name = "ch.helvetia.kafka.auto.offset.reset")
    String autoOffsetReset;

    @Inject
    NatPersonMapper natPersonMapper;

    @Inject
    JurPersonMapper jurPersonMapper;

    @Inject
    KommVerMapper kommVerMapper;

    @Inject
    AddressMapper addressMapper;

    @Inject
    DublettenMapper dublettenMapper;

    @Inject
    SSLConfig sslConfig;

    final String SYRIUS = "SYRIUS_MF";


    PartnerReplikationExpServiceV0 replikationExpService = new PartnerReplikationExpServiceV0(SyriusConfig.class.getResource("/wsdl/partnermgmt/partnerdatenverw/process/partnerreplikationexp/v0/PartnerReplikationExpService.wsdl"));
    PartnerrolleServiceV1 partnerrolleService = new PartnerrolleServiceV1(SyriusConfig.class.getResource("/wsdl/partnermgmt/partnerdatenverw/data/partnerrolle/v1/PartnerrolleService.wsdl"));
}
