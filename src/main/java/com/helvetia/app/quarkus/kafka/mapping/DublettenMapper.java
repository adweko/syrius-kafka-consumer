package com.helvetia.app.quarkus.kafka.mapping;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsDublBeziehungCodeType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartnerIdType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereDublettenRequestType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.WsDubletteType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.WsPartnerReferenzType;

import javax.enterprise.context.ApplicationScoped;
import java.util.Objects;
import java.util.UUID;

@ApplicationScoped
public class DublettenMapper {

    public DublettenMapper(){
    }

    public RepliziereDublettenRequestType mapDublette(changeMdmResponse changeMdmResponse) {
        RepliziereDublettenRequestType dublettenRequestType = new RepliziereDublettenRequestType();
        dublettenRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());

        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType(); // survivor
        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
        wsPartnerIdType.setId(String.valueOf(
                changeMdmResponse.getBusinessPartnerID().get(0).getIsDuplicate().get(0).getBusinessPartnerID().get(0).getTECHMDMPartnerID()
        ).toLowerCase());
        //wsPartnerIdType.setId(context.read("$.BusinessPartnerID[0].isDuplicate[0].BusinessPartnerID[0].TECHMDMPartnerID").toString().toLowerCase()); //Survivor
        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
        dublettenRequestType.setPartnerReferenz(wsPartnerReferenzType);

        WsPartnerReferenzType wsPartnerReferenzType2 = new WsPartnerReferenzType(); // non-survivor
        WsPartnerIdType wsPartnerIdType2 = new WsPartnerIdType();
        WsDubletteType wsDubletteType = new WsDubletteType();
        wsPartnerIdType2.setId(String.valueOf(
                changeMdmResponse.getBusinessPartnerID().get(0).getTECHMDMPartnerID() // NOTE: not nat. Person specific
        ).toLowerCase());
        wsPartnerReferenzType2.setPartnerId(wsPartnerIdType2);
        //wsPartnerIdType2.setId(context.read("$.BusinessPartnerID[0].isNaturalPerson[0].TECHMDMPartnerID").toString().toLowerCase()); //Non-Survivor
        wsDubletteType.setPartnerReferenz(wsPartnerReferenzType2);

        WsDublBeziehungCodeType wsDublBeziehungCodeType = new WsDublBeziehungCodeType();
        CharSequence bitempDeleted = changeMdmResponse.getBusinessPartnerID().get(0).getIsDuplicate().get(0).getBITEMPDeleted();
        if (bitempDeleted != null && String.valueOf(bitempDeleted).equalsIgnoreCase("X")) {
            wsDublBeziehungCodeType.setCodeId("-2010014557"); //Boid Dublettentrennung
        } else {
            wsDublBeziehungCodeType.setCodeId("-2010014556"); //Boid Dublettenzusammenlegung
        }
        wsDubletteType.setDublBeziehung(wsDublBeziehungCodeType);

        String sourceTimeStamp = changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getBITEMPSourceTimestamp().toString();
        String techUser = Objects.toString(changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getTECHUser(), null);
        wsDubletteType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsDubletteType.setAenderungDurch(techUser);

        dublettenRequestType.getDubletten().add(wsDubletteType);
        return dublettenRequestType;
    }
}
