package com.helvetia.app.quarkus.kafka.streams.exception.dlq;

public class MappingException extends DLQException {
    public MappingException() {
        super();
    }

    public MappingException(String message) {
        super(message);
    }

    public MappingException(String message, Throwable cause) {
        super(message, cause);
    }

    public MappingException(Throwable cause) {
        super(cause);
    }
}
