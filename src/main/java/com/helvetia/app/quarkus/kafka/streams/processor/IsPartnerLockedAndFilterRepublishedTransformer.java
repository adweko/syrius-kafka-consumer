package com.helvetia.app.quarkus.kafka.streams.processor;

import ch.mdm.syrius.dlq.DLQMessage;
import ch.mdm.syrius.dlq.DLQMessages;
import changePartner.com.helvetia.custom.changeMdmResponse;
import com.helvetia.app.quarkus.kafka.logging.LogHelper;
import com.helvetia.app.quarkus.kafka.streams.TopologyProducer;
import com.helvetia.app.quarkus.kafka.streams.model.MDMTimestamp;
import com.helvetia.app.quarkus.kafka.streams.model.ResponseOrDlqmessages;
import com.helvetia.app.quarkus.kafka.streams.model.TechMDMPartnerID;
import com.helvetia.app.quarkus.kafka.streams.model.UnlockMessageValue;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.TimestampedKeyValueStore;
import org.apache.kafka.streams.state.ValueAndTimestamp;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.Duration;
import java.time.Instant;

@ApplicationScoped
public class IsPartnerLockedAndFilterRepublishedTransformer implements Transformer<TechMDMPartnerID, changeMdmResponse, KeyValue<TechMDMPartnerID, ResponseOrDlqmessages>> {

    private ProcessorContext context;
    private TimestampedKeyValueStore<TechMDMPartnerID, DLQMessages> dlqLocalStore;
    private TimestampedKeyValueStore<TechMDMPartnerID, UnlockMessageValue> republishedLocalStore;

    @Inject
    LogHelper logHelper;

    @Override
    @SuppressWarnings("unchecked")
    public void init(ProcessorContext context) {
        this.context = context;
        dlqLocalStore = (TimestampedKeyValueStore<TechMDMPartnerID, DLQMessages>) context.getStateStore(TopologyProducer.DLQ_STORE_NAME);
        republishedLocalStore = (TimestampedKeyValueStore<TechMDMPartnerID, UnlockMessageValue>) context.getStateStore(TopologyProducer.REPUBLISHED_STORE_NAME);

    }



    @Override
    public KeyValue<TechMDMPartnerID, ResponseOrDlqmessages> transform(TechMDMPartnerID techMDMPartnerID, changeMdmResponse response) {
        ValueAndTimestamp<DLQMessages> dlqMessagesValueAndTimestamp = dlqLocalStore.get(techMDMPartnerID);
        if (dlqMessagesValueAndTimestamp != null) {
            DLQMessages lockedMessages = dlqMessagesValueAndTimestamp.value();
            logHelper.logInfo(null, "Message send to DLQ because partner " + techMDMPartnerID.getValue() + " is " +
                    "still locked. Topic/Partition/Offset: " +
                    context.topic() + " / " + context.partition() + " / " + context.offset());
            lockedMessages.getMessages().add(new DLQMessage(context.topic(), context.partition(), context.offset(), context.timestamp(), response));
            return new KeyValue<>(techMDMPartnerID, new ResponseOrDlqmessages(lockedMessages));
        }
        ValueAndTimestamp<UnlockMessageValue> republishedValueAndTimestamp = republishedLocalStore.get(techMDMPartnerID);
        if (republishedValueAndTimestamp != null) {
            Instant republishedAt = republishedValueAndTimestamp.value().getRepublishedAt().getInstant();
            Instant messageTime = Instant.ofEpochMilli(context.timestamp());
            if (messageTime.isBefore(republishedAt)) {
                logHelper.logInfo(null, "Message is ignored because a better version was already republished for " + techMDMPartnerID.getValue() + ". Will only use verions with timestamps larger " +
                        republishedAt + " . Topic/Partition/Offset of the ignored message is: " +
                        context.topic() + " / " + context.partition() + " / " + context.offset());
            }
            return null;
        }

        return new KeyValue<>(techMDMPartnerID, new ResponseOrDlqmessages(response));
    }

    @Override
    public void close() {

    }
}
