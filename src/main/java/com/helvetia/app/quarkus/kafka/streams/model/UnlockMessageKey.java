package com.helvetia.app.quarkus.kafka.streams.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Objects;

/**
 * Republishing messages arriving in ch-mdm.unlock.partner, like this>
 * key: {"TECHMDMPartnerID":"E107B98D-5083-436D-9513-D626B9C475BC","TECHExternalPID":"189f027d-7028-4aa2-9f02-7d70284aa255","TECHSourceSystem":"MDM","BITEMPMDMTimestamp":"2020-08-20T08:57:22+0000"}
 * value: {"TECHMDMPartnerID":"E107B98D-5083-436D-9513-D626B9C475BC","RepublishedAt":"20200819152152623"}
 */
public class UnlockMessageKey {

    public static final Serde<UnlockMessageKey> serde = new Serde<>() {
        @Override
        public Serializer<UnlockMessageKey> serializer() {
            return (topic, data) -> Serdes.String().serializer().serialize(topic, data.toJsonString());
        }

        @Override
        public Deserializer<UnlockMessageKey> deserializer() {
            return (topic, data) -> UnlockMessageKey.fromJsonString(Serdes.String().deserializer().deserialize(topic, data));
        }
    };

    public static final String PARTNER_ID_FIELD_NAME = "TECHMDMPartnerID";
    public static final String TECHExternalPID_FIELD_NAME = "TECHExternalPID";
    public static final String TECHSourceSystem_FIELD_NAME = "TECHSourceSystem";
    public static final String BITEMPMDMTimestamp_FIELD_NAME = "BITEMPMDMTimestamp";

    private final TechMDMPartnerID partnerId;
    private final String techExternalPid;
    private final String techSourceSystem;
    private final MDMTimestamp biTempMdmTimeStamp;

    private static final ObjectMapper mapper = new ObjectMapper();

    private UnlockMessageKey(String jsonRepresentation) {
        try {
            final JsonNode jsonNode = mapper.readTree(jsonRepresentation);
            this.partnerId = TechMDMPartnerID.fromRawValueString(jsonNode.get(PARTNER_ID_FIELD_NAME).asText());
            this.techExternalPid = jsonNode.get(TECHExternalPID_FIELD_NAME).asText();
            this.techSourceSystem = jsonNode.get(TECHSourceSystem_FIELD_NAME).asText();
            this.biTempMdmTimeStamp = new MDMTimestamp(jsonNode.get(BITEMPMDMTimestamp_FIELD_NAME).asText());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public UnlockMessageKey(TechMDMPartnerID partnerId, String techExternalPid, String techSourceSystem, MDMTimestamp biTempMdmTimeStamp) {
        this.partnerId = partnerId;
        this.techExternalPid = techExternalPid;
        this.techSourceSystem = techSourceSystem;
        this.biTempMdmTimeStamp = biTempMdmTimeStamp;
    }

    public static UnlockMessageKey create(TechMDMPartnerID partnerId, String techExternalPid, String techSourceSystem, MDMTimestamp biTempMdmTimeStamp) {
        return new UnlockMessageKey(partnerId, techExternalPid, techSourceSystem, biTempMdmTimeStamp);
    }

    public String toJsonString() {
        try {
            final ObjectNode objectNode = JsonNodeFactory.instance.objectNode();
            objectNode.put(PARTNER_ID_FIELD_NAME, partnerId.getValue());
            objectNode.put(TECHExternalPID_FIELD_NAME, techExternalPid);
            objectNode.put(TECHSourceSystem_FIELD_NAME, techSourceSystem);
            objectNode.put(BITEMPMDMTimestamp_FIELD_NAME, biTempMdmTimeStamp != null ? biTempMdmTimeStamp.getRawTimestamp() : null);
            return mapper.writeValueAsString(objectNode);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static UnlockMessageKey fromJsonString(String jsonRepresentation) {
        return new UnlockMessageKey(jsonRepresentation);
    }

    public TechMDMPartnerID getPartnerId() {
        return partnerId;
    }

    public String getTechExternalPid() {
        return techExternalPid;
    }

    public String getTechSourceSystem() {
        return techSourceSystem;
    }

    public MDMTimestamp getBiTempMdmTimeStamp() {
        return biTempMdmTimeStamp;
    }

    @Override
    public String toString() {
        return "UnlockMessageKey{" +
                "partnerId=" + partnerId +
                ", techExternalPid='" + techExternalPid + '\'' +
                ", techSourceSystem='" + techSourceSystem + '\'' +
                ", biTempMdmTimeStamp=" + biTempMdmTimeStamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnlockMessageKey that = (UnlockMessageKey) o;
        return Objects.equals(partnerId, that.partnerId) &&
                Objects.equals(techExternalPid, that.techExternalPid) &&
                Objects.equals(techSourceSystem, that.techSourceSystem) &&
                Objects.equals(biTempMdmTimeStamp, that.biTempMdmTimeStamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(partnerId, techExternalPid, techSourceSystem, biTempMdmTimeStamp);
    }
}
