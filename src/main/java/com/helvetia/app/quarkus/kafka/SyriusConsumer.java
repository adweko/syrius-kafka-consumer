package com.helvetia.app.quarkus.kafka;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.ApiFaultMessage;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.PartnerReplikationExpServicePortType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.*;
import com.helvetia.app.quarkus.kafka.common.AppHelper;
import com.helvetia.app.quarkus.kafka.config.SAMLResponseCreator;
import com.helvetia.app.quarkus.kafka.config.SyriusConfig;
import com.helvetia.app.quarkus.kafka.config.WsConfiguration;
import com.helvetia.app.quarkus.kafka.logging.LogHelper;
import com.jayway.jsonpath.PathNotFoundException;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDEntry;
import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.w3c.dom.Element;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.ws.BindingProvider;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.time.Duration;
import java.util.*;


@SuppressWarnings("unchecked")
@ApplicationScoped
public class SyriusConsumer {

    @Inject
    SyriusConfig syriusConfig;

    @Inject
    LogHelper logHelper;


    private static RepliziereResponseType repliziereResponseType = null;

    public Properties configure() {
        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, syriusConfig.getGroupID());
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, syriusConfig.getBootstrapServers());
        consumerConfig.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, syriusConfig.getSchemaRegistryURL());
        consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
        consumerConfig.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);
        consumerConfig.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        consumerConfig.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1);

        consumerConfig.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SSL");
        consumerConfig.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, "/var/run/secrets/java.io/truststores/user-truststore.jks");
        consumerConfig.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, "changeit");
        consumerConfig.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, "/var/run/secrets/java.io/keystores/user-keystore.jks");
        consumerConfig.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, "changeit");
        consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, syriusConfig.getAutoOffsetReset());
        return consumerConfig;
    }


    public static boolean sendPartnerToSyrius(PartnerSyrius partner, SyriusConfig syriusConfig, PartnerReplikationExpServicePortType partnerReplikationExpServicePortType, String logTransactionId, WsConfiguration.Configuration wsConfiguration, LogHelper logHelper) throws Exception {

        if (partner.getRepliziereNatPersonRequestType() != null) {
            logHelper.logInfo(logTransactionId, "will try to send nat. Person " + AppHelper.objectToXml(partner.getRepliziereNatPersonRequestType()));

            prepareWs(partnerReplikationExpServicePortType, wsConfiguration, syriusConfig);

            repliziereResponseType = partnerReplikationExpServicePortType.repliziereNatPerson(partner.getRepliziereNatPersonRequestType());
            //TODO: Error Handling based on Response
            logHelper.logInfo(logTransactionId, "Repliziere Nat Person Response " + repliziereResponseType.getReplikationErfolgreich() + ", Accessing endpoint: " + syriusConfig.getEndpoint());
        }
        if (partner.getRepliziereJurPersonRequestType() != null) {
            logHelper.logInfo(logTransactionId, "will try to send jur. Person " + AppHelper.objectToXml(partner.getRepliziereJurPersonRequestType()));
            syriusConfig.getSslConfig().setupSSL(partnerReplikationExpServicePortType);

            // SAML-Assertion dem Request hinzufügen
            prepareWs(partnerReplikationExpServicePortType, wsConfiguration, syriusConfig);

            repliziereResponseType = partnerReplikationExpServicePortType.repliziereJurPerson(partner.getRepliziereJurPersonRequestType());
            //TODO: Error Handling based on Response
            logHelper.logInfo(logTransactionId, "Repliziere Jur Person Response " + repliziereResponseType.getReplikationErfolgreich() + ", Accessing endpoint: " + syriusConfig.getEndpoint());
        }
        if (!partner.getRepliziereChannelRequestWrappers().isEmpty()) {
            for (RepliziereChannelRequestWrapper requestWrapper : partner.getRepliziereChannelRequestWrappers()) {
                Object request = requestWrapper.getRequest();
                if (request instanceof RepliziereTelefonRequestType) {
                    logHelper.logInfo(logTransactionId, "will try to send telefon " + AppHelper.objectToXml(request));

                    prepareWs(partnerReplikationExpServicePortType, wsConfiguration, syriusConfig);

                    repliziereResponseType = partnerReplikationExpServicePortType.repliziereTelefon(
                            (RepliziereTelefonRequestType) request);
                } else if (request instanceof RepliziereEmailRequestType) {
                    logHelper.logInfo(logTransactionId, "will try to send email " + AppHelper.objectToXml(request));
                    prepareWs(partnerReplikationExpServicePortType, wsConfiguration, syriusConfig);

                    repliziereResponseType = partnerReplikationExpServicePortType.repliziereEmail(
                            (RepliziereEmailRequestType) request);
                } else if (request instanceof RepliziereWebRequestType) {
                    logHelper.logInfo(logTransactionId, "will try to send web " + AppHelper.objectToXml(request));
                    prepareWs(partnerReplikationExpServicePortType, wsConfiguration, syriusConfig);

                    repliziereResponseType = partnerReplikationExpServicePortType.repliziereWeb(
                            (RepliziereWebRequestType) request);
                } else if (request instanceof RepliziereDomiziladresseRequestType) {
                    logHelper.logInfo(logTransactionId, "will try to send domizil " + AppHelper.objectToXml(request));
                    prepareWs(partnerReplikationExpServicePortType, wsConfiguration, syriusConfig);

                    repliziereResponseType = partnerReplikationExpServicePortType.repliziereDomiziladresse(
                            (RepliziereDomiziladresseRequestType) request);
                } else if (request instanceof RepliziereZusatzadresseRequestType) {
                    logHelper.logInfo(logTransactionId, "will try to send zusatzadresse " + AppHelper.objectToXml(request));
                    prepareWs(partnerReplikationExpServicePortType, wsConfiguration, syriusConfig);

                    repliziereResponseType = partnerReplikationExpServicePortType.repliziereZusatzadresse(
                            (RepliziereZusatzadresseRequestType) request);
                } else {
                    throw new RuntimeException("invalid type of channel request");
                }

                //TODO: Error Handling based on Response
                logHelper.logInfo(logTransactionId, "Repliziere Channel Response " + repliziereResponseType.getReplikationErfolgreich() + ", Accessing endpoint: " + syriusConfig.getEndpoint());
            }
        }

        if (!partner.getReplizierePartnerrolleRequestTypeList().isEmpty()) {
            for (ReplizierePartnerrolleRequestType request : partner.getReplizierePartnerrolleRequestTypeList()) {
                logHelper.logInfo(logTransactionId, "will try to send partner rolle " + AppHelper.objectToXml(request));
                prepareWs(partnerReplikationExpServicePortType, wsConfiguration, syriusConfig);

                repliziereResponseType = partnerReplikationExpServicePortType.replizierePartnerrolle(request);
                //TODO: Error Handling based on Response
                logHelper.logInfo(logTransactionId, "Repliziere Rolle / UWCode Response " + repliziereResponseType.getReplikationErfolgreich() + ", Accessing endpoint: " + syriusConfig.getEndpoint());
            }
        }

        if (partner.getRepliziereDublettenRequestType() != null) {
            logHelper.logInfo(logTransactionId, "will try to send dublette " + AppHelper.objectToXml(partner.getRepliziereDublettenRequestType()));
            prepareWs(partnerReplikationExpServicePortType, wsConfiguration, syriusConfig);

            repliziereResponseType = partnerReplikationExpServicePortType.repliziereDubletten(partner.getRepliziereDublettenRequestType());
            //TODO: Error Handling based on Response
            logHelper.logInfo(logTransactionId, "RepliziereDublette Response " + repliziereResponseType.getReplikationErfolgreich() + ", Accessing endpoint: " + syriusConfig.getEndpoint());
        }

        return true;
    }

    public static boolean isMessageRelevant(changeMdmResponse mdmchangeMessage, String irrelevantSourceSystemName) {
        if (mdmchangeMessage.getBusinessPartnerID().get(0).getIsNaturalPerson() != null &&
                mdmchangeMessage.getBusinessPartnerID().get(0).getIsNaturalPerson().size() > 0 &&
                !mdmchangeMessage.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getTECHSourceSystem().toString().equals(irrelevantSourceSystemName)) {
            return true;
        }
        if (mdmchangeMessage.getBusinessPartnerID().get(0).getIsLegalPerson() != null &&
                mdmchangeMessage.getBusinessPartnerID().get(0).getIsLegalPerson().size() > 0 &&
                !mdmchangeMessage.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getTECHSourceSystem().toString().equals(irrelevantSourceSystemName)) {
            return true;
        }
        List<hasChannelIDEntry> channelList = mdmchangeMessage.getBusinessPartnerID().get(0).getHasChannelID();
        if (channelList != null) {
            for (hasChannelIDEntry channel : channelList) {
                if (!channel.getTECHSourceSystem().toString().equals(irrelevantSourceSystemName)) {
                    return true;
                }
            }
        }
        if (mdmchangeMessage.getBusinessPartnerID().get(0).getHasUnderwritingCodeID() != null &&
                mdmchangeMessage.getBusinessPartnerID().get(0).getHasUnderwritingCodeID().size() > 0 &&
                !mdmchangeMessage.getBusinessPartnerID().get(0).getHasUnderwritingCodeID().get(0).getTECHSourceSystem().toString().equals(irrelevantSourceSystemName)
        ) {
            return true;
        }
        if (mdmchangeMessage.getBusinessPartnerID().get(0).getIsDuplicate() != null &&
                mdmchangeMessage.getBusinessPartnerID().get(0).getIsDuplicate().size() > 0 &&
                !mdmchangeMessage.getBusinessPartnerID().get(0).getIsDuplicate().get(0).getTECHSourceSystem().toString().equals(irrelevantSourceSystemName)
        ) {
            return true;
        }
        return false;
    }

    public void startConsuming() {
        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());

        KafkaConsumer<String, changeMdmResponse> consumer = new KafkaConsumer<>(configure());
        consumer.subscribe(Arrays.asList(syriusConfig.getInputTopic()));

        // initial - set a UUID (will be overwritten for each message)
        String logTransactionId = UUID.randomUUID().toString();

        // read SAML configuration from environment
        WsConfiguration wsConfiguration = null;
        try {
            wsConfiguration = new WsConfiguration();
        } catch (Exception e) {
            logHelper.logError(logTransactionId, "cannot start, SAML environment variables are missing");
            System.exit(1);
        }

        while (true) {
            try {
                while (true) {
                    ConsumerRecords<String, changeMdmResponse> records = consumer.poll(Duration.ofMillis(100));
                    for (ConsumerRecord<String, changeMdmResponse> record : records) {
                        long startTime = System.nanoTime();
                        logTransactionId = LogHelper.getTechMdmPartnerID(record.value().getBusinessPartnerID().get(0))
                                + "|" + record.offset()
                                + "|" + LogHelper.getPartnerId(record.value().getBusinessPartnerID().get(0), LogHelper.ZEPASPARTNERKEY)
                                + "|" + LogHelper.getPartnerId(record.value().getBusinessPartnerID().get(0), LogHelper.SYRIUSPARTNERKEY);

                        logHelper.logInfo(logTransactionId, "offset: " + record.offset() + " key: " + record.key() + " value: " + record.value());
                        changeMdmResponse mdmchangeMessage = record.value();

                        if (isMessageRelevant(mdmchangeMessage, syriusConfig.getSYRIUS())) {

                            PartnerSyrius partnerSyrius = PartnerSyrius.fromChangeMdmResponse(mdmchangeMessage, syriusConfig);
                            sendPartnerToSyrius(partnerSyrius, syriusConfig, partnerReplikationExpServicePortType, logTransactionId, wsConfiguration.getConfiguration(), logHelper);
                        } else {
                            logHelper.logInfo(logTransactionId, "message is not relevant");
                        }
                        consumer.commitSync();
                        logHelper.logInfo(logTransactionId, "Message processing Duration: " + ((System.nanoTime() - startTime) / 1000000) + " ms");
                        logHelper.logInfo(logTransactionId,
                                "successfully processed");
                    }
                }
            } catch (DatatypeConfigurationException e) {
                logHelper.logError(logTransactionId + " Datatype conf error ", e);
            } catch (PathNotFoundException e) {
                logHelper.logError(logTransactionId + " Parsing Error ", e);
            } catch (ApiFaultMessage e) {
                logHelper.logError(logTransactionId + " Syrius Error ", e);
            } catch (Exception e) {
                logHelper.logError(logTransactionId, e);
            } finally {
                consumer.commitSync();
            }
        }
    }


    public static void prepareWs(PartnerReplikationExpServicePortType partnerReplikationExpServicePortType, WsConfiguration.Configuration wsConfiguration, SyriusConfig syriusConfig) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException, TransformerException // throws Exception
    {

        // setup SLL
        syriusConfig.getSslConfig().setupSSL(partnerReplikationExpServicePortType);

        // Generate SAML assertion
        SAMLResponseCreator responseCreator = new SAMLResponseCreator();
        Element samlResponse = responseCreator.getSAMLResponse(wsConfiguration, false);
        String samlResponseString = SAMLResponseCreator.getSamlStringResponse(samlResponse);
        final String samlResponseStringWithUserBase64 = new String(Base64.getEncoder().encode(
                (wsConfiguration.getLoggedInUser() + ":" + samlResponseString).getBytes()));
        // put base64 encoded "userid:samlResponse" into HTTP Header with Key Authorization and value prefixed with "Basic "
        // Note that this is eerily similar to Basic password authentication with user:password
        // so there may be more elegant code using the APIs to do this
        BindingProvider bindingProvider = (BindingProvider) partnerReplikationExpServicePortType;
        Map<String, List<String>> headers = new HashMap<>();
        headers.put("Authorization", Arrays.asList("Basic " + samlResponseStringWithUserBase64));
        bindingProvider.getRequestContext().put(Message.PROTOCOL_HEADERS, headers);

        // prepare further configs
        Client client = ClientProxy.getClient(partnerReplikationExpServicePortType);
        HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
        final HTTPClientPolicy policy = httpConduit.getClient();
        // allow redirection
        policy.setAutoRedirect(true);
        // we need to store some authentication cookies spanning over several redirects
        client.getRequestContext().put(org.apache.cxf.message.Message.MAINTAIN_SESSION, Boolean.TRUE);
        // allow relative redirects
        client.getRequestContext().put("http.redirect.relative.uri", "true");
        // allowing for some looping on same URI
        client.getRequestContext().put("http.redirect.max.same.uri.count", 2);
    }


}
