package com.helvetia.app.quarkus.kafka.mapping;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsErlaubnisCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsErlaubnisartCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsKommVerbTypCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsRechtsformCodeType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.*;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.*;
import com.helvetia.app.mdm.ReferenceDataMapper;
import inboundTrafoZepasLegalPerson.json.Response.RowSchema.isLegalPerson.isLegalPersonEntry;

import javax.enterprise.context.ApplicationScoped;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class JurPersonMapper {

    private static ReferenceDataMapper singleReferenceDataMapper;

    public ReferenceDataMapper getReferenceDataMapper() throws IOException {
        if (singleReferenceDataMapper == null) {
            singleReferenceDataMapper = new ReferenceDataMapper();
        }
        return singleReferenceDataMapper;
    }

    public JurPersonMapper(){
    }

    public RepliziereJurPersonRequestType mapJurPerson(changeMdmResponse changeMdmResponse) throws IOException {

        RepliziereJurPersonRequestType repliziereJurPersonRequestType = new RepliziereJurPersonRequestType();
        WsPartnerReferenzType partnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType partnerIdType = new WsPartnerIdType();
        final String techMdmPartnerID = changeMdmResponse.getBusinessPartnerID().get(0).getIsLegalPerson().get(0).getTECHMDMPartnerID().toString().toLowerCase();
        partnerIdType.setId(techMdmPartnerID);
        partnerReferenzType.setPartnerId(partnerIdType);
        repliziereJurPersonRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());

        // che0wea, 20200722, entferne externeReferenz
//        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
//        wsExterneReferenzType.setId(changeMdmResponse.getBusinessPartnerID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber().get(0).getIDNumber().toString());
//        partnerReferenzType.setExterneReferenz(wsExterneReferenzType);


        repliziereJurPersonRequestType.setPartnerReferenz(partnerReferenzType);

        WsJurPersonStrukturType wsJurPersonStrukturType = new WsJurPersonStrukturType();
        WsJurPersonType wsJurPersonType = new WsJurPersonType();


        isLegalPersonEntry legalPersonEntry = changeMdmResponse.getBusinessPartnerID().get(0).getIsLegalPerson().get(0);
        final XMLGregorianCalendar validFrom = DateHelper.syriusDateFromXsDateWithLimit(legalPersonEntry.getBITEMPValidFrom());

        wsJurPersonType.setStateFrom(validFrom);

        final XMLGregorianCalendar validTo = DateHelper.syriusDateFromXsDateWithLimit(legalPersonEntry.getBITEMPValidTo());
        wsJurPersonType.setStateUpto(validTo);

        if (legalPersonEntry.getLegalPerson().get(0).getClosingDate() != null) {
            final XMLGregorianCalendar closingDate = DateHelper.syriusDateFromXsDateWithLimit(legalPersonEntry.getLegalPerson().get(0).getClosingDate());
            wsJurPersonType.setAufloesungsdatum(closingDate);
        }

        WsRechtsformCodeType wsRechtsformCodeType = new WsRechtsformCodeType();
        String legalType = getReferenceDataMapper().getLegalTypeMapMdmToSyr().get(String.valueOf(legalPersonEntry.getLegalPerson().get(0).getLegalType()));
        wsRechtsformCodeType.setCodeId(legalType);
        wsJurPersonType.setRechtsform(wsRechtsformCodeType);
        wsJurPersonType.setNogaCode(Objects.toString(legalPersonEntry.getLegalPerson().get(0).getNOGACode(), null));
        WsPartnerschutzDefIdType wsPartnerschutzDefIdType = new WsPartnerschutzDefIdType();
        Optional<CharSequence> encryptionCodeVIP = Optional.ofNullable(changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getBusinessPartner().get(0).getEncryptionCodeVIP());

        // TODO: validate if Nogacode wins against encryptioncode
        if (wsJurPersonType.getNogaCode() != null && wsJurPersonType.getNogaCode().equals("653999")) {
            wsPartnerschutzDefIdType.setId("3d4733ea-99e2-46ce-bbb4-70cbe871b212");
        } else if (encryptionCodeVIP.isPresent() && encryptionCodeVIP.get().toString().equals("X")) {
            wsPartnerschutzDefIdType.setId("bc56efd9-beb7-44cb-beb4-1a30968a48f9");
        } else {
            wsPartnerschutzDefIdType.setId("-1");
        }
        wsJurPersonType.setPartnerschutzDefId(wsPartnerschutzDefIdType);


        String sourceTimeStamp = changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getBITEMPSourceTimestamp().toString();
        wsJurPersonType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsJurPersonType.setAenderungDurch(
                Objects.toString(
                        changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).
                                getTECHUser(), null));
        wsJurPersonType.setZusatz1(Objects.toString(legalPersonEntry.getLegalPerson().get(0).
                getNameAddition(), null));
        wsJurPersonType.setZusatz2(Objects.toString(legalPersonEntry.getLegalPerson().get(0).
                getNameAddition2(), null));
        String syriusLanguage = getReferenceDataMapper().getLanguageMapMdmToSyr().get(String.valueOf(legalPersonEntry.getLegalPerson().get(0).getLanguage()));
        wsJurPersonType.setKorrespondenzsprache(syriusLanguage);
//        wsJurPersonType.setMitarbeiterBenutzerId();

        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");
        wsJurPersonType.setMutationsgrundId(wsMutationsgrundIdType);
        wsJurPersonType.setName(legalPersonEntry.getLegalPerson().get(0).getCompanyName().toString());

        wsJurPersonStrukturType.getJurPersonStates().add(wsJurPersonType);

        WsKommVerbVerwendungType wsKommVerbVerwendungType = new WsKommVerbVerwendungType();
        WsKommVerbTypCodeType wsKommVerbTypCodeType = new WsKommVerbTypCodeType();
        wsKommVerbVerwendungType.setVerbindungstyp(wsKommVerbTypCodeType);

        WsErlaubnisCodeType wsErlaubnisCodeType = new WsErlaubnisCodeType();
        if (legalPersonEntry.getLegalPerson().get(0).getNoAds() != null) {
            wsErlaubnisCodeType.setCodeId("-64830"); //keine Werbung
        } else {
            wsErlaubnisCodeType.setCodeId("-64831"); //werbung
        }

        wsKommVerbVerwendungType.setErlaubnis(wsErlaubnisCodeType);
        WsErlaubnisartCodeType wsErlaubnisartCodeType = new WsErlaubnisartCodeType();
        wsErlaubnisartCodeType.setCodeId("-64868"); //Anweisung Kunde
        wsKommVerbVerwendungType.setErlaubnisart(wsErlaubnisartCodeType);
        wsJurPersonStrukturType.getKommVerbVerwendungen().add(wsKommVerbVerwendungType);

        WsPartneridentifikatorIdType techMdmPartnerIDAsPartnerIdentifikatorId = new WsPartneridentifikatorIdType();
        techMdmPartnerIDAsPartnerIdentifikatorId.setId(techMdmPartnerID);

        WsPartneridentifikatorType wsPartneridentifikatorType = new WsPartneridentifikatorType();
        wsPartneridentifikatorType.setId(techMdmPartnerIDAsPartnerIdentifikatorId);
        WsPartneridentifikatorDefIdType wsPartneridentifikatorDefIdType = new WsPartneridentifikatorDefIdType();
        wsPartneridentifikatorDefIdType.setId("SYR_Partner_PartnerNr");
        wsPartneridentifikatorType.setPartneridentifikatorDefId(wsPartneridentifikatorDefIdType);
        wsPartneridentifikatorType.setWert(changeMdmResponse.getBusinessPartnerID().get(0).getHasIdentificationNumber().
                get(0).getIdentificationNumber().get(0).getIDNumber().toString());
        wsJurPersonStrukturType.getPartneridentifikatoren().add(wsPartneridentifikatorType);
        //wsJurPersonStrukturType.setGueltAb(wsJurPersonType.getStateFrom());
        wsJurPersonStrukturType.setGueltBis(wsJurPersonType.getStateUpto());

        String UID = Objects.toString(legalPersonEntry.getLegalPerson().get(0).getUID(), null);
        if (UID != null) {
            UID = UID.replace(".", "").replace("-", "").toUpperCase();
            WsPartneridentifikatorType wsPartneridentifikatorType2 = new WsPartneridentifikatorType();
            wsPartneridentifikatorType2.setId(techMdmPartnerIDAsPartnerIdentifikatorId);
            WsPartneridentifikatorDefIdType wsPartneridentifikatorDefIdType2 = new WsPartneridentifikatorDefIdType();
            wsPartneridentifikatorDefIdType2.setId("SYR_Partner_UidNr");
            wsPartneridentifikatorType2.setPartneridentifikatorDefId(wsPartneridentifikatorDefIdType2);
            wsPartneridentifikatorType2.setWert(UID);
            wsJurPersonStrukturType.getPartneridentifikatoren().add(wsPartneridentifikatorType2);
        }

        String FinmaNr = Objects.toString(legalPersonEntry.getLegalPerson().get(0).getFINMANumber(), "n/a");
        WsPartneridentifikatorType wsPartneridentifikatorType3 = new WsPartneridentifikatorType();
        wsPartneridentifikatorType3.setGueltAb(wsJurPersonType.getStateFrom());
        wsPartneridentifikatorType3.setGueltBis(wsJurPersonType.getStateUpto());
        wsPartneridentifikatorType3.setId(techMdmPartnerIDAsPartnerIdentifikatorId);
        WsPartneridentifikatorDefIdType wsPartneridentifikatorDefIdType3 = new WsPartneridentifikatorDefIdType();
        wsPartneridentifikatorDefIdType3.setId("2890e963-c97f-491e-bb55-b431391f88d2");
        wsPartneridentifikatorType3.setPartneridentifikatorDefId(wsPartneridentifikatorDefIdType3);
        wsPartneridentifikatorType3.setWert(FinmaNr);
        wsJurPersonStrukturType.getPartneridentifikatoren().add(wsPartneridentifikatorType3);

        String mdmTimeStamp = changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0)
                .getBITEMPMDMTimestamp().toString();
        long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4,6)),
                Integer.parseInt(mdmTimeStamp.substring(6,8)), Integer.parseInt(mdmTimeStamp.substring(8,10)),
                Integer.parseInt(mdmTimeStamp.substring(10,12)), Integer.parseInt(mdmTimeStamp.substring(12,14)),
                Integer.parseInt(mdmTimeStamp.substring(14,17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;
        wsJurPersonStrukturType.setExterneVersion(epoch); //nanoseconds since epoch
        repliziereJurPersonRequestType.setJurPersonStruktur(wsJurPersonStrukturType);
        return repliziereJurPersonRequestType;
    }
}
