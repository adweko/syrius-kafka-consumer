package com.helvetia.app.quarkus.kafka.streams.specificlogic;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.helvetia.app.quarkus.kafka.streams.RelationToEntity;
import com.helvetia.app.quarkus.kafka.streams.model.TechMDMPartnerID;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class RelationToEntityImpl implements RelationToEntity<changeMdmResponse, TechMDMPartnerID> {
    @Override
    public boolean canBeRelatedToEntity(changeMdmResponse message) {
        final CharSequence techmdmPartnerID = getTechmdmPartnerID(message);
        return techmdmPartnerID != null && !techmdmPartnerID.toString().isEmpty();
    }

    @Override
    public TechMDMPartnerID extractEntity(changeMdmResponse message) {
        final CharSequence techmdmPartnerIDChars = getTechmdmPartnerID(message);
        return TechMDMPartnerID.fromRawValueString(techmdmPartnerIDChars.toString());
    }

    private CharSequence getTechmdmPartnerID(changeMdmResponse message) {
        return message.getBusinessPartnerID().get(0).getTECHMDMPartnerID();
    }
}
