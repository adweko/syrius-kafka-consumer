package com.helvetia.app.quarkus.kafka.streams.specificlogic;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.ApiFaultMessage;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.PartnerReplikationExpServicePortType;
import com.helvetia.app.quarkus.kafka.PartnerSyrius;
import com.helvetia.app.quarkus.kafka.SyriusConsumer;
import com.helvetia.app.quarkus.kafka.config.SyriusConfig;
import com.helvetia.app.quarkus.kafka.config.WsConfiguration;
import com.helvetia.app.quarkus.kafka.logging.LogHelper;
import com.helvetia.app.quarkus.kafka.streams.MapperAndSender;
import com.helvetia.app.quarkus.kafka.streams.exception.dlq.MappingException;
import com.helvetia.app.quarkus.kafka.streams.exception.dlq.SyriusDataQualityException;
import com.helvetia.app.quarkus.kafka.streams.model.ResultOrExceptionWithInput;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.ws.BindingProvider;

@ApplicationScoped
public class MapperAndSenderImpl implements MapperAndSender<changeMdmResponse, Boolean> {
    @Inject
    SyriusConfig syriusConfig;

    @Inject
    WsConfiguration wsConfiguration;

    PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = null;

    @Inject
    LogHelper logHelper;

    @Override
    public ResultOrExceptionWithInput<changeMdmResponse, Boolean> mapAndSend(changeMdmResponse v) {
        String logTransactionId = "";
        try {
            logTransactionId = LogHelper.getTechMdmPartnerID(v.getBusinessPartnerID().get(0))
                    + "|" + LogHelper.getPartnerId(v.getBusinessPartnerID().get(0), LogHelper.ZEPASPARTNERKEY)
                    + "|" + LogHelper.getPartnerId(v.getBusinessPartnerID().get(0), LogHelper.SYRIUSPARTNERKEY);
        } catch (Exception e) {
            logHelper.logError("", "could not extract transaction id from message");
        }

        try {
            if (partnerReplikationExpServicePortType == null) {
                partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
                ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());
            }



            logHelper.logInfo(logTransactionId, "value: " + v);
            PartnerSyrius partnerSyrius;
            try {
                partnerSyrius = PartnerSyrius.fromChangeMdmResponse(v, syriusConfig);
            } catch (Exception e) {
                logHelper.logError(logTransactionId, e);
                throw new MappingException(e);
            }
            final boolean success = SyriusConsumer.sendPartnerToSyrius(partnerSyrius, syriusConfig, partnerReplikationExpServicePortType, logTransactionId, wsConfiguration.getConfiguration(), logHelper);
            return new ResultOrExceptionWithInput<>(success);
        } catch (Exception exception) {
            logHelper.logError(logTransactionId, exception);
            Exception classifiedException = classifyException(exception, logTransactionId);
            return new ResultOrExceptionWithInput<>(v, classifiedException);
        }
    }

    /**
     * Rethrow as DLQException if appropriate
     */
    private Exception classifyException(Exception e, String trId) {
        // wrap here any known DQ exceptions into subclasses of DLQException
        if ( e instanceof ApiFaultMessage) {
            logHelper.logError(trId, "Classifying " + ApiFaultMessage.class.getSimpleName() + " as a Syrius data quality exception. Will go to DLQ.");
            return new SyriusDataQualityException(e);
        }
        return e;
    }


}
