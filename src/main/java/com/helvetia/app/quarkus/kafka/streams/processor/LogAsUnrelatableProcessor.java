package com.helvetia.app.quarkus.kafka.streams.processor;
import changePartner.com.helvetia.custom.changeMdmResponse;
import com.helvetia.app.quarkus.kafka.logging.LogHelper;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
/**
 * Need an extra Processor for this, since topic, partition and offset are not directly accessible in KStreams
 */
@ApplicationScoped
public class LogAsUnrelatableProcessor implements Processor<String, changeMdmResponse> {
    private ProcessorContext context;

    private ProcessorContext context() {
        return context;
    }
    @Override
    public void init(ProcessorContext processorContext) {
        this.context = processorContext;
    }
    @Override
    public void close() {
    }
    @Inject
    LogHelper logHelper;
    public LogAsUnrelatableProcessor(LogHelper logHelper){
        this.logHelper = logHelper;
    }
    @Override
    public void process(String key, changeMdmResponse value) {
        logHelper.logError(null, "message not relatable to Partner. Topic/Partition/Offset: " +
                context().topic() + " / " + context().partition() + " / " + context().offset());
    }
}