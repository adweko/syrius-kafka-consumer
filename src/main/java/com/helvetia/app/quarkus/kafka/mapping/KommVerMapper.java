package com.helvetia.app.quarkus.kafka.mapping;

import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsErlaubnisCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsErlaubnisartCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsKommVerbTypCodeType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.*;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.*;
import com.helvetia.app.mdm.ReferenceDataMapper;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDEntry;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasEmail.hasEmailEntry;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasFax.hasFaxEntry;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasMobile.hasMobileEntry;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSkype.hasSkypeEntry;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasTelephone.hasTelephoneEntry;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasTelephone.hasTelephoneSchema.Telephone.TelephoneEntry;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry;

import javax.enterprise.context.ApplicationScoped;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;
import java.util.UUID;

@ApplicationScoped
public class KommVerMapper {

    private static ReferenceDataMapper singleReferenceDataMapper;

    public ReferenceDataMapper getReferenceDataMapper() throws IOException {
        if (singleReferenceDataMapper == null) {
            singleReferenceDataMapper = new ReferenceDataMapper();
        }
        return singleReferenceDataMapper;
    }

    public KommVerMapper() {
    }

    //1
    public RepliziereTelefonRequestType mapTelefon(hasChannelIDEntry channel) throws IOException {
        RepliziereTelefonRequestType repliziereTelefonRequestType = new RepliziereTelefonRequestType();

        String sourceTimeStamp = channel.getBITEMPSourceTimestamp().toString();
        String techUser = Objects.toString(channel.getTECHUser(), null);

        repliziereTelefonRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());
        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
        wsPartnerIdType.setId(channel.getTECHMDMPartnerID().toString().toLowerCase());
        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
        repliziereTelefonRequestType.setPartnerReferenz(wsPartnerReferenzType);

        WsTelefonReferenzType wsTelefonReferenzType = new WsTelefonReferenzType();
        WsTelefonIdType wsTelefonIdType = new WsTelefonIdType();
        wsTelefonIdType.setId(channel.getChannelID().get(0).getChannelID().toString().toLowerCase());
        wsTelefonReferenzType.setTelefonId(wsTelefonIdType);
        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
        wsExterneReferenzType.setId(channel.getChannelID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber().get(0).getIDNumber().toString());
        wsTelefonReferenzType.setExterneReferenz(wsExterneReferenzType);
        repliziereTelefonRequestType.setTelefonReferenz(wsTelefonReferenzType);


        WsTelefonStrukturType wsTelefonStrukturType = new WsTelefonStrukturType();
        TelephoneEntry telephoneEntry =
                channel.getChannelID().get(0).getHasTelephone().get(0).getTelephone().get(0);

        hasTelephoneEntry hasTelephoneEntry = channel.getChannelID().get(0).getHasTelephone().get(0);
        WsTelefonType wsTelefonType = new WsTelefonType();
        wsTelefonType.setTelefonNummer(telephoneEntry.getTelephoneNumber().toString());
        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");

        wsTelefonType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsTelefonType.setAenderungDurch(techUser);

        wsTelefonType.setMutationsgrundId(wsMutationsgrundIdType);
        XMLGregorianCalendar validFrom = DateHelper.syriusDateFromXsDateWithLimit(hasTelephoneEntry.getBITEMPValidFrom());
        wsTelefonType.setStateFrom(validFrom);

        if (channel.getChannelID().get(0).getHasTelephone().get(0).getBITEMPDeleted() != null && channel.getChannelID().get(0).getHasTelephone().get(0).getBITEMPDeleted().toString().equals("X")) {
            wsTelefonType.setStateUpto(wsTelefonType.getStateFrom());
            wsTelefonStrukturType.setGueltBis(wsTelefonType.getStateFrom());
        } else {
            XMLGregorianCalendar validTo = DateHelper.syriusDateFromXsDateWithLimit(hasTelephoneEntry.getBITEMPValidTo());
            wsTelefonType.setStateUpto(validTo);
            wsTelefonStrukturType.setGueltBis(wsTelefonType.getStateUpto());
            //wsTelefonStrukturType.setGueltAb(wsTelefonType.getStateFrom());
        }
        wsTelefonStrukturType.getTelefonStates().add(wsTelefonType);

        WsTelefonDefIdType wsTelefonDefIdType = new WsTelefonDefIdType();
        ChannelEntry channelEntry =
                channel.getChannelID().get(0).getIsChannel().get(0).getChannel().get(0);
        String telefonDefIdType = getReferenceDataMapper().getCommunicationMapMdmToSyr().get(String.valueOf(channelEntry.getChannelNote()));
        wsTelefonDefIdType.setId(telefonDefIdType);
        wsTelefonStrukturType.setTelefonDefId(wsTelefonDefIdType);


        WsKommVerbVerwendungType wsKommVerbVerwendungType = new WsKommVerbVerwendungType();
        WsErlaubnisCodeType wsErlaubnisCodeType = new WsErlaubnisCodeType();
        if (channelEntry.getNoAds() != null) {
            wsErlaubnisCodeType.setCodeId("-64830");
        } else {
            wsErlaubnisCodeType.setCodeId("-64831");
        }
        wsKommVerbVerwendungType.setErlaubnis(wsErlaubnisCodeType);
        WsErlaubnisartCodeType wsErlaubnisartCodeType = new WsErlaubnisartCodeType();
        wsErlaubnisartCodeType.setCodeId("-64868");
        wsKommVerbVerwendungType.setErlaubnisart(wsErlaubnisartCodeType);

        WsKommVerbTypCodeType wsKommVerbTypCodeType = new WsKommVerbTypCodeType();
        wsKommVerbTypCodeType.setCodeId("-64827"); //Telefon
        wsKommVerbVerwendungType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsKommVerbVerwendungType.setAenderungDurch(techUser);
        wsKommVerbVerwendungType.setVerbindungstyp(wsKommVerbTypCodeType);
        wsTelefonStrukturType.setKommVerbVerwendung(wsKommVerbVerwendungType);
        String mdmTimeStamp = channel.getBITEMPMDMTimestamp().toString();
        long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4,6)),
                Integer.parseInt(mdmTimeStamp.substring(6,8)), Integer.parseInt(mdmTimeStamp.substring(8,10)),
                Integer.parseInt(mdmTimeStamp.substring(10,12)), Integer.parseInt(mdmTimeStamp.substring(12,14)),
                Integer.parseInt(mdmTimeStamp.substring(14,17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;
        wsTelefonStrukturType.setExterneVersion(epoch);
        repliziereTelefonRequestType.setTelefonStruktur(wsTelefonStrukturType);

        return repliziereTelefonRequestType;
    }

    //2
    public RepliziereTelefonRequestType mapMobile(hasChannelIDEntry channel) throws IOException {
        RepliziereTelefonRequestType repliziereTelefonRequestType = new RepliziereTelefonRequestType();

        String sourceTimeStamp = channel.getBITEMPSourceTimestamp().toString();
        String techUser = Objects.toString(channel.getTECHUser(), null);

        repliziereTelefonRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());
        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
        wsPartnerIdType.setId(channel.getTECHMDMPartnerID().toString().toLowerCase());
        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
        repliziereTelefonRequestType.setPartnerReferenz(wsPartnerReferenzType);

        WsTelefonReferenzType wsTelefonReferenzType = new WsTelefonReferenzType();
        WsTelefonIdType wsTelefonIdType = new WsTelefonIdType();
        wsTelefonIdType.setId(channel.getChannelID().get(0).getChannelID().toString().toLowerCase());
        wsTelefonReferenzType.setTelefonId(wsTelefonIdType);
        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
        wsExterneReferenzType.setId(channel.getChannelID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber().get(0).getIDNumber().toString());
        wsTelefonReferenzType.setExterneReferenz(wsExterneReferenzType);
        repliziereTelefonRequestType.setTelefonReferenz(wsTelefonReferenzType);

        WsTelefonStrukturType wsTelefonStrukturType = new WsTelefonStrukturType();
        WsTelefonType wsTelefonType = new WsTelefonType();
        hasMobileEntry hasMobileEntry = channel.getChannelID().get(0).getHasMobile().get(0);
        wsTelefonType.setTelefonNummer(channel.getChannelID().get(0).getHasMobile().get(0).getMobile().get(0).getMobileNumber().toString());
        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");
        wsTelefonType.setMutationsgrundId(wsMutationsgrundIdType);
        final XMLGregorianCalendar validFrom = DateHelper.syriusDateFromXsDateWithLimit(hasMobileEntry.getBITEMPValidFrom());
        wsTelefonType.setStateFrom(validFrom);
        wsTelefonType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsTelefonType.setAenderungDurch(techUser);

        if (channel.getChannelID().get(0).getHasMobile().get(0).getBITEMPDeleted() != null && channel.getChannelID().get(0).getHasMobile().get(0).getBITEMPDeleted().toString().equals("X")) {
            wsTelefonType.setStateUpto(wsTelefonType.getStateFrom());
            wsTelefonStrukturType.setGueltBis(wsTelefonType.getStateFrom());
        } else {
            final XMLGregorianCalendar validTo = DateHelper.syriusDateFromXsDateWithLimit(hasMobileEntry.getBITEMPValidTo());
            wsTelefonType.setStateUpto(validTo);
            wsTelefonStrukturType.setGueltBis(wsTelefonType.getStateUpto());
            //wsTelefonStrukturType.setGueltAb(wsTelefonType.getStateFrom());
        }
        wsTelefonStrukturType.getTelefonStates().add(wsTelefonType);

        WsTelefonDefIdType wsTelefonDefIdType = new WsTelefonDefIdType();
        ChannelEntry channelEntry =
                channel.getChannelID().get(0).getIsChannel().get(0).getChannel().get(0);
        String telefonDefIdType = getReferenceDataMapper().getCommunicationMapMdmToSyr().get(String.valueOf(channelEntry.getChannelNote()));
        wsTelefonDefIdType.setId(telefonDefIdType);
        wsTelefonStrukturType.setTelefonDefId(wsTelefonDefIdType);

        WsKommVerbVerwendungType wsKommVerbVerwendungType = new WsKommVerbVerwendungType();
        WsErlaubnisCodeType wsErlaubnisCodeType = new WsErlaubnisCodeType();
        if (channelEntry.getNoAds() != null) {
            wsErlaubnisCodeType.setCodeId("-64830");

        } else {
            wsErlaubnisCodeType.setCodeId("-64831");
        }
        wsKommVerbVerwendungType.setErlaubnis(wsErlaubnisCodeType);
        WsErlaubnisartCodeType wsErlaubnisartCodeType = new WsErlaubnisartCodeType();
        wsErlaubnisartCodeType.setCodeId("-64868");
        wsKommVerbVerwendungType.setErlaubnisart(wsErlaubnisartCodeType);
        WsKommVerbTypCodeType wsKommVerbTypCodeType = new WsKommVerbTypCodeType();
        wsKommVerbTypCodeType.setCodeId("-65180"); //Mobile
        wsKommVerbVerwendungType.setVerbindungstyp(wsKommVerbTypCodeType);
        wsKommVerbVerwendungType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsKommVerbVerwendungType.setAenderungDurch(techUser);

        wsTelefonStrukturType.setKommVerbVerwendung(wsKommVerbVerwendungType);
        String mdmTimeStamp = channel.getBITEMPMDMTimestamp().toString();
        long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4,6)),
                Integer.parseInt(mdmTimeStamp.substring(6,8)), Integer.parseInt(mdmTimeStamp.substring(8,10)),
                Integer.parseInt(mdmTimeStamp.substring(10,12)), Integer.parseInt(mdmTimeStamp.substring(12,14)),
                Integer.parseInt(mdmTimeStamp.substring(14,17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;
        wsTelefonStrukturType.setExterneVersion(epoch);
        repliziereTelefonRequestType.setTelefonStruktur(wsTelefonStrukturType);

        return repliziereTelefonRequestType;
    }

    //3
    public RepliziereTelefonRequestType mapFax(hasChannelIDEntry channel) throws IOException {
        RepliziereTelefonRequestType repliziereTelefonRequestType = new RepliziereTelefonRequestType();

        String sourceTimeStamp = channel.getBITEMPSourceTimestamp().toString();
        String techUser = Objects.toString(channel.getTECHUser(), null);

        repliziereTelefonRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());
        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
        wsPartnerIdType.setId(channel.getTECHMDMPartnerID().toString().toLowerCase());
        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
        repliziereTelefonRequestType.setPartnerReferenz(wsPartnerReferenzType);

        WsTelefonReferenzType wsTelefonReferenzType = new WsTelefonReferenzType();
        WsTelefonIdType wsTelefonIdType = new WsTelefonIdType();
        wsTelefonIdType.setId(channel.getChannelID().get(0).getChannelID().toString().toLowerCase());
        wsTelefonReferenzType.setTelefonId(wsTelefonIdType);
        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
        wsExterneReferenzType.setId(channel.getChannelID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber().get(0).getIDNumber().toString());
        wsTelefonReferenzType.setExterneReferenz(wsExterneReferenzType);
        repliziereTelefonRequestType.setTelefonReferenz(wsTelefonReferenzType);


        WsTelefonStrukturType wsTelefonStrukturType = new WsTelefonStrukturType();

        hasFaxEntry hasFaxEntry = channel.getChannelID().get(0).getHasFax().get(0);
        WsTelefonType wsTelefonType = new WsTelefonType();
        wsTelefonType.setTelefonNummer(hasFaxEntry.getFax().get(0).getFaxNumber().toString());
        wsTelefonType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsTelefonType.setAenderungDurch(techUser);
        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");
        wsTelefonType.setMutationsgrundId(wsMutationsgrundIdType);
        final XMLGregorianCalendar validFrom = DateHelper.syriusDateFromXsDateWithLimit(hasFaxEntry.getBITEMPValidFrom());
        wsTelefonType.setStateFrom(validFrom);

        if (channel.getChannelID().get(0).getHasFax().get(0).getBITEMPDeleted() != null && channel.getChannelID().get(0).getHasFax().get(0).getBITEMPDeleted().toString().equals("X")) {
            wsTelefonType.setStateUpto(wsTelefonType.getStateFrom());
            wsTelefonStrukturType.setGueltBis(wsTelefonType.getStateFrom());
        } else {
            final XMLGregorianCalendar validTo = DateHelper.syriusDateFromXsDateWithLimit(hasFaxEntry.getBITEMPValidTo());
            wsTelefonType.setStateUpto(validTo);
            wsTelefonStrukturType.setGueltBis(wsTelefonType.getStateUpto());
            //wsTelefonStrukturType.setGueltAb(wsTelefonType.getStateFrom());
        }
        wsTelefonStrukturType.getTelefonStates().add(wsTelefonType);

        ChannelEntry channelEntry =
                channel.getChannelID().get(0).getIsChannel().get(0).getChannel().get(0);
        WsTelefonDefIdType wsTelefonDefIdType = new WsTelefonDefIdType();
        String telefonDefIdType = getReferenceDataMapper().getCommunicationMapMdmToSyr().get(String.valueOf(channelEntry.getChannelNote()));
        wsTelefonDefIdType.setId(telefonDefIdType);
        wsTelefonStrukturType.setTelefonDefId(wsTelefonDefIdType);


        WsKommVerbVerwendungType wsKommVerbVerwendungType = new WsKommVerbVerwendungType();
        WsErlaubnisCodeType wsErlaubnisCodeType = new WsErlaubnisCodeType();
        if (channelEntry.getNoAds() != null) {
            wsErlaubnisCodeType.setCodeId("-64830");

        } else {
            wsErlaubnisCodeType.setCodeId("-64831");
        }
        wsKommVerbVerwendungType.setErlaubnis(wsErlaubnisCodeType);
        WsErlaubnisartCodeType wsErlaubnisartCodeType = new WsErlaubnisartCodeType();
        wsErlaubnisartCodeType.setCodeId("-64868");
        wsKommVerbVerwendungType.setErlaubnisart(wsErlaubnisartCodeType);
        wsKommVerbVerwendungType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsKommVerbVerwendungType.setAenderungDurch(techUser);

        WsKommVerbTypCodeType wsKommVerbTypCodeType = new WsKommVerbTypCodeType();
        wsKommVerbTypCodeType.setCodeId("-64885"); //Fax
        wsKommVerbVerwendungType.setVerbindungstyp(wsKommVerbTypCodeType);
        wsTelefonStrukturType.setKommVerbVerwendung(wsKommVerbVerwendungType);
        String mdmTimeStamp = channel.getBITEMPMDMTimestamp().toString();
        long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4,6)),
                Integer.parseInt(mdmTimeStamp.substring(6,8)), Integer.parseInt(mdmTimeStamp.substring(8,10)),
                Integer.parseInt(mdmTimeStamp.substring(10,12)), Integer.parseInt(mdmTimeStamp.substring(12,14)),
                Integer.parseInt(mdmTimeStamp.substring(14,17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;
        wsTelefonStrukturType.setExterneVersion(epoch);
        repliziereTelefonRequestType.setTelefonStruktur(wsTelefonStrukturType);

        return repliziereTelefonRequestType;
    }

    //4
    public RepliziereEmailRequestType mapMail(hasChannelIDEntry channel) throws IOException {
        RepliziereEmailRequestType repliziereEmailRequestType = new RepliziereEmailRequestType();

        String sourceTimeStamp = channel.getBITEMPSourceTimestamp().toString();
        String techUser = Objects.toString(channel.getTECHUser(), null);

        repliziereEmailRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());
        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
        wsPartnerIdType.setId(channel.getTECHMDMPartnerID().toString().toLowerCase());
        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
        repliziereEmailRequestType.setPartnerReferenz(wsPartnerReferenzType);

        WsEmailReferenzType wsEmailReferenzType = new WsEmailReferenzType();
        WsEmailIdType wsEmailIdType = new WsEmailIdType();
        wsEmailIdType.setId(channel.getChannelID().get(0).getChannelID().toString().toLowerCase());
        wsEmailReferenzType.setEmailId(wsEmailIdType);
        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
        wsExterneReferenzType.setId(channel.getChannelID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber().get(0).getIDNumber().toString());
        wsEmailReferenzType.setExterneReferenz(wsExterneReferenzType);
        repliziereEmailRequestType.setEmailReferenz(wsEmailReferenzType);


        WsEmailStrukturType wsEmailStrukturType = new WsEmailStrukturType();

        hasEmailEntry hasEmailEntry = channel.getChannelID().get(0).getHasEmail().get(0);
        WsEmailType wsEmailType = new WsEmailType();
        wsEmailType.setEmailAdresse(hasEmailEntry.getEmail().get(0).getEmailAddress().toString());
        wsEmailType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsEmailType.setAenderungDurch(techUser);
        //wsEmailType.setIsDefStandard();
        //wsEmailType.setIsStandard();
        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");
        wsEmailType.setMutationsgrundId(wsMutationsgrundIdType);

        final XMLGregorianCalendar validFrom = DateHelper.syriusDateFromXsDateWithLimit(hasEmailEntry.getBITEMPValidFrom());
        wsEmailType.setStateFrom(validFrom);

        if (channel.getChannelID().get(0).getHasEmail().get(0).getBITEMPDeleted() != null && channel.getChannelID().get(0).getHasEmail().get(0).getBITEMPDeleted().toString().equals("X")) {
            wsEmailType.setStateUpto(wsEmailType.getStateFrom());
            wsEmailStrukturType.setGueltBis(wsEmailType.getStateFrom());
        } else {
            final XMLGregorianCalendar validTo = DateHelper.syriusDateFromXsDateWithLimit(hasEmailEntry.getBITEMPValidTo());
            wsEmailType.setStateUpto(validTo);
            wsEmailStrukturType.setGueltBis(wsEmailType.getStateUpto());
            //wsEmailStrukturType.setGueltAb(wsEmailType.getStateFrom());
        }
        wsEmailStrukturType.getEmailStates().add(wsEmailType);

        ChannelEntry channelEntry =
                channel.getChannelID().get(0).getIsChannel().get(0).getChannel().get(0);
        WsEmailDefIdType wsEmailDefIdType = new WsEmailDefIdType();
        String emailDefId = getReferenceDataMapper().getCommunicationMapMdmToSyr().get(String.valueOf(channelEntry.getChannelNote()));
        wsEmailDefIdType.setId(emailDefId); //Privat, Geschäft etc.
        wsEmailStrukturType.setEmailDefId(wsEmailDefIdType);


        WsKommVerbVerwendungType wsKommVerbVerwendungType = new WsKommVerbVerwendungType();
        WsErlaubnisCodeType wsErlaubnisCodeType = new WsErlaubnisCodeType();

        if (channelEntry.getNoAds() != null) {
            wsErlaubnisCodeType.setCodeId("-64830");

        } else {
            wsErlaubnisCodeType.setCodeId("-64831");
        }
        wsKommVerbVerwendungType.setErlaubnis(wsErlaubnisCodeType);
        WsErlaubnisartCodeType wsErlaubnisartCodeType = new WsErlaubnisartCodeType();
        wsErlaubnisartCodeType.setCodeId("-64868");
        wsKommVerbVerwendungType.setErlaubnisart(wsErlaubnisartCodeType);
        wsKommVerbVerwendungType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsKommVerbVerwendungType.setAenderungDurch(techUser);

        WsKommVerbTypCodeType wsKommVerbTypCodeType = new WsKommVerbTypCodeType();
        wsKommVerbTypCodeType.setCodeId("-64826"); //Email
        wsKommVerbVerwendungType.setVerbindungstyp(wsKommVerbTypCodeType);
        wsEmailStrukturType.setKommVerbVerwendung(wsKommVerbVerwendungType);
        String mdmTimeStamp = channel.getBITEMPMDMTimestamp().toString();
        long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4,6)),
                Integer.parseInt(mdmTimeStamp.substring(6,8)), Integer.parseInt(mdmTimeStamp.substring(8,10)),
                Integer.parseInt(mdmTimeStamp.substring(10,12)), Integer.parseInt(mdmTimeStamp.substring(12,14)),
                Integer.parseInt(mdmTimeStamp.substring(14,17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;
        wsEmailStrukturType.setExterneVersion(epoch);
        repliziereEmailRequestType.setEmailStruktur(wsEmailStrukturType);


        return repliziereEmailRequestType;
    }

    //5
    public RepliziereWebRequestType mapSkype(hasChannelIDEntry channel) throws IOException {
        RepliziereWebRequestType repliziereWebRequestType = new RepliziereWebRequestType();

        String sourceTimeStamp = channel.getBITEMPSourceTimestamp().toString();
        String techUser = Objects.toString(channel.getTECHUser(), null);

        repliziereWebRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());
        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
        wsPartnerIdType.setId(channel.getTECHMDMPartnerID().toString().toLowerCase());
        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
        repliziereWebRequestType.setPartnerReferenz(wsPartnerReferenzType);

        WsWebReferenzType wsWebReferenzType = new WsWebReferenzType();
        WsWebIdType wsWebIdType = new WsWebIdType();
        wsWebIdType.setId(channel.getChannelID().get(0).getChannelID().toString().toLowerCase());
        wsWebReferenzType.setWebId(wsWebIdType);
        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
        wsExterneReferenzType.setId(channel.getChannelID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber().get(0).getIDNumber().toString());
        wsWebReferenzType.setExterneReferenz(wsExterneReferenzType);
        repliziereWebRequestType.setWebReferenz(wsWebReferenzType);

        WsWebStrukturType wsWebStrukturType = new WsWebStrukturType();

        WsWebType wsWebType = new WsWebType();
        hasSkypeEntry hasSkypeEntry =
                channel.getChannelID().get(0).getHasSkype().get(0);
        wsWebType.setUrl(hasSkypeEntry.getSkype().get(0).getSkypeAddress().toString());

        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");
        wsWebType.setMutationsgrundId(wsMutationsgrundIdType);
        wsWebType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsWebType.setAenderungDurch(techUser);
        final XMLGregorianCalendar validFrom = DateHelper.syriusDateFromXsDateWithLimit(hasSkypeEntry.getBITEMPValidFrom());
        wsWebType.setStateFrom(validFrom);

        if (channel.getChannelID().get(0).getHasSkype().get(0).getBITEMPDeleted() != null && channel.getChannelID().get(0).getHasSkype().get(0).getBITEMPDeleted().toString().equals("X")) {
            wsWebType.setStateUpto(wsWebType.getStateFrom());
            wsWebStrukturType.setGueltBis(wsWebType.getStateFrom());
        } else {
            final XMLGregorianCalendar validTo = DateHelper.syriusDateFromXsDateWithLimit(hasSkypeEntry.getBITEMPValidTo());
            wsWebType.setStateUpto(validTo);
            wsWebStrukturType.setGueltBis(wsWebType.getStateUpto());
            //wsWebStrukturType.setGueltAb(wsWebType.getStateFrom());
        }
        wsWebStrukturType.getWebStates().add(wsWebType);

        ChannelEntry channelEntry =
                channel.getChannelID().get(0).getIsChannel().get(0).getChannel().get(0);
        WsWebDefIdType wsWebDefIdType = new WsWebDefIdType();
        String skypeDefID = getReferenceDataMapper().getCommunicationMapMdmToSyr().get(String.valueOf(channelEntry.getChannelNote()));
        wsWebDefIdType.setId(skypeDefID);
        wsWebStrukturType.setWebDefId(wsWebDefIdType);


        WsKommVerbVerwendungType wsKommVerbVerwendungType = new WsKommVerbVerwendungType();
        WsErlaubnisCodeType wsErlaubnisCodeType = new WsErlaubnisCodeType();
        if (channelEntry.getNoAds() != null) {
            wsErlaubnisCodeType.setCodeId("-64830");

        } else {
            wsErlaubnisCodeType.setCodeId("-64831");
        }
        wsKommVerbVerwendungType.setErlaubnis(wsErlaubnisCodeType);
        wsKommVerbVerwendungType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsKommVerbVerwendungType.setAenderungDurch(techUser);
        WsErlaubnisartCodeType wsErlaubnisartCodeType = new WsErlaubnisartCodeType();
        wsErlaubnisartCodeType.setCodeId("-64868");
        wsKommVerbVerwendungType.setErlaubnisart(wsErlaubnisartCodeType);
        wsWebStrukturType.setKommVerbVerwendung(wsKommVerbVerwendungType);
        String mdmTimeStamp = channel.getBITEMPMDMTimestamp().toString();
        long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4,6)),
                Integer.parseInt(mdmTimeStamp.substring(6,8)), Integer.parseInt(mdmTimeStamp.substring(8,10)),
                Integer.parseInt(mdmTimeStamp.substring(10,12)), Integer.parseInt(mdmTimeStamp.substring(12,14)),
                Integer.parseInt(mdmTimeStamp.substring(14,17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;
        wsWebStrukturType.setExterneVersion(epoch);
        repliziereWebRequestType.setWebStruktur(wsWebStrukturType);


        return repliziereWebRequestType;
    }
}
