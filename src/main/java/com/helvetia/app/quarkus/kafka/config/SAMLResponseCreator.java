/*
 * Copyright (C) 2018 Helvetia Versicherungen. All Rights Reserved.
 */
package com.helvetia.app.quarkus.kafka.config;

import com.helvetia.security.saml.SAMLAssertion;
import com.helvetia.security.saml.SAMLFactory;
import com.helvetia.security.saml.SAMLResponse;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * Helper class to create SAML response
 *
 * @author CHE0BMC
 */
public class SAMLResponseCreator {

    public Element getSAMLResponse(WsConfiguration.Configuration configuration, boolean printResponse) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, TransformerException
    // throws Exception
            {
        InputStream in = new ByteArrayInputStream(configuration.getSamltokenSignerKeystore());
        KeyStore store = KeyStore.getInstance("JKS");
        store.load(in, null);

        Calendar cal = Calendar.getInstance();
        Date dateNow = Date.from(LocalDateTime.now().minusSeconds(20).atZone(ZoneId.systemDefault()).toInstant());
        cal.setTime(dateNow);
        final String password = configuration.getSamltokenSignerPassword();

        SAMLFactory factory = SAMLFactory.getInstance();
        factory.initialize(store);

        final String issuer = "ch_default-saml_token";

        SAMLAssertion samlAssertion = new SAMLAssertion();
        samlAssertion.setIdPrefix("aid");
        samlAssertion.setIncludeSignerCert(true);
        samlAssertion.setIssueInstant(cal);
        samlAssertion.setOneTimeUse(true);
        // assertion.setIssuer("<name des Verwenders (Applikationsname)>");
        samlAssertion.setIssuer(issuer);
        samlAssertion.setSubject(configuration.getLoggedInUser());
        // Gueltigkeit 2 Minuten 120
        samlAssertion.setTtl(120000);
        // Element assertion = factory.generateDom(samlAssertion, password);

        SAMLResponse samlResponse = new SAMLResponse();
        samlResponse.setIdPrefix("aid");
        // samlResponse.setIncludeSignerCert(true);
        samlResponse.setIssueInstant(cal);
        // samlResponse.setOneTimeUse(true);
        // assertion.setIssuer("<name des Verwenders (Applikationsname)>");
        samlResponse.setIssuer(issuer);
        samlResponse.setAssertion(samlAssertion);

        // Subject: Tatsaechlich angemeldeter Benutzer, über den Principal des JAAS Subjects erhältlich
        // samlResponse.setSubject(configuration.getLoggedInUser());
        // Gueltigkeit 2 Minuten 120
        // samlResponse.setTtl(120000);
        // final String vaultString = getVaultString(properties.getProperty(Property.SAML_PASSWORD.toString()));
        // final String password = SecurityVaultUtil.getValueAsString(vaultString);
        Element response = factory.generateDom(samlResponse, password);
        if (printResponse) {
            System.out.println(getSamlStringResponse(response));
            System.out.println("\nSamlResponse successfully created.");
        }
        return response;
    }

    static public String getSamlStringResponse(Element response) throws TransformerException {
        TransformerFactory transFactory = TransformerFactory.newInstance();
        transFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

        Transformer transformer = transFactory.newTransformer();
        StringWriter buffer = new StringWriter();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.transform(new DOMSource(response), new StreamResult(buffer));
        return buffer.toString();
    }
}
