package com.helvetia.app.quarkus.kafka;

import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.*;

/**
 * Wrapper for the different Channel-related types of request in Syrius.
 * Used as no common interface is available for
 * {@link RepliziereTelefonRequestType},
 * {@link RepliziereEmailRequestType},
 * {@link RepliziereWebRequestType},
 * {@link RepliziereDomiziladresseRequestType},
 * {@link RepliziereZusatzadresseRequestType}
 *
 * @param <E> Either one of the types above. Available constructors enforce that
 */
public class RepliziereChannelRequestWrapper<E> {
    private final E request;

    public static RepliziereChannelRequestWrapper<RepliziereTelefonRequestType> of(RepliziereTelefonRequestType repliziereTelefonRequestType) {
        return new RepliziereChannelRequestWrapper<>(repliziereTelefonRequestType);
    }

    public static RepliziereChannelRequestWrapper<RepliziereEmailRequestType> of(RepliziereEmailRequestType repliziereEmailRequestType) {
        return new RepliziereChannelRequestWrapper<>(repliziereEmailRequestType);
    }

    public static RepliziereChannelRequestWrapper<RepliziereWebRequestType> of(RepliziereWebRequestType repliziereWebRequestType) {
        return new RepliziereChannelRequestWrapper<>(repliziereWebRequestType);
    }

    public static RepliziereChannelRequestWrapper<RepliziereDomiziladresseRequestType> of(RepliziereDomiziladresseRequestType repliziereDomiziladresseRequestType) {
        return new RepliziereChannelRequestWrapper<>(repliziereDomiziladresseRequestType);
    }

    public static RepliziereChannelRequestWrapper<RepliziereZusatzadresseRequestType> of(RepliziereZusatzadresseRequestType repliziereZusatzadresseRequestType) {
        return new RepliziereChannelRequestWrapper<>(repliziereZusatzadresseRequestType);
    }

    public E getRequest() {
        return request;
    }

    private RepliziereChannelRequestWrapper(E request) {
        this.request = request;
    }

}
