package com.helvetia.app.quarkus.kafka.logging;

import changePartner.json.Response.Row;
import inboundTrafoZepasLegalPerson.json.Response.RowSchema.hasIdentificationNumber.hasIdentificationNumberSchema.IdentificationNumber.IdentificationNumberEntry;
import io.netty.handler.logging.LogLevel;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.MDC;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@Slf4j
@ApplicationScoped
public class LogHelper {

    @ConfigProperty(name = "ch.helvetia.kafka.log.app")
    String app;
    @ConfigProperty(name = "ch.helvetia.kafka.log.version")
    String version;

    public final static String ZEPASPARTNERKEY = "1";
    public final static String SYRIUSPARTNERKEY = "2";

    private void logGeneric(String trId, String message, LogLevel level) {
        try {
            MDC.put(LogConstants.KEY_APP, app);
            MDC.put(LogConstants.KEY_VERSION, version);
            MDC.put(LogConstants.KEY_USER_ID, "CH_MDM");
            if ((trId != null) && (!trId.isEmpty())) {
                message = ", trId=\"" + trId + "\", " + message;
            } else {
                message = ", " + message;
            }

            if (level == LogLevel.TRACE) {
                log.trace(message);
            } else if (level == LogLevel.DEBUG) {
                log.debug(message);
            } else if (level == LogLevel.INFO) {
                log.info(message);
            } else if (level == LogLevel.WARN) {
                log.warn(message);
            } else if (level == LogLevel.ERROR) {
                log.error(message);
            }
        } catch (IllegalThreadStateException e) {
            log.error("logInfo - " + e.getMessage());
        } finally {
            MDC.clear();
        }
    }

    public void logTrace(String trId, String message) {
        logGeneric(trId, message, LogLevel.TRACE);
    }

    public void logDebug(String trId, String message) {
        logGeneric(trId, message, LogLevel.DEBUG);
    }

    public void logInfo(String trId, String message) {
        logGeneric(trId, message, LogLevel.INFO);
    }

    public void logWarn(String trId, String message) {
        logGeneric(trId, message, LogLevel.WARN);
    }

    public void logError(String trId, String message) {
        logGeneric(trId, message, LogLevel.ERROR);
    }

    public void logError(String trId, Throwable exception) {
        logGeneric(trId, exception, LogLevel.ERROR);
    }


    private void logGeneric(String trId, Throwable exception, LogLevel level) {

        try {
            MDC.put(LogConstants.KEY_APP, app);
            MDC.put(LogConstants.KEY_VERSION, version);
            MDC.put(LogConstants.KEY_USER_ID, "CH_MDM");
            String preMessage;
            if ((trId != null) && (!trId.isEmpty())) {
                preMessage = ", trId=\"" + trId + "\", ";
            } else {
                preMessage = "Exception: ";
            }

            if (level == LogLevel.TRACE) {
                log.trace(preMessage, exception);
            } else if (level == LogLevel.DEBUG) {
                log.debug(preMessage, exception);
            } else if (level == LogLevel.INFO) {
                log.info(preMessage, exception);
            } else if (level == LogLevel.WARN) {
                log.warn(preMessage, exception);
            } else if (level == LogLevel.ERROR) {
                log.error(preMessage, exception);
            }
        } catch (IllegalThreadStateException e) {
            log.error("logInfo - " + e.getMessage());
        } finally {
            MDC.clear();
        }
    }

    public static String getTechMdmPartnerID(Row row) {

        String result = "unknown";

        try {

            if (row.getHasIdentificationNumber().size() > 0) {
                result = row.getHasIdentificationNumber().get(0).getTECHMDMPartnerID().toString();
            } else if (row.getTECHMDMPartnerID() != null) {
                result = row.getTECHMDMPartnerID().toString();
            }

        } catch (NullPointerException ex) {
            // just for private analytic reasons: in case of an exception: upper case
            result = "UNKNOWN";
        } catch (IndexOutOfBoundsException ex) {
            result = "UnKnown";
        } catch (Exception ex) {
            result = "UnKnowN";
        }

        return result;
    }

    // ZEPAS => partnerId: 1
    // Syrius => partnerId: 2
    public static String getPartnerId(Row row, String partnerKey) {

        String result = "unknown";

        try {

            if (row.getHasIdentificationNumber() != null) {

                List<IdentificationNumberEntry> identificationNumbers = row.getHasIdentificationNumber().get(0).getIdentificationNumber();
                if (identificationNumbers != null) {

                    int entry = 0;
                    while ((entry < identificationNumbers.size()) && (result.equals("unknown"))) {
                        if (identificationNumbers.get(entry).getIDType().toString().startsWith(partnerKey)) {
                            result = identificationNumbers.get(entry).getIDNumber().toString();
                        }
                        entry++;
                    }

                }

            }

            if (result.equals("unknown") && row.getHasChannelID().get(0).getChannelID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber() != null) {

                List<inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasIdentificationNumber.hasIdentificationNumberSchema.IdentificationNumber.IdentificationNumberEntry> identificationNumbers = row.getHasChannelID().get(0).getChannelID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber();
                if (identificationNumbers != null) {

                    int entry = 0;
                    while ((entry < identificationNumbers.size()) && (result.equals("unknown"))) {
                        if (identificationNumbers.get(entry).getIDType().toString().startsWith(partnerKey)) {
                            result = identificationNumbers.get(entry).getIDNumber().toString();
                        }
                        entry++;
                    }

                }

            }
        } catch (NullPointerException ex) {
            // just for private analytic reasons: in case of an exception: upper case
            result = "UNKNOWN";
        } catch (IndexOutOfBoundsException ex) {
            result = "UnKnown";
        } catch (Exception ex) {
            result = "UnKnowN";
        }

        return result;
    }

}
