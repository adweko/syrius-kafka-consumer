package com.helvetia.app.quarkus.kafka.streams;

@FunctionalInterface
public interface RelevantMessageFilter<M> {
    boolean isMessageRelevant(M message);
}
