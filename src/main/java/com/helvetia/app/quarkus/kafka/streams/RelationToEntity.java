package com.helvetia.app.quarkus.kafka.streams;

public interface RelationToEntity<M, E> {
    boolean canBeRelatedToEntity(M message);
    E extractEntity(M message);

}
