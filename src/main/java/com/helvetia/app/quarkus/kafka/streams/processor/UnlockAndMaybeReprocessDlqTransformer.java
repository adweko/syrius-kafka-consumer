package com.helvetia.app.quarkus.kafka.streams.processor;

import ch.mdm.syrius.dlq.DLQMessage;
import ch.mdm.syrius.dlq.DLQMessages;
import changePartner.com.helvetia.custom.changeMdmResponse;
import com.helvetia.app.quarkus.kafka.logging.LogHelper;
import com.helvetia.app.quarkus.kafka.streams.MapperAndSender;
import com.helvetia.app.quarkus.kafka.streams.TopologyProducer;
import com.helvetia.app.quarkus.kafka.streams.exception.stop.ReprocessingFailedException;
import com.helvetia.app.quarkus.kafka.streams.model.TechMDMPartnerID;
import com.helvetia.app.quarkus.kafka.streams.model.UnlockMessageValue;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.TimestampedKeyValueStore;
import org.apache.kafka.streams.state.ValueAndTimestamp;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.Duration;
import java.time.Instant;

@ApplicationScoped
public class UnlockAndMaybeReprocessDlqTransformer implements Transformer<String, changeMdmResponse, KeyValue<TechMDMPartnerID, DLQMessages>> {
    public static final Duration REPROCESS_FREQUENCY = Duration.ofSeconds(2);
    private ProcessorContext context;
    private TimestampedKeyValueStore<TechMDMPartnerID, DLQMessages> dlqstore;
    private TimestampedKeyValueStore<TechMDMPartnerID, UnlockMessageValue> republishedStore;

    @Inject
    MapperAndSender<changeMdmResponse, Boolean> mapperAndSender;

    @Inject
    LogHelper logHelper;


    @Override
    @SuppressWarnings("unchecked")
    public void init(ProcessorContext processorContext) {
        this.context = processorContext;
        dlqstore = (TimestampedKeyValueStore<TechMDMPartnerID, DLQMessages>) processorContext.getStateStore(TopologyProducer.DLQ_STORE_NAME);
        republishedStore = (TimestampedKeyValueStore<TechMDMPartnerID, UnlockMessageValue>) processorContext.getStateStore(TopologyProducer.REPUBLISHED_STORE_NAME);

        context.schedule(REPROCESS_FREQUENCY, PunctuationType.WALL_CLOCK_TIME, time -> maybeReprocessDlq());

    }


    private void maybeReprocessDlq() {
        try (KeyValueIterator<TechMDMPartnerID, ValueAndTimestamp<DLQMessages>> all = dlqstore.all()) {
            all.forEachRemaining(kv -> {
                TechMDMPartnerID partnerId = kv.key;
                DLQMessages dlqMessages = kv.value.value();
                ValueAndTimestamp<UnlockMessageValue> republished = republishedStore.get(partnerId);
                if (republished != null) {
                    UnlockMessageValue unlockMessageValue = republished.value();
                    // reprocess all dlqueued messages after startingTimeStamp
                    for (DLQMessage msg : dlqMessages.getMessages()) {
                        Instant msgTimstamp = Instant.ofEpochMilli(msg.getSourceTimestamp());
                        if (msgTimstamp.isAfter(unlockMessageValue.getRepublishedAt().getInstant())) {
                            logHelper.logInfo(null, "Reprocessing message from DLQ. Original message from Topic/Partition/Offset: " +
                                    context.topic() + " / " + context.partition() + " / " + context.offset());
                            final changeMdmResponse originalMessage = msg.getOriginalMessage();
                            try {
                                mapperAndSender.mapAndSend(originalMessage);
                            } catch (Exception exception) {
                                final String errorMessage = "Processing failed with exception. Please check the exception. If " +
                                        "it is of a technical, recoverable nature (like target system down) ensure to fix it in" +
                                        " environment, then republish again. If it is data quality, correct the source data in MDM" +
                                        " & republish again. The job will continue, but the partner will stay locked until the next republish.";
                                ReprocessingFailedException reprocessingFailedException = new ReprocessingFailedException(errorMessage, exception);
                                logHelper.logError(null, errorMessage);
                                logHelper.logError(null, reprocessingFailedException);
                                return;
                            }
                        }
                    }
                    // send delete to dlq after reprocessing
                    context.forward(partnerId, null);
                }
            });
        }

    }

    @Override
    public KeyValue<TechMDMPartnerID, DLQMessages> transform(String ignoredKey, changeMdmResponse ignoredValue) {
        // do not need it, only here for punctuator
       return null;
    }


    @Override
    public void close() {

    }

}
