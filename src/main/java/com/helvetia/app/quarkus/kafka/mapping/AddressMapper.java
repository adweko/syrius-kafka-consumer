package com.helvetia.app.quarkus.kafka.mapping;

import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsPostfachOhneNrCodeType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.*;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.*;
import com.helvetia.app.mdm.ReferenceDataMapper;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDEntry;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasLocationID.hasLocationIDSchema.LocationID.LocationIDEntry;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasLocationID.hasLocationIDSchema.LocationID.LocationIDSchema.hasAddressLocationID.hasAddressLocationIDSchema.AddressLocationID.AddressLocationIDSchema.hasAddressLocation.hasAddressLocationEntry;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasLocationID.hasLocationIDSchema.LocationID.LocationIDSchema.hasAddressLocationID.hasAddressLocationIDSchema.AddressLocationID.AddressLocationIDSchema.hasAddressLocation.hasAddressLocationSchema.AddressLocation.AddressLocationEntry;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasLocationID.hasLocationIDSchema.LocationID.LocationIDSchema.hasRegion.hasRegionSchema.Region.RegionEntry;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry;

import javax.enterprise.context.ApplicationScoped;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class AddressMapper {

    private static ReferenceDataMapper singleReferenceDataMapper;

    public ReferenceDataMapper getReferenceDataMapper() throws IOException {
        if (singleReferenceDataMapper == null) {
            singleReferenceDataMapper = new ReferenceDataMapper();
        }
        return singleReferenceDataMapper;
    }

    public AddressMapper() throws IOException {
    }

    public RepliziereDomiziladresseRequestType mapDomizilAdresse(hasChannelIDEntry channel) throws IOException {

        RepliziereDomiziladresseRequestType repliziereDomiziladresseRequestType = new RepliziereDomiziladresseRequestType();

        String sourceTimeStamp = channel.getBITEMPSourceTimestamp().toString();
        String techUser = Objects.toString(channel.getTECHUser(), null);

        WsPartnerReferenzType partnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType partnerIdType = new WsPartnerIdType();
        partnerIdType.setId(channel.getTECHMDMPartnerID().toString().toLowerCase());
        partnerReferenzType.setPartnerId(partnerIdType);
        repliziereDomiziladresseRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());
        repliziereDomiziladresseRequestType.setPartnerReferenz(partnerReferenzType);

        WsDomiziladresseStrukturType wsDomiziladresseStrukturType = new WsDomiziladresseStrukturType();

        String mdmTimeStamp = channel.getBITEMPMDMTimestamp().toString();
        long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4, 6)),
                Integer.parseInt(mdmTimeStamp.substring(6, 8)), Integer.parseInt(mdmTimeStamp.substring(8, 10)),
                Integer.parseInt(mdmTimeStamp.substring(10, 12)), Integer.parseInt(mdmTimeStamp.substring(12, 14)),
                Integer.parseInt(mdmTimeStamp.substring(14, 17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;

        // che0bmc, 20200721, use a global epoch to modify the timestamp
        // wsDomiziladresseStrukturType.setExterneVersion(epoch); //nanoseconds since epoch
        long mdmModifiedEpoch = epoch + 0300000000;
        wsDomiziladresseStrukturType.setExterneVersion(mdmModifiedEpoch); //nanoseconds since epoch

        WsAdresseType wsAdresseType = new WsAdresseType();
        hasAddressLocationEntry hasAddressLocationEntry =
                channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasAddressLocationID().get(0).getAddressLocationID().get(0).getHasAddressLocation().get(0);

        final XMLGregorianCalendar validFrom = DateHelper.syriusDateFromXsDateWithLimit(hasAddressLocationEntry.getBITEMPValidFrom());
        wsAdresseType.setStateFrom(validFrom);
        final XMLGregorianCalendar validTo = DateHelper.syriusDateFromXsDateWithLimit(hasAddressLocationEntry.getBITEMPValidTo());
        if (isDeleted(channel)) {
            wsAdresseType.setStateUpto(validFrom);
        } else {
            wsAdresseType.setStateUpto(validTo);
        }

        wsAdresseType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsAdresseType.setAenderungDurch(techUser);

        AddressLocationEntry addressLocationEntry =
                hasAddressLocationEntry.getAddressLocation().get(0);
        wsAdresseType.setPlz(Objects.toString(addressLocationEntry.getZIPCode(), null));
        wsAdresseType.setPlzZusatz(Objects.toString(addressLocationEntry.getZIPCodeExtension(), null));
        wsAdresseType.setOrt(Objects.toString(addressLocationEntry.getCity(), null));
        String country = getReferenceDataMapper().getNationalityMapMdmToSyr().get(String.valueOf(addressLocationEntry.getCountry()));
        wsAdresseType.setLand(country);
        wsAdresseType.setHausnummer(Objects.toString(addressLocationEntry.getHouseNumber(), null));

        wsAdresseType.setStrasse(Objects.toString(addressLocationEntry.getStreet(), null));

        RegionEntry regionEntry = channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasRegion().get(0).getRegion().get(0);

        wsAdresseType.setProvinz(getReferenceDataMapper().getRegionMapMdmToSyr().get(String.valueOf(regionEntry.getRegionName())));

        SiteEntry site = channel.getChannelID().get(0).getHasSite().get(0).getSite().get(0);
        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");
        wsAdresseType.setMutationsgrundId(wsMutationsgrundIdType);
        wsAdresseType.setPostfach(Objects.toString(addressLocationEntry.getPostbox(), null));
        wsAdresseType.setZusatz1VorStr(Objects.toString(site.getFlat(), null));
        wsAdresseType.setZusatz2VorStr(Objects.toString(site.getLocationDescription(), null));
        wsDomiziladresseStrukturType.getAdresseStates().add(wsAdresseType);
        //wsDomiziladresseStrukturType.setGueltAb(wsAdresseType.getStateFrom());
        wsDomiziladresseStrukturType.setGueltBis(wsAdresseType.getStateUpto());

        final String channelID = channel.getChannelID().get(0).getChannelID().toString().toLowerCase();

        //Adressrolle
        WsAdressrolleStrukturType wsAdressrolleStrukturType = new WsAdressrolleStrukturType();
        //wsAdressrolleStrukturType.setGueltAb(wsAdresseType.getStateFrom());
        wsAdressrolleStrukturType.setGueltBis(wsAdresseType.getStateUpto());
        WsAdressrolleDefIdType wsAdressrolleDefIdType = new WsAdressrolleDefIdType();
        wsAdressrolleDefIdType.setId("54c1de18-eeb6-491d-b818-8e3e655faac3"); //Boid Adressrolle for Wohn/Geschaeft-Adresse
        wsAdressrolleStrukturType.setAdressrolleDefId(wsAdressrolleDefIdType);
        //wsAdressrolleStrukturType.setExterneReferenz();
        WsAdressrolleIdType wsAdressrolleIdType = new WsAdressrolleIdType();
        wsAdressrolleIdType.setId(channelID);
        wsAdressrolleStrukturType.setId(wsAdressrolleIdType);
        WsAdressrolleType wsAdressrolleType = new WsAdressrolleType();
        wsAdressrolleType.setMutationsgrundId(wsMutationsgrundIdType);
        wsAdressrolleType.setStateFrom(wsAdresseType.getStateFrom());
        wsAdressrolleType.setStateUpto(wsAdresseType.getStateUpto());

        wsAdressrolleType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsAdressrolleType.setAenderungDurch(techUser);


        wsAdressrolleStrukturType.getAdressrolleStates().add(wsAdressrolleType);
        wsDomiziladresseStrukturType.getAdressrollen().add(wsAdressrolleStrukturType);
        repliziereDomiziladresseRequestType.setDomiziladresseStruktur(wsDomiziladresseStrukturType);


        WsAdresseReferenzType wsAdresseReferenzType = new WsAdresseReferenzType();
        WsAdresseIdType wsAdresseIdType = new WsAdresseIdType();

        wsAdresseIdType.setId(channelID);
        wsAdresseReferenzType.setAdresseId(wsAdresseIdType);
        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
        wsExterneReferenzType.setId(channel.getChannelID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber().get(0).getIDNumber().toString());
        wsAdresseReferenzType.setExterneReferenz(wsExterneReferenzType);
        repliziereDomiziladresseRequestType.setAdresseReferenz(wsAdresseReferenzType);

        return repliziereDomiziladresseRequestType;
    }


    public RepliziereZusatzadresseRequestType mapZusatzAdresse(hasChannelIDEntry channel) throws IOException {
        RepliziereZusatzadresseRequestType repliziereZusatzadresseRequestType = new RepliziereZusatzadresseRequestType();

        String sourceTimeStamp = channel.getBITEMPSourceTimestamp().toString();
        String techUser = Objects.toString(channel.getTECHUser(), null);

        WsPartnerReferenzType partnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType partnerIdType = new WsPartnerIdType();
        partnerIdType.setId(channel.getTECHMDMPartnerID().toString().toLowerCase());
        partnerReferenzType.setPartnerId(partnerIdType);
        repliziereZusatzadresseRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());
        repliziereZusatzadresseRequestType.setPartnerReferenz(partnerReferenzType);

        WsZusatzadresseStrukturType wsZusatzadresseStrukturType = new WsZusatzadresseStrukturType();


        String mdmTimeStamp = channel.getBITEMPMDMTimestamp().toString();
        long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4, 6)),
                Integer.parseInt(mdmTimeStamp.substring(6, 8)), Integer.parseInt(mdmTimeStamp.substring(8, 10)),
                Integer.parseInt(mdmTimeStamp.substring(10, 12)), Integer.parseInt(mdmTimeStamp.substring(12, 14)),
                Integer.parseInt(mdmTimeStamp.substring(14, 17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;

        // che0bmc, 20200721, use a global epoch to modify the timestamp
        // wsZusatzadresseStrukturType.setExterneVersion(epoch); //nanoseconds since epoch
        long mdmModifiedEpoch = epoch + 0600000000;
        wsZusatzadresseStrukturType.setExterneVersion(mdmModifiedEpoch); //nanoseconds since epoch

        WsAdresseType wsAdresseType = new WsAdresseType();

        wsAdresseType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsAdresseType.setAenderungDurch(techUser);

        hasAddressLocationEntry hasAddressLocationEntry =
                channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasAddressLocationID().get(0).getAddressLocationID().get(0).getHasAddressLocation().get(0);

        final XMLGregorianCalendar validFrom = DateHelper.syriusDateFromXsDateWithLimit(hasAddressLocationEntry.getBITEMPValidFrom());
        wsAdresseType.setStateFrom(validFrom);
        final XMLGregorianCalendar validTo = DateHelper.syriusDateFromXsDateWithLimit(hasAddressLocationEntry.getBITEMPValidTo());
        if (isDeleted(channel)) {
            wsAdresseType.setStateUpto(validFrom);
        } else {
            wsAdresseType.setStateUpto(validTo);
        }

        AddressLocationEntry addressLocationEntry =
                hasAddressLocationEntry.getAddressLocation().get(0);
        wsAdresseType.setPlz(Objects.toString(addressLocationEntry.getZIPCode(), null));
        wsAdresseType.setPlzZusatz(Objects.toString(addressLocationEntry.getZIPCodeExtension(), null));
        wsAdresseType.setOrt(Objects.toString(addressLocationEntry.getCity(), null));
        String country = getReferenceDataMapper().getNationalityMapMdmToSyr().get(String.valueOf(addressLocationEntry.getCountry()));
        wsAdresseType.setLand(country);
        wsAdresseType.setHausnummer(Objects.toString(addressLocationEntry.getHouseNumber(), null));

        wsAdresseType.setPostfach(Objects.toString(addressLocationEntry.getPostbox(), null));
        // che0bmc: Postfachadresse und keine PostfachNr dann PostfachOhneNr setzen
        if (((addressLocationEntry.getPostbox() == null) || addressLocationEntry.getPostbox().toString().equals("null") || addressLocationEntry.getPostbox().toString().isEmpty()) && (channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getUsage().toString()).equals("003")) {
            WsPostfachOhneNrCodeType wsPostfachOhneNrCodeType = new WsPostfachOhneNrCodeType();
            wsPostfachOhneNrCodeType.setCodeId("-10110");
            wsAdresseType.setPostfachOhneNr(wsPostfachOhneNrCodeType);
        }
        wsAdresseType.setStrasse(Objects.toString(addressLocationEntry.getStreet(), null));

        RegionEntry regionEntry = channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasRegion().get(0).getRegion().get(0);

        wsAdresseType.setProvinz(getReferenceDataMapper().getRegionMapMdmToSyr().get(String.valueOf(regionEntry.getRegionName())));

        SiteEntry site = channel.getChannelID().get(0).getHasSite().get(0).getSite().get(0);
        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");
        wsAdresseType.setMutationsgrundId(wsMutationsgrundIdType);
        wsAdresseType.setPostfach(Objects.toString(addressLocationEntry.getPostbox(), null));
        // che0bmc: Postfachadresse und keine PostfachNr dann PostfachOhneNr setzen
        if (((addressLocationEntry.getPostbox() == null) || addressLocationEntry.getPostbox().toString().equals("null") || addressLocationEntry.getPostbox().toString().isEmpty()) && (channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getUsage().toString()).equals("003")) {
            WsPostfachOhneNrCodeType wsPostfachOhneNrCodeType = new WsPostfachOhneNrCodeType();
            wsPostfachOhneNrCodeType.setCodeId("-10110");
            wsAdresseType.setPostfachOhneNr(wsPostfachOhneNrCodeType);
        }
        wsAdresseType.setZusatz1VorStr(Objects.toString(site.getFlat(), null));
        wsAdresseType.setZusatz2VorStr(Objects.toString(site.getLocationDescription(), null));
        wsZusatzadresseStrukturType.getAdresseStates().add(wsAdresseType);
        //wsZusatzadresseStrukturType.setGueltAb(wsAdresseType.getStateFrom());
        wsZusatzadresseStrukturType.setGueltBis(wsAdresseType.getStateUpto());

        //Adressrolle
        LocationIDEntry locationIDEntry =
                channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0);
        WsAdressrolleStrukturType wsAdressrolleStrukturType = new WsAdressrolleStrukturType();
        //wsAdressrolleStrukturType.setGueltAb(wsAdresseType.getStateFrom());
        wsAdressrolleStrukturType.setGueltBis(wsAdresseType.getStateUpto());
        WsAdressrolleDefIdType wsAdressrolleDefIdType = new WsAdressrolleDefIdType();
        if ((locationIDEntry.getUsage().toString()).equals("002")) { // Zweitsitzadresse
            wsAdressrolleDefIdType.setId("c584981c-4354-4650-8a97-817ab16a97b0"); //Boid Adressrolle
        } else if ((locationIDEntry.getUsage().toString()).equals("003")) { // Postfachadresse
            wsAdressrolleDefIdType.setId("72b90033-1c03-4c6b-99c5-89d5a2047cef"); //Boid Adressrolle
        }
        wsAdressrolleStrukturType.setAdressrolleDefId(wsAdressrolleDefIdType);

        final String channelID = channel.getChannelID().get(0).getChannelID().toString().toLowerCase();

        WsAdressrolleIdType wsAdressrolleIdType = new WsAdressrolleIdType();
        wsAdressrolleIdType.setId(channelID);
        wsAdressrolleStrukturType.setId(wsAdressrolleIdType);
        WsAdressrolleType wsAdressrolleType = new WsAdressrolleType();
        wsAdressrolleType.setMutationsgrundId(wsMutationsgrundIdType);
        wsAdressrolleType.setStateFrom(wsAdresseType.getStateFrom());
        wsAdressrolleType.setStateUpto(wsAdresseType.getStateUpto());

        wsAdressrolleType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsAdressrolleType.setAenderungDurch(techUser);
        wsAdressrolleStrukturType.getAdressrolleStates().add(wsAdressrolleType);
        wsZusatzadresseStrukturType.getAdressrollen().add(wsAdressrolleStrukturType);
        repliziereZusatzadresseRequestType.setZusatzadresseStruktur(wsZusatzadresseStrukturType);

        WsAdresseReferenzType wsAdresseReferenzType = new WsAdresseReferenzType();
        WsAdresseIdType wsAdresseIdType = new WsAdresseIdType();

        wsAdresseIdType.setId(channelID);
        wsAdresseReferenzType.setAdresseId(wsAdresseIdType);
        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
        wsExterneReferenzType.setId(channel.getChannelID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber().get(0).getIDNumber().toString());
        wsAdresseReferenzType.setExterneReferenz(wsExterneReferenzType);
        repliziereZusatzadresseRequestType.setAdresseReferenz(wsAdresseReferenzType);

        return repliziereZusatzadresseRequestType;
    }

    private boolean isDeleted(hasChannelIDEntry channel) {
        final Optional<CharSequence> biTempDeleted = Optional.ofNullable(channel.getBITEMPDeleted());
        return biTempDeleted.isPresent() && biTempDeleted.get().toString().equals("X");
    }
}
