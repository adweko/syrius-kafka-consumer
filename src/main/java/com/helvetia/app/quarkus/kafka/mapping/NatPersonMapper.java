package com.helvetia.app.quarkus.kafka.mapping;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.adcubum.syrius.api.common.codes.v1.WsCodeType;
import com.adcubum.syrius.api.common.types.v1.WsCustomAttributeCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.*;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.*;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.*;
import com.helvetia.app.mdm.ReferenceDataMapper;
import inboundTrafoZepasNaturalPerson.json.Response.RowSchema.isNaturalPerson.isNaturalPersonSchema.NaturalPerson.NaturalPersonEntry;
import inboundTrafoZepasUnderwriting.json.Response.RowSchema.hasUnderwritingCodeID.hasUnderwritingCodeIDEntry;

import javax.enterprise.context.ApplicationScoped;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationScoped
public class NatPersonMapper {

    private static ReferenceDataMapper singleReferenceDataMapper;

    public ReferenceDataMapper getReferenceDataMapper() {
        try {
            if (singleReferenceDataMapper == null) {
                singleReferenceDataMapper = new ReferenceDataMapper();
            }
            return singleReferenceDataMapper;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public NatPersonMapper() {
    }

    public RepliziereNatPersonRequestType mapNatPerson(changeMdmResponse changeMdmResponse, long mdmModifiedEpoch) {

        RepliziereNatPersonRequestType natPersonRequestType = new RepliziereNatPersonRequestType();
        natPersonRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());

        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
        wsPartnerIdType.setId(changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getTECHMDMPartnerID().toString().toLowerCase());
        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
        // che0bmc, 20200722, entferne externeReferenz
        // WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
        // wsExterneReferenzType.setId(changeMdmResponse.getBusinessPartnerID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber().get(0).getIDNumber().toString());
        // wsPartnerReferenzType.setExterneReferenz(wsExterneReferenzType);

        WsNatPersonStrukturType wsNatPersonStrukturType = new WsNatPersonStrukturType();
        WsPartneridentifikatorType wsPartneridentifikatorType = new WsPartneridentifikatorType();
        WsPartneridentifikatorDefIdType wsPartneridentifikatorDefIdType = new WsPartneridentifikatorDefIdType();
        wsPartneridentifikatorDefIdType.setId("SYR_Partner_PartnerNr");
        wsPartneridentifikatorType.setPartneridentifikatorDefId(wsPartneridentifikatorDefIdType);
        wsPartneridentifikatorType.setWert(changeMdmResponse.getBusinessPartnerID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber().get(0).getIDNumber().toString());
        wsNatPersonStrukturType.getPartneridentifikatoren().add(wsPartneridentifikatorType);

        NaturalPersonEntry naturalPersonEntry = changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getNaturalPerson().get(0);


        if (naturalPersonEntry.getSocialSecurityNumber() != null) {
            WsPartneridentifikatorType wsPartneridentifikatorType2 = new WsPartneridentifikatorType();
            WsPartneridentifikatorDefIdType wsPartneridentifikatorDefIdType2 = new WsPartneridentifikatorDefIdType();
            wsPartneridentifikatorDefIdType2.setId("SYR_Partner_SozialVersNr");
            wsPartneridentifikatorType2.setPartneridentifikatorDefId(wsPartneridentifikatorDefIdType2);
            wsPartneridentifikatorType2.setWert(naturalPersonEntry.getSocialSecurityNumber().toString());
            wsNatPersonStrukturType.getPartneridentifikatoren().add(wsPartneridentifikatorType2);
        }

        WsNatPersonType wsNatPersonType = new WsNatPersonType();

        WsAufenthaltsbewCodeType wsAufenthaltsbewCodeType = new WsAufenthaltsbewCodeType();
        String residencePermit = getReferenceDataMapper().getResidencePermitMapMdmToSyr().get(String.valueOf(changeMdmResponse.getBusinessPartnerID().get(0)
                .getIsNaturalPerson().get(0).getNaturalPerson().get(0).getResidencePermit()));
        wsAufenthaltsbewCodeType.setCodeId(residencePermit);
        wsNatPersonType.setAufenthaltsbew(wsAufenthaltsbewCodeType);
        wsNatPersonType.setMitarbeiterBenutzerId(Objects.toString(naturalPersonEntry.getEmployeeUser(), null));


//        WsAuslaenderstatusIdType wsAuslaenderstatusIdType = new WsAuslaenderstatusIdType();
//        wsAuslaenderstatusIdType.setId(context.read("$.BusinessPartnerID[0].isNaturalPerson[0].NaturalPerson[0].alienStatus"));
//        wsNatPersonType.setAuslaenderstatus(wsAuslaenderstatusIdType);

        WsBerufsgruppeCodeType wsBerufsgruppeCodeType = new WsBerufsgruppeCodeType();
        String employmentStatus = getReferenceDataMapper().getEmploymentStatusMapMdmToSyr().get(String.valueOf(naturalPersonEntry.getEmploymentStatus()));
        wsBerufsgruppeCodeType.setCodeId(employmentStatus);
        wsNatPersonType.setBerufsgruppe(wsBerufsgruppeCodeType);

        String sourceTimeStamp = changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getBITEMPSourceTimestamp().toString();
        wsNatPersonType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsNatPersonType.setAenderungDurch(
                Objects.toString(
                        changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getTECHUser(), null)
        );


        if (naturalPersonEntry.getDateOfBirth() != null) {
            final XMLGregorianCalendar dateOfBirth = DateHelper.syriusDateFromXsDateNoLimit(naturalPersonEntry.getDateOfBirth());
            wsNatPersonType.setGeburtsdatum(dateOfBirth);
        }

        WsGeschlechtCodeType wsGeschlechtCodeType = new WsGeschlechtCodeType();
        wsGeschlechtCodeType.setCodeId(getReferenceDataMapper().getSexMapMdmToSyr().get(String.valueOf(naturalPersonEntry.getSex())));
        wsNatPersonType.setGeschlecht(wsGeschlechtCodeType);
        wsNatPersonType.setNameZusatz(Objects.toString(naturalPersonEntry.getOtherTitle(), null));

        wsNatPersonType.setHeimatort(Objects.toString(naturalPersonEntry.getHomeTown(), null));
        wsNatPersonType.setLedigname(Objects.toString(naturalPersonEntry.getNameAtBirth(), null));
        wsNatPersonType.setNationalitaet(getReferenceDataMapper().getNationalityMapMdmToSyr().get(String.valueOf(naturalPersonEntry.getNationality())));
        WsTitelCodeType wsTitelCodeType = new WsTitelCodeType();

        if (naturalPersonEntry.getSalutation().equals("07")) { //Anrede Schwester //TODO: NPE
            wsTitelCodeType.setCodeId("9f2d1f1e-b3cb-47f0-8d8f-7c6df9d581a2"); // Nonne
        } else {
            wsTitelCodeType.setCodeId(getReferenceDataMapper().getTitleMapMdmToSyr().get(String.valueOf(naturalPersonEntry.getAcademicDegree())));
        }
        wsNatPersonType.setTitel(wsTitelCodeType);

        WsInAusbildungCodeType wsInAusbildungCodeType = new WsInAusbildungCodeType();
        wsInAusbildungCodeType.setCodeId("-10111"); //default: no
        wsNatPersonType.setInAusbildung(wsInAusbildungCodeType);

        WsArbeitslosCodeType wsArbeitslosCodeType = new WsArbeitslosCodeType();
        wsArbeitslosCodeType.setCodeId("-10111"); //default no
        wsNatPersonType.setArbeitslos(wsArbeitslosCodeType);

        if (naturalPersonEntry.getDateOfDeath() != null) {
            final XMLGregorianCalendar dateOfDeath = DateHelper.syriusDateFromXsDateNoLimit(naturalPersonEntry.getDateOfDeath());
            wsNatPersonType.setTodesdatum(dateOfDeath);
        }
        wsNatPersonType.setVorname(naturalPersonEntry.getName().toString());
        if (naturalPersonEntry.getSecondName() != null || naturalPersonEntry.getThirdName() != null) {
            wsNatPersonType.setWeitereVornamen(naturalPersonEntry.getSecondName().toString() + " " + naturalPersonEntry.getThirdName().toString());
        }
        wsNatPersonType.setName(naturalPersonEntry.getSurname().toString());
        WsZivilstandCodeType wsZivilstandCodeType = new WsZivilstandCodeType();
        String maritalStatus = getReferenceDataMapper().getMaritalStatusMapMdmToSyr().get(String.valueOf(naturalPersonEntry.getMaritalStatus()));
        wsZivilstandCodeType.setCodeId(maritalStatus);
        wsNatPersonType.setZivilstand(wsZivilstandCodeType);

        String syriusKorrespondenzsprache = getReferenceDataMapper().getLanguageMapMdmToSyr().get(String.valueOf(naturalPersonEntry.getLanguage()));
        wsNatPersonType.setKorrespondenzsprache(syriusKorrespondenzsprache);
        wsNatPersonType.setMitarbeiterBenutzerId(Objects.toString(naturalPersonEntry.getEmployeeUser(), null));

        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a"); //specific syrius-boid
        wsNatPersonType.setMutationsgrundId(wsMutationsgrundIdType);

        final XMLGregorianCalendar validFrom = DateHelper.syriusDateFromXsDateWithLimit(changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getBITEMPValidFrom());
        wsNatPersonType.setStateFrom(validFrom);

        final XMLGregorianCalendar validTo = DateHelper.syriusDateFromXsDateWithLimit(changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getBITEMPValidTo());
        wsNatPersonType.setStateUpto(validTo);

        Optional<CharSequence> encryptionCodeVIP = Optional.ofNullable(changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getBusinessPartner().get(0).getEncryptionCodeVIP());
        WsPartnerschutzDefIdType wsPartnerschutzDefIdType = new WsPartnerschutzDefIdType();
        //wsPartnerschutzDefIdType.setId("-1"); // default if not encrypted
        encryptionCodeVIP.ifPresent(code -> {
            if (code.toString().equals("X")) {
                wsPartnerschutzDefIdType.setId("bc56efd9-beb7-44cb-beb4-1a30968a48f9");
            }
        });
        wsNatPersonType.setPartnerschutzDefId(wsPartnerschutzDefIdType);
        String mdmTimeStamp = changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0)
                .getBITEMPMDMTimestamp().toString();
        long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4, 6)),
                Integer.parseInt(mdmTimeStamp.substring(6, 8)), Integer.parseInt(mdmTimeStamp.substring(8, 10)),
                Integer.parseInt(mdmTimeStamp.substring(10, 12)), Integer.parseInt(mdmTimeStamp.substring(12, 14)),
                Integer.parseInt(mdmTimeStamp.substring(14, 17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;
        //long epoch2 = LocalDateTime.now().atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L; //nanoseconds since epoch

        // che0bmc, 20200721, use a global epoch to modify the timestamp
        // wsNatPersonStrukturType.setExterneVersion(epoch); //needed for processing order in syrius
        wsNatPersonStrukturType.setExterneVersion(mdmModifiedEpoch); //needed for processing order in syrius
        wsNatPersonStrukturType.getNatPersonStates().add(wsNatPersonType);
        //wsNatPersonStrukturType.setGueltAb(wsNatPersonType.getStateFrom());
        wsNatPersonStrukturType.setGueltBis(wsNatPersonType.getStateUpto());
        WsErlaubnisCodeType wsErlaubnisCodeType = new WsErlaubnisCodeType();

        WsKommVerbVerwendungType wsKommVerbVerwendungType = new WsKommVerbVerwendungType();
        WsKommVerbTypCodeType wsKommVerbTypCodeType = new WsKommVerbTypCodeType();
        wsKommVerbVerwendungType.setVerbindungstyp(wsKommVerbTypCodeType);

        if (naturalPersonEntry.getNoAds() != null) {
            wsErlaubnisCodeType.setCodeId("-64830"); //keine Werbung
        } else {
            wsErlaubnisCodeType.setCodeId("-64831"); //werbung
        }

        wsKommVerbVerwendungType.setErlaubnis(wsErlaubnisCodeType);
        WsErlaubnisartCodeType wsErlaubnisartCodeType = new WsErlaubnisartCodeType();
        wsErlaubnisartCodeType.setCodeId("-64868"); //Anweisung Kunde
        wsKommVerbVerwendungType.setErlaubnisart(wsErlaubnisartCodeType);
        wsNatPersonStrukturType.getKommVerbVerwendungen().add(wsKommVerbVerwendungType);

        natPersonRequestType.setPartnerReferenz(wsPartnerReferenzType);
        natPersonRequestType.setNatPersonStruktur(wsNatPersonStrukturType);
        return natPersonRequestType;
    }


    public ReplizierePartnerrolleRequestType mapDiscountCategoryToPersonenRolleRabattklasse(changeMdmResponse changeMdmResponse, long mdmModifiedEpoch) {
        NaturalPersonEntry naturalPersonEntry = changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getNaturalPerson().get(0);
        ReplizierePartnerrolleRequestType partnerrolleRequestType = new ReplizierePartnerrolleRequestType();

        String sourceTimeStamp = changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getBITEMPSourceTimestamp().toString();


        partnerrolleRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());
        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();

        wsPartnerIdType.setId(changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getTECHMDMPartnerID().toString().toLowerCase());
        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);

        partnerrolleRequestType.setPartnerReferenz(wsPartnerReferenzType);

        WsPartnerrolleReferenzType wsPartnerrolleReferenzType = new WsPartnerrolleReferenzType();

        String role = "88afdfe0-f186-4eb6-9f4e-669ba04fb60b"; // for all discount categories

        WsPartnerrolleIdType wsPartnerrolleIdType = new WsPartnerrolleIdType();
        WsPartnerrolleDefIdType wsPartnerrolleDefIdType = new WsPartnerrolleDefIdType();

        // all discountCategories now share a single role, db606637-55d6-45dc-a3a0-74e873738dec
        // the specific discount category goes into a custom attribute
        wsPartnerrolleDefIdType.setId(role);

        WsPartnerrolleType wsPartnerrolleType = new WsPartnerrolleType();
        String customAttribute = "e87c5df6-cae1-467a-8dfb-6877c688ed48"; // code for "keine Rabattklasse" (no discountcategory)

        if (naturalPersonEntry.getDiscountCategory() != null &&
                !naturalPersonEntry.getDiscountCategory().toString().isEmpty() &&
                !naturalPersonEntry.getDiscountCategory().toString().isBlank()) {
            customAttribute = getReferenceDataMapper().getDiscountCategoryMapMdmToSyr().get(String.valueOf(naturalPersonEntry.getDiscountCategory()));
        }

        final WsCodeType wsCodeType = new WsCodeType();
        wsCodeType.setCodeId(customAttribute);
        final WsCustomAttributeCodeType wsCustomAttributeCodeType = new WsCustomAttributeCodeType();
        wsCustomAttributeCodeType.setAttrValue(wsCodeType);
        wsCustomAttributeCodeType.setAttrId("ca4254b0-7d3c-4f69-a7c9-c5b32dd1d065");

        wsPartnerrolleType.getCustomAttribute().add(wsCustomAttributeCodeType);

//        wsPartnerrolleIdType.setId(UUID.randomUUID().toString().toLowerCase());
        wsPartnerrolleIdType.setId(wsCustomAttributeCodeType.getAttrId());

        // che0bmc, 20200720: keine partnerrolleId in partnerrolleReferenz
        // wsPartnerrolleReferenzType.setPartnerrolleId(wsPartnerrolleIdType);
        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
        wsExterneReferenzType.setId(role);
        wsPartnerrolleReferenzType.setExterneReferenz(wsExterneReferenzType);
        partnerrolleRequestType.setPartnerrolleReferenz(wsPartnerrolleReferenzType);

        WsPartnerrolleStrukturType wsPartnerrolleStrukturType = new WsPartnerrolleStrukturType();

        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");
        wsPartnerrolleType.setMutationsgrundId(wsMutationsgrundIdType);


        wsPartnerrolleType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsPartnerrolleType.setAenderungDurch(
                Objects.toString(
                        changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getTECHUser(), null)
        );

        final XMLGregorianCalendar validFrom = DateHelper.syriusDateFromXsDateWithLimit(changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getBITEMPValidFrom());
        wsPartnerrolleType.setStateFrom(validFrom);

        final XMLGregorianCalendar validTo = DateHelper.syriusDateFromXsDateWithLimit(changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getBITEMPValidTo());
        wsPartnerrolleType.setStateUpto(validTo);

        String mdmTimeStamp = changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0)
                .getBITEMPMDMTimestamp().toString();
        long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4, 6)),
                Integer.parseInt(mdmTimeStamp.substring(6, 8)), Integer.parseInt(mdmTimeStamp.substring(8, 10)),
                Integer.parseInt(mdmTimeStamp.substring(10, 12)), Integer.parseInt(mdmTimeStamp.substring(12, 14)),
                Integer.parseInt(mdmTimeStamp.substring(14, 17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;

        // che0bmc, 20200721, use a global epoch to modify the timestamp
        // wsPartnerrolleStrukturType.setExterneVersion(epoch); //needed for processing order in syrius
        wsPartnerrolleStrukturType.setExterneVersion(mdmModifiedEpoch); //needed for processing order in syrius
        wsPartnerrolleStrukturType.getPartnerrolleStates().add(wsPartnerrolleType);
        //wsPartnerrolleStrukturType.setGueltAb(wsPartnerrolleType.getStateFrom());
        wsPartnerrolleStrukturType.setGueltBis(wsPartnerrolleType.getStateUpto());
        wsPartnerrolleStrukturType.setPartnerrolleDefId(wsPartnerrolleDefIdType);
        partnerrolleRequestType.setPartnerrolleStruktur(wsPartnerrolleStrukturType);
        return partnerrolleRequestType;
    }

    public ReplizierePartnerrolleRequestType mapEmployeeToPersonenRolleMitarbeiter(changeMdmResponse changeMdmResponse, long mdmModifiedEpoch) {
        NaturalPersonEntry naturalPersonEntry = changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getNaturalPerson().get(0);

        ReplizierePartnerrolleRequestType partnerrolleRequestType = new ReplizierePartnerrolleRequestType();

        String sourceTimeStamp = changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getBITEMPSourceTimestamp().toString();

        partnerrolleRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());
        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();

        wsPartnerIdType.setId(changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getTECHMDMPartnerID().toString().toLowerCase());
        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);

        partnerrolleRequestType.setPartnerReferenz(wsPartnerReferenzType);

        String role = "6cfb5bb7-2932-4705-a97b-208a0cb70477"; // technische Mitarbeiter Rolle

        WsPartnerrolleReferenzType wsPartnerrolleReferenzType = new WsPartnerrolleReferenzType();
        WsPartnerrolleIdType wsPartnerrolleIdType = new WsPartnerrolleIdType();
        WsPartnerrolleDefIdType wsPartnerrolleDefIdType = new WsPartnerrolleDefIdType();

        wsPartnerrolleDefIdType.setId(role);

        WsPartnerrolleType wsPartnerrolleType = new WsPartnerrolleType();
        String customAttribute;
        if (naturalPersonEntry.getEmployee() != null && naturalPersonEntry.getEmployee().toString().equals("X")) {
            customAttribute = "-10110"; // code for "yes, I am employee"
        } else {
            customAttribute = "-10111"; // code for "no, I am not an employee"
        }


        final WsCodeType wsCodeType = new WsCodeType();
        wsCodeType.setCodeId(customAttribute);
        final WsCustomAttributeCodeType wsCustomAttributeCodeType = new WsCustomAttributeCodeType();
        wsCustomAttributeCodeType.setAttrValue(wsCodeType);
        wsCustomAttributeCodeType.setAttrId("bb52ece7-d0d5-4558-b5a6-b52744129ed4"); // Mitarbeiter ParamAttr ID

        wsPartnerrolleType.getCustomAttribute().add(wsCustomAttributeCodeType);

//        wsPartnerrolleIdType.setId(UUID.randomUUID().toString().toLowerCase());
        wsPartnerrolleIdType.setId(wsCustomAttributeCodeType.getAttrId());

        // che0bmc, 20200720: keine partnerrolleId in partnerrolleReferenz
        // wsPartnerrolleReferenzType.setPartnerrolleId(wsPartnerrolleIdType);
        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
        wsExterneReferenzType.setId(role);
        wsPartnerrolleReferenzType.setExterneReferenz(wsExterneReferenzType);
        partnerrolleRequestType.setPartnerrolleReferenz(wsPartnerrolleReferenzType);

        WsPartnerrolleStrukturType wsPartnerrolleStrukturType = new WsPartnerrolleStrukturType();

        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");
        wsPartnerrolleType.setMutationsgrundId(wsMutationsgrundIdType);

        wsPartnerrolleType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsPartnerrolleType.setAenderungDurch(
                Objects.toString(
                        changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getTECHUser(), null)
        );

        final XMLGregorianCalendar validFrom = DateHelper.syriusDateFromXsDateWithLimit(changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getBITEMPValidFrom());
        wsPartnerrolleType.setStateFrom(validFrom);

        final XMLGregorianCalendar validTo = DateHelper.syriusDateFromXsDateWithLimit(changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getBITEMPValidTo());
        wsPartnerrolleType.setStateUpto(validTo);

        String mdmTimeStamp = changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0)
                .getBITEMPMDMTimestamp().toString();
        long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4, 6)),
                Integer.parseInt(mdmTimeStamp.substring(6, 8)), Integer.parseInt(mdmTimeStamp.substring(8, 10)),
                Integer.parseInt(mdmTimeStamp.substring(10, 12)), Integer.parseInt(mdmTimeStamp.substring(12, 14)),
                Integer.parseInt(mdmTimeStamp.substring(14, 17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;

        // che0bmc, 20200721, use a global epoch to modify the timestamp
        // wsPartnerrolleStrukturType.setExterneVersion(epoch); //needed for processing order in syrius
        wsPartnerrolleStrukturType.setExterneVersion(mdmModifiedEpoch); //needed for processing order in syrius

        wsPartnerrolleStrukturType.getPartnerrolleStates().add(wsPartnerrolleType);
        //wsPartnerrolleStrukturType.setGueltAb(wsPartnerrolleType.getStateFrom());
        wsPartnerrolleStrukturType.setGueltBis(wsPartnerrolleType.getStateUpto());
        wsPartnerrolleStrukturType.setPartnerrolleDefId(wsPartnerrolleDefIdType);
        partnerrolleRequestType.setPartnerrolleStruktur(wsPartnerrolleStrukturType);
        return partnerrolleRequestType;
    }

    public Optional<ReplizierePartnerrolleRequestType> mapResidencePermitToPersonenRolleDiplomat(changeMdmResponse changeMdmResponse, long mdmModifiedEpoch) {
        NaturalPersonEntry naturalPersonEntry = changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getNaturalPerson().get(0);
        // if not diplomat, return empty
        if (naturalPersonEntry.getResidencePermit() == null || !naturalPersonEntry.getResidencePermit().toString().equals("D")) {
            return Optional.empty();
        }
        // else map diplomat

        ReplizierePartnerrolleRequestType partnerrolleRequestType = new ReplizierePartnerrolleRequestType();

        String sourceTimeStamp = changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getBITEMPSourceTimestamp().toString();

        partnerrolleRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());
        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();

        wsPartnerIdType.setId(changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getTECHMDMPartnerID().toString().toLowerCase());
        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);

        partnerrolleRequestType.setPartnerReferenz(wsPartnerReferenzType);

        WsPartnerrolleReferenzType wsPartnerrolleReferenzType = new WsPartnerrolleReferenzType();

        String role = "7b136f16-c446-464d-bd43-6aab3d77884c"; //boid diplomat

        WsPartnerrolleIdType wsPartnerrolleIdType = new WsPartnerrolleIdType();
        WsPartnerrolleDefIdType wsPartnerrolleDefIdType = new WsPartnerrolleDefIdType();

        wsPartnerrolleDefIdType.setId(role);

        WsPartnerrolleType wsPartnerrolleType = new WsPartnerrolleType();

//        wsPartnerrolleIdType.setId(UUID.randomUUID().toString().toLowerCase());
        wsPartnerrolleIdType.setId(changeMdmResponse.getBusinessPartnerID().get(0).getTECHMDMPartnerID().toString().toLowerCase());

        // che0bmc, 20200720: keine partnerrolleId in partnerrolleReferenz
        // wsPartnerrolleReferenzType.setPartnerrolleId(wsPartnerrolleIdType);
        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
        wsExterneReferenzType.setId(role);
        wsPartnerrolleReferenzType.setExterneReferenz(wsExterneReferenzType);
        partnerrolleRequestType.setPartnerrolleReferenz(wsPartnerrolleReferenzType);

        WsPartnerrolleStrukturType wsPartnerrolleStrukturType = new WsPartnerrolleStrukturType();

        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");
        wsPartnerrolleType.setMutationsgrundId(wsMutationsgrundIdType);

        wsPartnerrolleType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
        wsPartnerrolleType.setAenderungDurch(
                Objects.toString(
                        changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getTECHUser(), null)
        );

        final XMLGregorianCalendar validFrom = DateHelper.syriusDateFromXsDateWithLimit(changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getBITEMPValidFrom());
        wsPartnerrolleType.setStateFrom(validFrom);

        final XMLGregorianCalendar validTo = DateHelper.syriusDateFromXsDateWithLimit(changeMdmResponse.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getBITEMPValidTo());
        wsPartnerrolleType.setStateUpto(validTo);

        String mdmTimeStamp = changeMdmResponse.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0)
                .getBITEMPMDMTimestamp().toString();
        long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4, 6)),
                Integer.parseInt(mdmTimeStamp.substring(6, 8)), Integer.parseInt(mdmTimeStamp.substring(8, 10)),
                Integer.parseInt(mdmTimeStamp.substring(10, 12)), Integer.parseInt(mdmTimeStamp.substring(12, 14)),
                Integer.parseInt(mdmTimeStamp.substring(14, 17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;

        // che0bmc, 20200721, use a global epoch to modify the timestamp
        // wsPartnerrolleStrukturType.setExterneVersion(epoch); //needed for processing order in syrius
        wsPartnerrolleStrukturType.setExterneVersion(mdmModifiedEpoch); //needed for processing order in syrius

        wsPartnerrolleStrukturType.getPartnerrolleStates().add(wsPartnerrolleType);
        //wsPartnerrolleStrukturType.setGueltAb(wsPartnerrolleType.getStateFrom());
        wsPartnerrolleStrukturType.setGueltBis(wsPartnerrolleType.getStateUpto());
        wsPartnerrolleStrukturType.setPartnerrolleDefId(wsPartnerrolleDefIdType);
        partnerrolleRequestType.setPartnerrolleStruktur(wsPartnerrolleStrukturType);
        return Optional.of(partnerrolleRequestType);
    }


    public List<ReplizierePartnerrolleRequestType> mapUwCodeRolle(hasUnderwritingCodeIDEntry hasUnderwritingCodeIDEntry) {


        final String sourceTimeStamp = String.valueOf(hasUnderwritingCodeIDEntry.getBITEMPSourceTimestamp());
        final String TECHMDMPartnerID = hasUnderwritingCodeIDEntry.getTECHMDMPartnerID().toString().toLowerCase();

        List<String> roles = getPartnerrollenFrom(hasUnderwritingCodeIDEntry);
        List<ReplizierePartnerrolleRequestType> requests = roles.stream()
                .map(role -> {
                    WsPartnerrolleType wsPartnerrolleType = new WsPartnerrolleType();
                    WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
                    wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");
                    wsPartnerrolleType.setMutationsgrundId(wsMutationsgrundIdType);

                    wsPartnerrolleType.setAenderungAm(DateHelper.gregorianDateFromMDMTimestamp(sourceTimeStamp));
                    wsPartnerrolleType.setAenderungDurch(
                            Objects.toString(
                                    hasUnderwritingCodeIDEntry.getTECHUser(), null)
                    );

                    XMLGregorianCalendar validFrom = DateHelper.syriusDateFromXsDateWithLimit(hasUnderwritingCodeIDEntry.getBITEMPValidFrom());
                    XMLGregorianCalendar validTo = DateHelper.syriusDateFromXsDateWithLimit(hasUnderwritingCodeIDEntry.getBITEMPValidTo());

                    wsPartnerrolleType.setStateFrom(validFrom);
                    if (isUnderwritingDelete(hasUnderwritingCodeIDEntry)) {
                        wsPartnerrolleType.setStateUpto(validFrom);
                    } else {
                        wsPartnerrolleType.setStateUpto(validTo);
                    }

                    WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();
                    wsPartnerIdType.setId(TECHMDMPartnerID);

                    WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
                    wsPartnerReferenzType.setPartnerId(wsPartnerIdType);

                    WsPartnerrolleDefIdType wsPartnerrolleDefIdType = new WsPartnerrolleDefIdType();
                    wsPartnerrolleDefIdType.setId(role);

                    WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
                    wsExterneReferenzType.setId(role);

                    WsPartnerrolleReferenzType wsPartnerrolleReferenzType = new WsPartnerrolleReferenzType();

                    WsPartnerrolleIdType wsPartnerrolleIdType = new WsPartnerrolleIdType();
                    // che0bmc, 20200720: keine partnerrolleId in partnerrolleReferenz
                    // wsPartnerrolleReferenzType.setPartnerrolleId(wsPartnerrolleIdType);
                    wsPartnerrolleReferenzType.setExterneReferenz(wsExterneReferenzType);

                    WsPartnerrolleStrukturType wsPartnerrolleStrukturType = new WsPartnerrolleStrukturType();
                    String mdmTimeStamp = hasUnderwritingCodeIDEntry.getBITEMPMDMTimestamp().toString();
                    long epoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4, 6)),
                            Integer.parseInt(mdmTimeStamp.substring(6, 8)), Integer.parseInt(mdmTimeStamp.substring(8, 10)),
                            Integer.parseInt(mdmTimeStamp.substring(10, 12)), Integer.parseInt(mdmTimeStamp.substring(12, 14)),
                            Integer.parseInt(mdmTimeStamp.substring(14, 17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;
                    wsPartnerrolleStrukturType.setExterneVersion(epoch);
                    wsPartnerrolleStrukturType.getPartnerrolleStates().add(wsPartnerrolleType);
                    //wsPartnerrolleStrukturType.setGueltAb(wsPartnerrolleType.getStateFrom());
                    wsPartnerrolleStrukturType.setGueltBis(wsPartnerrolleType.getStateUpto());
                    wsPartnerrolleStrukturType.setPartnerrolleDefId(wsPartnerrolleDefIdType);
                    //wsPartnerrolleStrukturType.setGueltAb(wsPartnerrolleType.getStateFrom());
                    wsPartnerrolleStrukturType.setGueltBis(wsPartnerrolleType.getStateUpto());

                    ReplizierePartnerrolleRequestType partnerrolleRequestType = new ReplizierePartnerrolleRequestType();
                    partnerrolleRequestType.setPartnerReferenz(wsPartnerReferenzType);
                    partnerrolleRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());
                    partnerrolleRequestType.setPartnerrolleStruktur(wsPartnerrolleStrukturType);
                    partnerrolleRequestType.setPartnerrolleReferenz(wsPartnerrolleReferenzType);


                    return partnerrolleRequestType;
                })
                .collect(Collectors.toList());

        return requests;
    }

    private boolean isUnderwritingDelete(hasUnderwritingCodeIDEntry hasUnderwritingCodeIDEntry) {
        final Optional<CharSequence> bitempDeleted = Optional.ofNullable(hasUnderwritingCodeIDEntry.getBITEMPDeleted());
        if (bitempDeleted.isPresent()) {
            return bitempDeleted.get().toString().equals("X");
        } else {
            return false;
        }
    }

    private List<String> getPartnerrollenFrom(hasUnderwritingCodeIDEntry hasUnderwritingCodeIDEntry) {
        return hasUnderwritingCodeIDEntry.getUnderwritingCodeID()
                .get(0).getHasUnderwritingCode()
                .get(0).getUnderwritingCode()
                .stream()
                .map(uwCode -> getReferenceDataMapper().getUwCodesMapMdmToSyr().get(String.valueOf(uwCode.getStopCode())))
                .collect(Collectors.toList());
    }


}
