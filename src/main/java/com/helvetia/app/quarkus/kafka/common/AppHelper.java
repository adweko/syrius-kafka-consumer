package com.helvetia.app.quarkus.kafka.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.xml.bind.JAXB;
import java.io.StringWriter;

public class AppHelper {

    public static String readEnvAndThrowExcIfEmpty(String name) throws NullPointerException{

        // Logger logger = LoggerFactory.getLogger(AppHelper.class);

        String result = System.getenv(name);
        if ( result == null ) {
            throw new NullPointerException("Environment Variable \"" + name + "\" required but not existing");
        }
        if ( result.equals("") ) {
            throw new NullPointerException("Environment Variable \"" + name + "\" required but not set");
        }

        // logger.info("Environment: \"" + name + "\" contains \"" + result + "\"");

        return result;
    }

    public static String objectToXml(Object object) {
        try {
            StringWriter sw = new StringWriter();
            JAXB.marshal(object, sw);
            String result = sw.toString().replace("\n", "").replace("\r", "").replace("\t", "");
            while (result.contains(" <")) {
                result = result.replace(" <", "<");
            }
            while (result.contains("> ")) {
                result = result.replace("> ", ">");
            }
            return result;
        } catch (Exception ex) {
            return "xml not valid:" + ex.getMessage();
        }
    }

    public static String classToJson(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return "object not serializable";
        }
    }

}
