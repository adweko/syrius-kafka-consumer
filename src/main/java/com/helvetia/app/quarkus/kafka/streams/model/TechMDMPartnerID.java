package com.helvetia.app.quarkus.kafka.streams.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Objects;

public class TechMDMPartnerID {

    public static final Serde<TechMDMPartnerID> jsonSerde = new Serde<>() {
        @Override
        public Serializer<TechMDMPartnerID> serializer() {
            return ((topic, data) -> Serdes.String().serializer().serialize(topic, data.toJsonString()));
        }

        @Override
        public Deserializer<TechMDMPartnerID> deserializer() {
            return ((topic, data) -> {
                final String deserialized = Serdes.String().deserializer().deserialize(topic, data);
                return TechMDMPartnerID.fromJsonString(deserialized);
            });
        }
    };

    public static final Serde<TechMDMPartnerID> serde = new Serde<>() {
        @Override
        public Serializer<TechMDMPartnerID> serializer() {
            return ((topic, data) -> Serdes.String().serializer().serialize(topic, data.getValue()));
        }

        @Override
        public Deserializer<TechMDMPartnerID> deserializer() {
            return ((topic, data) -> {
                final String deserialized = Serdes.String().deserializer().deserialize(topic, data);
                return new TechMDMPartnerID(deserialized);
            });
        }
    };

    private final String value;
    private static final ObjectMapper mapper = new ObjectMapper();
    public static final String PARTNER_ID_FIELD_NAME = "TECHMDMPartnerID";



    public static TechMDMPartnerID fromJsonString(String jsonRepresentation) {
        try {
            final JsonNode jsonNode = mapper.readTree(jsonRepresentation);
            return new TechMDMPartnerID(jsonNode.get(PARTNER_ID_FIELD_NAME).asText());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }


    public static TechMDMPartnerID fromRawValueString(String rawValue) {
        return new TechMDMPartnerID(rawValue);
    }

    public String toJsonString() {
        try {
            final ObjectNode objectNode = JsonNodeFactory.instance.objectNode();
            objectNode.put(PARTNER_ID_FIELD_NAME, value);
            return mapper.writeValueAsString(objectNode);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }




    private TechMDMPartnerID(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "TechMDMPartnerID{" +
                "value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TechMDMPartnerID that = (TechMDMPartnerID) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
