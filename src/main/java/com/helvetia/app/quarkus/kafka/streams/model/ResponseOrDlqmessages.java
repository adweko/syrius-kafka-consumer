package com.helvetia.app.quarkus.kafka.streams.model;

import ch.mdm.syrius.dlq.DLQMessages;
import changePartner.com.helvetia.custom.changeMdmResponse;

public class ResponseOrDlqmessages {
    private final DLQMessages dlqMessages;
    private final changeMdmResponse response;

    public ResponseOrDlqmessages(DLQMessages dlqMessages) {
        this.dlqMessages = dlqMessages;
        this.response = null;
    }
    public ResponseOrDlqmessages(changeMdmResponse response) {
        this.response = response;
        this.dlqMessages = null;
    }

    public boolean isResponse() {
        return response != null;
    }

    public boolean isDlqMessages() {
        return dlqMessages != null;
    }

    public DLQMessages getDlqMessages() {
        return dlqMessages;
    }

    public changeMdmResponse getResponse() {
        return response;
    }
}
