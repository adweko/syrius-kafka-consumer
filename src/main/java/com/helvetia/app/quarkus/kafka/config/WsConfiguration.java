/*
 * Copyright (C) 2019 Helvetia Versicherungen. All Rights Reserved.
 */
package com.helvetia.app.quarkus.kafka.config;

import javax.enterprise.context.ApplicationScoped;
import java.util.Base64;

import static com.helvetia.app.quarkus.kafka.common.AppHelper.readEnvAndThrowExcIfEmpty;

@ApplicationScoped
public class WsConfiguration {

    public class Configuration {
        private String loggedInUser;
        private byte[] samltokenSignerKeystore;
        private String samltokenSignerPassword;

        public String getLoggedInUser() {
            return loggedInUser;
        }

        public void setLoggedInUser(String loggedInUser) {
            this.loggedInUser = loggedInUser;
        }

        public byte[] getSamltokenSignerKeystore() {
            return samltokenSignerKeystore;
        }

        public void setSamltokenSignerKeystore(byte[] samltokenSignerKeystore) {
            this.samltokenSignerKeystore = samltokenSignerKeystore;
        }

        public String getSamltokenSignerPassword() {
            return samltokenSignerPassword;
        }

        public void setSamltokenSignerPassword(String samltokenSignerPassword) {
            this.samltokenSignerPassword = samltokenSignerPassword;
        }
    }

    Configuration configuration;

    public WsConfiguration()  {

        configuration = new Configuration();

        configuration.setLoggedInUser(readEnvAndThrowExcIfEmpty("SYRIUS_USER"));
        configuration.setSamltokenSignerKeystore(Base64.getDecoder().decode(readEnvAndThrowExcIfEmpty("MDM_TKSIGN_SAML_JKS")));
        configuration.setSamltokenSignerPassword(readEnvAndThrowExcIfEmpty("MDM_TKSIGN_SAML_JKS_PWD"));
    }

    public Configuration getConfiguration() {
        return configuration;
    }
}
