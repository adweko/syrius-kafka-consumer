package com.helvetia.app.quarkus.kafka.streams.processor;

import ch.mdm.syrius.dlq.DLQMessage;
import ch.mdm.syrius.dlq.DLQMessages;
import changePartner.com.helvetia.custom.changeMdmResponse;
import com.helvetia.app.quarkus.kafka.logging.LogHelper;
import com.helvetia.app.quarkus.kafka.streams.model.ResultOrExceptionWithInput;
import com.helvetia.app.quarkus.kafka.streams.model.TechMDMPartnerID;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Collections;

@ApplicationScoped
public class MapToDeadLetterQueueTransformer<R> implements Transformer<TechMDMPartnerID, ResultOrExceptionWithInput<changeMdmResponse,R>, KeyValue<TechMDMPartnerID, DLQMessages>> {

    private ProcessorContext context;

    @Inject
    LogHelper logHelper;

    @Override
    @SuppressWarnings("unchecked")
    public void init(ProcessorContext context) {
        this.context = context;
    }
    @Override
    public KeyValue<TechMDMPartnerID, DLQMessages> transform(TechMDMPartnerID techMDMPartnerID, ResultOrExceptionWithInput<changeMdmResponse, R> value) {
        DLQMessage dlqMessage = new DLQMessage(context.topic(), context.partition(), context.offset(), context.timestamp(), value.getInput());

        String logTransactionId = LogHelper.getTechMdmPartnerID(value.getInput().getBusinessPartnerID().get(0))
                + "|" + LogHelper.getPartnerId(value.getInput().getBusinessPartnerID().get(0), LogHelper.ZEPASPARTNERKEY)
                + "|" + LogHelper.getPartnerId(value.getInput().getBusinessPartnerID().get(0), LogHelper.SYRIUSPARTNERKEY);
        logHelper.logError(logTransactionId, "message send to DLQ. Topic/Partition/Offset: " +
                context.topic() + " / " + context.partition() + " / " + context.offset());

        return new KeyValue<>(techMDMPartnerID, new DLQMessages(Collections.singletonList(dlqMessage)));

    }


    @Override
    public void close() {

    }
}
