package com.helvetia.app.quarkus.kafka;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereDublettenRequestType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereJurPersonRequestType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereNatPersonRequestType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.ReplizierePartnerrolleRequestType;
import com.helvetia.app.quarkus.kafka.config.SyriusConfig;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDEntry;
import inboundTrafoZepasUnderwriting.json.Response.RowSchema.hasUnderwritingCodeID.hasUnderwritingCodeIDEntry;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Representation of a Partner in terms of the various Syrius entities to be called to create it in Syrius.
 */
@Getter
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PartnerSyrius {

    private static final Logger logger = LoggerFactory.getLogger(PartnerSyrius.class);

    private RepliziereNatPersonRequestType repliziereNatPersonRequestType;
    private RepliziereJurPersonRequestType repliziereJurPersonRequestType;
    private final List<RepliziereChannelRequestWrapper> repliziereChannelRequestWrappers = new ArrayList<>();
    private final List<ReplizierePartnerrolleRequestType> replizierePartnerrolleRequestTypeList = new ArrayList<>();
    private RepliziereDublettenRequestType repliziereDublettenRequestType;

    private void setRepliziereNatPersonRequestType(RepliziereNatPersonRequestType repliziereNatPersonRequestType) {
        this.repliziereNatPersonRequestType = repliziereNatPersonRequestType;
    }

    private void setRepliziereJurPersonRequestType(RepliziereJurPersonRequestType repliziereJurPersonRequestType) {
        this.repliziereJurPersonRequestType = repliziereJurPersonRequestType;
    }

    private void addReplizierePartnerrolleRequestType(ReplizierePartnerrolleRequestType replizierePartnerrolleRequestType) {
        this.replizierePartnerrolleRequestTypeList.add(replizierePartnerrolleRequestType);
    }

    private void addRepliziereChannelRequestWrapper(RepliziereChannelRequestWrapper repliziereChannelRequestWrapper) {
        this.repliziereChannelRequestWrappers.add(repliziereChannelRequestWrapper);
    }

    private void setRepliziereDublettenRequestType(RepliziereDublettenRequestType repliziereDublettenRequestType) {
        this.repliziereDublettenRequestType = repliziereDublettenRequestType;
    }

    public static PartnerSyrius fromChangeMdmResponse(changeMdmResponse mdmchangeMessage,
                                                      SyriusConfig syriusConfig) throws Exception {
        PartnerSyrius partner = new PartnerSyrius();

        // che0bmc, 20200721, use a global epoch to modify the timestamp
        String mdmTimeStamp = mdmchangeMessage.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0)
                .getBITEMPMDMTimestamp().toString();
        long mdmModifiedEpoch = LocalDateTime.of(Integer.parseInt(mdmTimeStamp.substring(0, 4)), Integer.parseInt(mdmTimeStamp.substring(4, 6)),
                Integer.parseInt(mdmTimeStamp.substring(6, 8)), Integer.parseInt(mdmTimeStamp.substring(8, 10)),
                Integer.parseInt(mdmTimeStamp.substring(10, 12)), Integer.parseInt(mdmTimeStamp.substring(12, 14)),
                Integer.parseInt(mdmTimeStamp.substring(14, 17)) * 1000000).atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;

        if (isNaturalPerson(mdmchangeMessage)) {
            partner.setRepliziereNatPersonRequestType(extractNatPersonFromResponse(mdmchangeMessage, syriusConfig, mdmModifiedEpoch));
            mdmModifiedEpoch += 1000000000L;
            List<ReplizierePartnerrolleRequestType> personRoles =
                    extractRolesFromNatPerson(mdmchangeMessage, syriusConfig, mdmModifiedEpoch);
            mdmModifiedEpoch += (personRoles.size() * 1000000000L);
            personRoles.forEach(partner::addReplizierePartnerrolleRequestType);
            partner.setRepliziereJurPersonRequestType(null);
        } else if (isLegalPerson(mdmchangeMessage)) {
            partner.setRepliziereJurPersonRequestType(extractJurPersonFromResponse(mdmchangeMessage, syriusConfig));
            partner.setRepliziereNatPersonRequestType(null);
        } /*else {
            throw new IllegalStateException("Partner is neither Natural nor Juristic Person");
        }*/

        List<RepliziereChannelRequestWrapper> channels = getChannelListFromResponse(mdmchangeMessage)
                .stream()
                .filter(Objects::nonNull)
                .map(channel -> mapChannelToSyrius(channel, syriusConfig))
                .collect(Collectors.toList());
        channels.forEach(partner::addRepliziereChannelRequestWrapper);

        List<ReplizierePartnerrolleRequestType> underWritings = getUnderwritingCodesFromResponse(mdmchangeMessage)
                .stream()
                .filter(Objects::nonNull)
                .flatMap(underWriting -> mapUnderWritingToSyrius(underWriting, syriusConfig).stream())
                .collect(Collectors.toList());
        underWritings.forEach(partner::addReplizierePartnerrolleRequestType);

        if (isDuplicateResponse(mdmchangeMessage)) {
            partner.setRepliziereDublettenRequestType(extractDuplicateFromResponse(mdmchangeMessage, syriusConfig));
        }

        return partner;
    }

    private static List<hasUnderwritingCodeIDEntry> getUnderwritingCodesFromResponse(changeMdmResponse mdmchangeMessage) {
        List<hasUnderwritingCodeIDEntry> uwCodes = mdmchangeMessage.getBusinessPartnerID().get(0).getHasUnderwritingCodeID();
        return Objects.requireNonNullElse(uwCodes, Collections.emptyList());
    }

    private static List<hasChannelIDEntry> getChannelListFromResponse(changeMdmResponse mdmchangeMessage) {
        List<hasChannelIDEntry> channels = mdmchangeMessage.getBusinessPartnerID().get(0).getHasChannelID();
        return Objects.requireNonNullElse(channels, Collections.emptyList());
    }

    private static List<ReplizierePartnerrolleRequestType> extractRolesFromNatPerson(changeMdmResponse mdmchangeMessage, SyriusConfig syriusConfig, long mdmModifiedEpoch) {

        ArrayList<ReplizierePartnerrolleRequestType> roles = new ArrayList<>();

        //Partnerrollen - eine Person kann Mitarbeiter, Diplomat sein und eine Rabattklase haben
        // we always map discount category, will set it to "no discount category" if not present
        roles.add(syriusConfig.getNatPersonMapper().mapDiscountCategoryToPersonenRolleRabattklasse(mdmchangeMessage, mdmModifiedEpoch));
        // we always map employee, will set it to "no" if not present
        mdmModifiedEpoch += 1000000000L;
        roles.add(syriusConfig.getNatPersonMapper().mapEmployeeToPersonenRolleMitarbeiter(mdmchangeMessage, mdmModifiedEpoch));
        // map diplomat if present
        mdmModifiedEpoch += 1000000000L;
        syriusConfig.getNatPersonMapper().mapResidencePermitToPersonenRolleDiplomat(mdmchangeMessage, mdmModifiedEpoch).ifPresent(roles::add);

        return roles;
    }

    private static RepliziereChannelRequestWrapper<?> mapChannelToSyrius(hasChannelIDEntry channel, SyriusConfig syriusConfig) {

        String channelType = String.valueOf(channel.getChannelID().get(0).getChannelType());
        logger.info("ChannelType = {}", channelType);
        try {
            switch (channelType) {
                case "1":
                    return RepliziereChannelRequestWrapper.of(syriusConfig.getKommVerMapper().mapTelefon(channel));
                case "2":
                    return RepliziereChannelRequestWrapper.of(syriusConfig.getKommVerMapper().mapMobile(channel));
                case "3":
                    return RepliziereChannelRequestWrapper.of(syriusConfig.getKommVerMapper().mapFax(channel));
                case "4":
                    return RepliziereChannelRequestWrapper.of(syriusConfig.getKommVerMapper().mapMail(channel));
                case "5":
                    return RepliziereChannelRequestWrapper.of(syriusConfig.getKommVerMapper().mapSkype(channel));
                case "6":
                    return RepliziereChannelRequestWrapper.of(syriusConfig.getAddressMapper().mapDomizilAdresse(channel));
                case "7":
                case "8":
                    return RepliziereChannelRequestWrapper.of(syriusConfig.getAddressMapper().mapZusatzAdresse(channel));
                default:
                    throw new IllegalStateException("unexpected channelType " + channelType + "in hasChannelIDEntry" + channel.toString());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private static List<ReplizierePartnerrolleRequestType> mapUnderWritingToSyrius(hasUnderwritingCodeIDEntry underWriting, SyriusConfig syriusConfig) {
//        try {
            return syriusConfig.getNatPersonMapper().mapUwCodeRolle(underWriting);
//        } catch (DatatypeConfigurationException | IOException e) {
//            throw new RuntimeException(e);
//        }
    }

    private static boolean isDuplicateResponse(changeMdmResponse mdmchangeMessage) {
        return mdmchangeMessage.getBusinessPartnerID().get(0).getIsDuplicate() != null && mdmchangeMessage.getBusinessPartnerID().get(0).getIsDuplicate().size() > 0;
    }

    private static boolean isLegalPerson(changeMdmResponse mdmchangeMessage) {
        return mdmchangeMessage.getBusinessPartnerID().get(0).getIsLegalPerson() != null && mdmchangeMessage.getBusinessPartnerID().get(0).getIsLegalPerson().size() > 0;
    }

    private static boolean isNaturalPerson(changeMdmResponse mdmchangeMessage) {
        return mdmchangeMessage.getBusinessPartnerID().get(0).getIsNaturalPerson() != null && mdmchangeMessage.getBusinessPartnerID().get(0).getIsNaturalPerson().size() > 0;
    }


    private static RepliziereJurPersonRequestType extractJurPersonFromResponse(changeMdmResponse mdmchangeMessage, SyriusConfig syriusConfig) throws IOException {
        return syriusConfig.getJurPersonMapper().mapJurPerson(mdmchangeMessage);
    }

    private static RepliziereNatPersonRequestType extractNatPersonFromResponse(changeMdmResponse mdmchangeMessage, SyriusConfig syriusConfig, long mdmModifiedEpoch){
        return syriusConfig.getNatPersonMapper().mapNatPerson(mdmchangeMessage, mdmModifiedEpoch);
    }

    private static RepliziereDublettenRequestType extractDuplicateFromResponse(changeMdmResponse mdmchangeMessage, SyriusConfig syriusConfig) {
        return syriusConfig.getDublettenMapper().mapDublette(mdmchangeMessage);
    }


}
