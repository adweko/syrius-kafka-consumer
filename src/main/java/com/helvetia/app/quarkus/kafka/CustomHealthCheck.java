package com.helvetia.app.quarkus.kafka;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Liveness;
import org.eclipse.microprofile.health.Readiness;

import javax.enterprise.context.ApplicationScoped;

@Liveness
@Readiness
@ApplicationScoped
public class CustomHealthCheck implements HealthCheck {
    @Override
    public HealthCheckResponse call() {
        try {
            return HealthCheckResponse.builder()
                    .name("SyriusConsumerHealth")
                    .up()
                    .withData("status", "ok")
                    .build();
        } catch (Exception e) {
            return HealthCheckResponse.builder()
                    .name("SyriusConsumerHealth")
                    .down()
                    .build();
        }
    }
}
