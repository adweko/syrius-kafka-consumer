package com.helvetia.app.quarkus.kafka.streams.specificlogic;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.helvetia.app.quarkus.kafka.config.SyriusConfig;
import com.helvetia.app.quarkus.kafka.logging.LogHelper;
import com.helvetia.app.quarkus.kafka.streams.RelevantMessageFilter;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDEntry;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class RelevantMessageFilterImpl implements RelevantMessageFilter<changeMdmResponse> {

    @Inject
    SyriusConfig syriusConfig;

    @Inject
    LogHelper logHelper;

    @Override
    public boolean isMessageRelevant(changeMdmResponse message) {
        if (message.getBusinessPartnerID().get(0).getIsNaturalPerson() != null &&
                message.getBusinessPartnerID().get(0).getIsNaturalPerson().size() > 0 &&
                !message.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getTECHSourceSystem().toString().equals(syriusConfig.getSYRIUS())) {
            return true;
        }
        if (message.getBusinessPartnerID().get(0).getIsLegalPerson() != null &&
                message.getBusinessPartnerID().get(0).getIsLegalPerson().size() > 0 &&
                !message.getBusinessPartnerID().get(0).getIsBusinessPartner().get(0).getTECHSourceSystem().toString().equals(syriusConfig.getSYRIUS())) {
            return true;
        }
        List<hasChannelIDEntry> channelList = message.getBusinessPartnerID().get(0).getHasChannelID();
        if (channelList != null) {
            for (hasChannelIDEntry channel : channelList) {
                if (!channel.getTECHSourceSystem().toString().equals(syriusConfig.getSYRIUS())) {
                    return true;
                }
            }
        }
        if (message.getBusinessPartnerID().get(0).getHasUnderwritingCodeID() != null &&
                message.getBusinessPartnerID().get(0).getHasUnderwritingCodeID().size() > 0 &&
                !message.getBusinessPartnerID().get(0).getHasUnderwritingCodeID().get(0).getTECHSourceSystem().toString().equals(syriusConfig.getSYRIUS())
        ) {
            return true;
        }
        if (message.getBusinessPartnerID().get(0).getIsDuplicate() != null &&
                message.getBusinessPartnerID().get(0).getIsDuplicate().size() > 0 &&
                !message.getBusinessPartnerID().get(0).getIsDuplicate().get(0).getTECHSourceSystem().toString().equals(syriusConfig.getSYRIUS())
        ) {
            return true;
        }
        String logTransactionId = LogHelper.getTechMdmPartnerID(message.getBusinessPartnerID().get(0))
                + "|" + LogHelper.getPartnerId(message.getBusinessPartnerID().get(0), LogHelper.ZEPASPARTNERKEY)
                + "|" + LogHelper.getPartnerId(message.getBusinessPartnerID().get(0), LogHelper.SYRIUSPARTNERKEY);

        logHelper.logInfo(logTransactionId, "message is not relevant");
        return false;
    }
}
